import React from 'react';
import ReactDOM from 'react-dom';
import { Router } from 'react-router-dom';
import './index.css';
import App from './Containers/App';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'tachyons';
import history from './history'
import { transitions, positions, Provider as AlertProvider } from 'react-alert'
import AlertTemplate from 'react-alert-template-basic'

const options = {
    // you can also just use 'bottom center'
    position: positions.TOP_RIGHT,
    timeout: 5000,
    offset: '30px',
    // you can also just use 'scale'
    transition: transitions.SCALE
  }

ReactDOM.render(
    <AlertProvider template={AlertTemplate} {...options}>
        <Router history={history}>
            <App />
        </Router>
    </AlertProvider>
    , document.getElementById('root'));

