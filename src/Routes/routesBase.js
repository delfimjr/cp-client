import React from 'react';
import { Route, Switch } from 'react-router-dom';

import HomeGeneralUser from '../Components/HomeGeneral/HomeGeneralUser'
import LogIn from '../Components/SignIn/LogIn'
import RecLogin from '../Components/SignIn/AccessRecovery'
import CreatePassword from '../Components/SignIn/AccessRecovery/createPassword'
import Admin from '../Components/Admin/Admin';




const RoutesBase = () => {
    return (
        <Switch>
            <Route exact path='/' component={HomeGeneralUser} />
            <Route path='/login' component={LogIn} />
            <Route path="/recuperar-acesso" component={RecLogin} />
            <Route exact path="/create-password" component={CreatePassword} />
            <Route path="/dashboard" component={Admin} />
        </Switch>
    )
}


export default RoutesBase;