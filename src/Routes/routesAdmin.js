import React from 'react';
import { Route, Switch } from 'react-router-dom';
import AdminManagment from '../Components/AdminManagment'
import Dashboard from '../Components/Home/Conteudo'
import Profile from '../Components/Profile'
import EnvioDeMensagem from '../Components/Messages/Envio_de_Mensagem'
import Inbox from '../Components/Inbox/';
import History from '../Components/Messages/Sent_Messages/history'
import VerPublicacao from '../Components/Publicacoes/VerPublicacao';
import GroupCreation from '../Components/DestinationGroup/';
import CriarPublicacao from '../Components/Publicacoes/CriarPublicacao';
import Notificacoes from '../Components/UserHeader/Notificacoes';
import Contacts from '../Components/Contacts/'
import VerPublicacaoComentada from '../Components/UserHeader/VerPublicacaoComentada';
import SingleMessage from '../Components/Inbox/SingleMessage'


const RoutesAdmin = (props) => {

    return (
        <Switch>
            <Route
                exact
                path="/dashboard"
                render={() => <Dashboard userObject={props.userObject} handlePageTitleChange={props.handlePageTitleChange} />}
            />

            <Route
                path="/dashboard/perfil"
                render={() => <Profile {...props} />}
            />

            <Route
                path="/dashboard/Notificacoes"
                render={() => <Notificacoes userObject={props.userObject} handlePageTitleChange={props.handlePageTitleChange} />}
            />

            <Route
                path="/dashboard/enviar-mensagem"
                render={() => <EnvioDeMensagem userObject={props.userObject} handlePageTitleChange={props.handlePageTitleChange} />}
            />

            <Route
                path="/dashboard/caixa-de-emails"
                render={() =>
                    <Inbox
                        userObject={props.userObject}
                        handlePageTitleChange={props.handlePageTitleChange}
                        data={props.data} 
                        error={props.error} 
                        isValidating={props.isValidating}
                        mutate={props.mutate}
                    />
                }
            />

            <Route
                path="/dashboard/mensagens-enviadas"
                render={() => <History userObject={props.userObject} handlePageTitleChange={props.handlePageTitleChange} />}
            />

            <Route
                path="/dashboard/publicacoes"
                render={() => <VerPublicacao userObject={props.userObject} cardId={props.handlePageTitleChange} handlePageTitleChange={props.handlePageTitleChange} />}
            />

            <Route
                path="/dashboard/publicacao/:id"
                render={() => <VerPublicacaoComentada userObject={props.userObject} cardId='card' handlePageTitleChange={props.handlePageTitleChange} />}
            />

            <Route
                path="/dashboard/criar-publicacao"
                render={() => <CriarPublicacao userObject={props.userObject} handlePageTitleChange={props.handlePageTitleChange} />}
            />

            <Route
                path="/dashboard/configuracoes/grupos"
                render={() => <GroupCreation userObject={props.userObject} handlePageTitleChange={props.handlePageTitleChange} />}
            />

            <Route
                path="/dashboard/configuracoes/administradores"
                render={() => <AdminManagment userObject={props.userObject} handlePageTitleChange={props.handlePageTitleChange} />}
            />

            <Route
                path="/dashboard/configuracoes/contactos"
                render={() => <Contacts handlePageTitleChange={props.handlePageTitleChange} />}
            />

            <Route
                exact
                path="/dashboard/caixa-de-emails/:id"
                render={() => <SingleMessage handlePageTitleChange={props.handlePageTitleChange} />}
            />
        </Switch>
    )
}


export default RoutesAdmin;