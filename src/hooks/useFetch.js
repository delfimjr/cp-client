import useSWR from 'swr'
import api from '../Services/api'

export function useFetch(url) {
    const { data, error, isValidating, mutate } = useSWR(url, async url => {
        const data = await api.get(url).then(response => response.data)
        return data
    }, {refreshInterval: 20000})

    return { data, error, isValidating, mutate }
}
