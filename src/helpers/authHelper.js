export function getUserPermissions() {
    const userData = JSON.parse(sessionStorage.getItem('id'));
    return userData.user.permissions
}

export function getHeaderWithToken() {
    const userData = JSON.parse(sessionStorage.getItem('id'));
    return {
        'Authorization': 'Bearer ' + userData.accessToken
    };
}
