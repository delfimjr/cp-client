function dateOrganizeHelper(dateFromDataBase) {
    let date = dateFromDataBase?.split('T')
    let hour = date?.pop()
    hour = hour?.split(':');
    hour.pop();

    return date + " " + hour.join(':')
}

export default dateOrganizeHelper;