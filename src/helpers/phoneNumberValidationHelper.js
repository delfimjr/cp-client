const numberFormatValidationHelper = async (numbers) => {
    var result = '';


    for (const number of numbers) {
      const result1 = await fetch(`http://apilayer.net/api/validate?access_key=c25b46b053622e9d7e1755f7f2abcacc&number=${number}`)

      const result2 = await result1.json()

      if (!result2.valid) {
        result = { valid: result2.valid, number: result2.number }
      }
    }
    return result
}

export default numberFormatValidationHelper;