export default function formDataHelper(files, body) {
    const data = new FormData();
    if (files != null) {
        for (var index = 0; index < files.length; index++) {
            data.append('file', files[index])
        }
    }

    data.append('content', JSON.stringify(body))

    return data
}