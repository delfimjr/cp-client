import React, {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';
import './Home.css';

import {FiMessageSquare, FiSend, FiShare2, FiUsers} from "react-icons/fi"

import {
    Row,
    Col,
} from "reactstrap";
import {getUserPermissions} from "../../helpers/authHelper";


const Dashboard = (props) => {
    useEffect(() => {
        props.handlePageTitleChange('Dashboard')
    }, [])


    return (
        <div className="container">
            <div className="row justify-content-start">
                <h1 className="dashboard-greeting-title">{`Olá, ${props.userObject.user.name}!`}&#128512;</h1>
            </div>
            <Row>
                {
                    getUserPermissions().includes('P_VIEW_EMAILS')
                    &&
                    <Col md="3">
                        <Link className="dashboard-link" to="/dashboard/caixa-de-emails">
                            <div className="dashboard-card">
                                <div className="row justify-content-between">
                                    <div className="col">
                                        <FiMessageSquare color="#F4D03F" size={40}/>
                                    </div>
                                    <div className="col">
                                        <h4 className="card-title">Emails</h4>
                                    </div>
                                </div>
                                <div className="row justify-content-center">
                                    <p className="dashboard-card-description">veja aqui os emails mais recentes.</p>
                                </div>
                                <div className="row justify-content-center">
                                    <hr className="dashboard"></hr>
                                </div>
                                <div className="row justify-content-center">
                                    <h6>Caixa de Entrada</h6>
                                </div>
                            </div>
                        </Link>
                    </Col>
                }

                {
                    getUserPermissions().includes('P_SEND_EMAILS')
                    &&
                    <Col md="3">
                        <Link className="dashboard-link" to="/dashboard/enviar-mensagem">
                            <div className="dashboard-card">
                                <div className="row justify-content-between">
                                    <div className="col">
                                        <FiSend color="#3498DB" size={40}/>
                                    </div>
                                    <div className="col-9">
                                        <h4 className="card-title" v>Mensagens</h4>
                                    </div>
                                </div>
                                <div className="row justify-content-center">
                                    <p className="dashboard-card-description">email e SMS.</p>
                                </div>
                                <div className="row justify-content-center">
                                    <hr className="dashboard"/>
                                </div>
                                <div className="row justify-content-center">
                                    <h6>Envio de mensagens</h6>
                                </div>
                            </div>
                        </Link>
                    </Col>
                }

                {
                    getUserPermissions().includes('P_PUBLISH_CONTENT')
                    &&
                    <Col md="3">
                        <Link className="dashboard-link" to="/dashboard/criar-publicacao">
                            <div className="dashboard-card">
                                <div className="row justify-content-between">
                                    <div className="col">
                                        <FiShare2 color="#1ABC9C" size={40}/>
                                    </div>
                                    <div className="col-9">
                                        <h4 className="card-title">Publicações</h4>
                                    </div>
                                </div>
                                <div className="row justify-content-center">
                                    <p className="dashboard-card-description">escreva "posts" para a página inicial</p>
                                </div>
                                <div className="row justify-content-center">
                                    <hr className="dashboard"></hr>
                                </div>
                                <div className="row justify-content-center">
                                    <h6>Criação de Publicações</h6>
                                </div>
                            </div>
                        </Link>
                    </Col>
                }

                {
                    getUserPermissions().includes('P_PUBLISH_CONTENT')
                    &&
                    <Col md="3">
                        <Link className="dashboard-link" to="/dashboard/configuracoes/grupos">
                            <div className="dashboard-card">
                                <div className="row justify-content-between">
                                    <div className="col">
                                        <FiUsers color="#F08080" size={40}/>
                                    </div>
                                    <div className="col">
                                        <h4 className="card-title">Grupos</h4>
                                    </div>
                                </div>
                                <div className="row justify-content-center">
                                    <p className="dashboard-card-description">personalize grupos de contactos</p>
                                </div>
                                <div className="row justify-content-center">
                                    <hr className="dashboard"></hr>
                                </div>
                                <div className="row justify-content-center">
                                    <h6>Gestão de Grupos</h6>
                                </div>
                            </div>
                        </Link>
                    </Col>
                }
            </Row>
        </div>
    )
}

export default Dashboard
