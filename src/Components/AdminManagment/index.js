import React, {Component} from "react";
import validator from 'validator';
import {FaSave, FaEdit, FaRegTrashAlt} from "react-icons/fa";
import {FiUserPlus, FiList, FiZap, FiZapOff} from 'react-icons/fi';

import {
    Card,
    CardBody,
    CardTitle,
    Row,
    Col,
    Table
} from "reactstrap";

import api from '../../Services/api'

import './styles.css'
import {fadeInDown, bounce} from 'react-animations';
import Radium, {StyleRoot} from 'radium';
import {ToastContainer, toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {getHeaderWithToken} from "../../helpers/authHelper";

const styles = {
    fadeInDown: {
        animation: 'x 0.8s',
        animationName: Radium.keyframes(fadeInDown, 'fadeInDown')
    },
    bounce: {
        animation: 'x 0.8s',
        animationName: Radium.keyframes(bounce, 'bounce')
    }
}

export default class AdminManagement extends Component {

    constructor(props) {
        super(props)
        this.state = {
            //BASIC STATES//
            name: '',
            lastname: '',
            email: '',
            loading: false,
            emptinessAlert: false,
            isValidEmail: true,
            newPasswordMatchWarning: '',
            emailIsEmptyBorder: '',
            newPassword1Border: '',
            newPassword2Border: '',
            lastnameIsEmptyBorder: '',
            nameIsEmptyBorder: '',
            sixCharacters: true,
            admins: [],
            roles: [],
            chosenRoleId: ''
        }
    }

    UNSAFE_componentWillMount() {
        this.props.handlePageTitleChange('Gestão de Administradores')
        this.fetch()
    }

    fetch = () => {
        const currentUserData = this.props.userObject
        this.setState({loading: true})

        api.get("communications", {headers: getHeaderWithToken()}).then(response => {

            const auxAdmins = response.data.filter(user => user.email !== currentUserData.user.email)
            this.setState({admins: auxAdmins})
            this.setState({loading: false})
        })
            .catch(() => {
                this.setState({loading: false})
                alert("failed on getting admin list")
            })

        api.get('roles',{headers: getHeaderWithToken()})
            .then(response => {
                this.setState({roles: response.data})
            })
    }

    onNameChange = (event) => {
        this.setState({name: event.target.value})
        this.setState({nameIsEmptyBorder: ''})
        this.setState({emptinessAlert: false})
    }

    onLastNameChange = (event) => {
        this.setState({lastname: event.target.value})
        this.setState({lastnameIsEmptyBorder: ''})
        this.setState({emptinessAlert: false})
    }

    onEmailChange = (event) => {
        this.setState({email: event.target.value})
        this.setState({emailIsEmptyBorder: ''})
        this.setState({emptinessAlert: false})
        this.setState({isValidEmail: true})
    }

    onPasswordChange = (event) => {
        this.setState({password: event.target.value})
        this.setState({newPassword1Border: ''})
        this.setState({sixCharacters: true})
        this.setState({emptinessAlert: false})
    }

    onConfirmPasswordChange = (event) => {
        this.setState({confirmPassword: event.target.value})
        this.setState({newPassword2Border: ''})
        this.setState({sixCharacters: true})
        this.setState({emptinessAlert: false})
    }

    onChangeRole = (event) => {
        this.setState({chosenRoleId: event.target.value})
    }

    handleSaveUser = async () => {
        const {name, lastname, email, chosenRoleId} = this.state;

        if (name !== '' && lastname !== '' && email !== '' && chosenRoleId !== '') {
            let isEmail = validator.isEmail(email)
            if (!isEmail) {
                this.setState({isValidEmail: false})
            } else {
                //register new Admin...
                const data = {
                    name,
                    lastName: lastname,
                    email,
                    roleId: chosenRoleId
                }
                this.setState({loading: true})

                await api.post('communications', data, {headers: getHeaderWithToken()}).then((response) => {
                    this.setState({loading: false})
                    toast.success('Utilizador Registado! As credenciais de acesso foram enviadas para o utilizador')
                    this.clearInputs()
                    this.fetch()
                })
                    .catch(err => {
                        this.setState({loading: false})
                        if (err?.response?.data?.error) {
                            toast.error(err?.response?.data?.error)
                        }
                    })
            }

        } else {
            this.setState({emptinessAlert: true})

            if (name === '') {
                this.setState({nameIsEmptyBorder: 'border border-warning'})
            }
            if (lastname === '') {
                this.setState({lastnameIsEmptyBorder: 'border border-warning'})
            }
            if (email === '') {
                this.setState({emailIsEmptyBorder: 'border border-warning'})
            }
        }
    }

    handleRemove = async id => {
        const {admins} = this.state

        this.setState({loading: true})
        api.delete(`communications/${id}`,{headers: getHeaderWithToken()}).then(() => {
            const auxAdmin = admins.filter(admin => admin.id !== id)
            this.setState({admins: auxAdmin})
            this.setState({loading: false})
        })
            .catch(e => {
                console.log(e);
                this.setState({loading: false});
                toast.error("Failed to remove user, probably he is associated to operations in the system");
            })
    }

    handleSuperAdmin = async id => {
        const {admins} = this.state
        this.setState({loading: true})

        const superAdmin = admins.filter(admin => admin.id === id)

        api.put(`communications/${id}/${superAdmin[0]?.super ? "unsetSuper" : "setSuper"}/`,{},{headers: getHeaderWithToken()}).then(() => {

            const auxAdmin = admins.map(admin => {
                if (admin.id === id) {
                    if (admin.super) {
                        return {...admin, super: false}
                    } else {
                        return {...admin, super: true}
                    }
                }

                return admin
            })

            this.setState({admins: auxAdmin})
            this.setState({loading: false})
        })
            .catch((err) => {
                this.setState({loading: false})
                alert(err?.response?.data?.error)
            })
    }


    clearInputs() {
        this.setState({password: ''})
        this.setState({confirmPassword: ''})
        this.setState({name: ''})
        this.setState({lastname: ''})
        this.setState({email: ''})
    }

    render() {
        const {
            emptinessAlert,
            isValidEmail,
            nameIsEmptyBorder,
            lastnameIsEmptyBorder,
            emailIsEmptyBorder,
            loading,
            admins,
            name,
            lastname,
            email,
            roles

        } = this.state

        let alert_message = "Todos os campos são obrigatórios.", color = "#ABEBC6"
        if (emptinessAlert) {
            alert_message = "Por favor, preencha os campos vazios."
            color = "#F7DC6F"
        }

        return (
            <>
                <ToastContainer/>
                <StyleRoot>
                    <Row>
                        <Col md="6">
                            <Card className="shadow-sm p-3">
                                <CardTitle>
                                <span style={{display: "flex"}}>
                                    <h4 className="title-register">Registar</h4>&nbsp;
                                    <FaEdit size={26} style={{color: "#424949"}}/>
                                </span>
                                </CardTitle>
                                <CardBody>
                                    <header className="container">
                                        <div className="row justify-content-center">
                                            <FiUserPlus
                                                className="icon-register"
                                                size={70}
                                                color="#B2BABB"
                                            />
                                        </div>
                                        <div
                                            className="message mt-2 mb-4 text-center"
                                            style={{background: `${color}`, borderRadius: "2px", fontSize: "10.5pt"}}
                                        >
                                            {alert_message}
                                        </div>
                                    </header>
                                    <form className="admin-register" ref={form => this.form = form}>
                                        <div className="admin-name-last">
                                            <input
                                                onChange={this.onNameChange}
                                                className={`form-control mr-2 register ${nameIsEmptyBorder}`}
                                                type='text'
                                                name="name"
                                                placeholder="Introduzir o nome"
                                                autoComplete="false"
                                                required
                                                value={name}
                                            />
                                            <input
                                                onChange={this.onLastNameChange}
                                                className={`form-control register ${lastnameIsEmptyBorder}`}
                                                type='text'
                                                name="lastname"
                                                placeholder="Introduzir o Apelido"
                                                autoComplete="nope"
                                                required
                                                value={lastname}
                                            />
                                        </div>
                                        <div className="admin-email">
                                            <input
                                                onChange={this.onEmailChange}
                                                className={`form-control register ${emailIsEmptyBorder}`}
                                                type='email'
                                                name="email"
                                                placeholder="E-mail"
                                                autoComplete="nope"
                                                required
                                                value={email}
                                            />
                                        </div>
                                        {
                                            !isValidEmail
                                                ?
                                                <div className="email-alert">
                                                    <span className='text-danger'>O Email é inválido</span>
                                                </div>
                                                :
                                                null
                                        }
                                        <div className="admin-email">
                                            <select onChange={this.onChangeRole} className='form-control register'>
                                                <option value=''>Select User Role</option>
                                                {
                                                    roles.map(role => (
                                                        <option value={role.id}>{role.name}</option>
                                                    ))
                                                }
                                            </select>
                                        </div>

                                        <div className="admin-save">
                                            <button
                                                onClick={this.handleSaveUser}
                                                className='btn btn-info'
                                                type="button"
                                            >
                                                Guardar &nbsp;
                                                <i><FaSave/></i>
                                            </button>
                                        </div>
                                    </form>
                                </CardBody>
                            </Card>
                        </Col>
                        <Col md="6">
                            <Card className="shadow-sm p-3 admin-list-card">
                                <CardTitle>
                                    <FiList size={35}/>
                                </CardTitle>
                                <CardBody>
                                    <Table className="table-hover table-borderless table-admin" responsive>
                                        {
                                            loading
                                                ?
                                                <h6>aguarde, por favor...</h6>
                                                :
                                                (
                                                    admins.length !== 0
                                                        ?
                                                        <tbody style={{cursor: "ponter", fontSize: "11pt"}}>
                                                        {

                                                            admins.map(admin =>
                                                                <tr className="admin-list" style={styles.fadeInDown}>
                                                                    <td>
                                                                        {admin.email}
                                                                        {
                                                                            admin.super &&
                                                                            <h6 style={{fontWeight: "normal"}}>
                                                                                <i class="badge badge-pill badge-dark">super</i>
                                                                            </h6>
                                                                        }
                                                                    </td>
                                                                    <td>
                                                                        <FaRegTrashAlt
                                                                            data-toggle="tooltip"
                                                                            data-placement="top"
                                                                            title="excluir admin"
                                                                            onClick={() => this.handleRemove(admin.id)}
                                                                            className='trashStyle'
                                                                            style={{color: '#EC7063'}}
                                                                        />
                                                                    </td>
                                                                    {
                                                                        !admin.super
                                                                            ?
                                                                            <td>
                                                                                <FiZap
                                                                                    data-toggle="tooltip"
                                                                                    data-placement="top"
                                                                                    title="tornar super admin"
                                                                                    onClick={() => this.handleSuperAdmin(admin.id)}
                                                                                    className='trashStyle'
                                                                                />
                                                                            </td>
                                                                            :
                                                                            <td>
                                                                                <FiZapOff
                                                                                    data-toggle="tooltip"
                                                                                    data-placement="top"
                                                                                    title="retirar da lista super"
                                                                                    onClick={() => this.handleSuperAdmin(admin.id)}
                                                                                    className='trashStyle'
                                                                                />
                                                                            </td>
                                                                    }
                                                                </tr>
                                                            )
                                                        }

                                                        </tbody>
                                                        :
                                                        <i className='text-info'>A plataforma ainda não possui outro(a)
                                                            admin além de você</i>
                                                )
                                        }

                                    </Table>
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>
                </StyleRoot>
            </>
        );

    }
}
