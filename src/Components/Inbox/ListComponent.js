import React from 'react';
import { fadeInDown } from 'react-animations';
import Radium from 'radium';

import {
    Table
} from "reactstrap";
import dateOrganizeHelper from '../../helpers/dateOrganizeHelper';


function ListComponent({ messages, handleSeeMessage, inbox }) {

    const styles = {
        fadeInDown: {
            animation: 'x 1s',
            animationName: Radium.keyframes(fadeInDown, 'fadeInDown')
        }
    }

    return (
        <Table className="table-hover table-borderless table-admin" responsive>
            {
                !messages
                    ?
                    <h6>aguarde, por favor...</h6>
                    :
                    (
                        inbox
                            ?
                            (
                                messages?.length !== 0
                                    ?
                                    <tbody style={{ cursor: "pointer", fontSize: "11pt" }}>
                                        {

                                            messages?.map(message => {
                                                let date = message.received_at.split('T')
                                                let hora = date.pop()
                                                hora = hora.split(':');
                                                hora.pop();
                                                let teaserText = ''
                                                if (message.subject === '') {
                                                    teaserText = `Sem Assunto - ${message.body}`
                                                }
                                                else {
                                                    teaserText = `${message.subject} - ${message.body}`
                                                }

                                                return (
                                                    message.seen === true
                                                        ?
                                                        <tr style={styles.fadeInDown} onClick={() => handleSeeMessage(message.id, message.seen)}>
                                                            <td style={{ fontWeight: "450" }}>{message.fromName}</td>
                                                            <td>
                                                                {
                                                                    teaserText.length > 80
                                                                        ?
                                                                        `${teaserText.slice(0, 80)}...`
                                                                        :
                                                                        teaserText
                                                                }
                                                            </td>
                                                            <td>{date + " " + hora.join(':')}</td>
                                                        </tr>
                                                        :
                                                        <tr
                                                            className="unread-inbox"
                                                            style={styles.fadeInDown}
                                                            onClick={() => handleSeeMessage(message.id, message.seen)}>
                                                            <td>{message.fromName}</td>
                                                            <td>
                                                                {
                                                                    teaserText.length > 80
                                                                        ?
                                                                        `${teaserText.slice(0, 80)}...`
                                                                        :
                                                                        teaserText
                                                                }
                                                            </td>
                                                            <td>{dateOrganizeHelper(message.received_at)}</td>
                                                        </tr>
                                                )
                                            })
                                        }
                                    </tbody>
                                    :
                                    <i className='text-info'>Sem Emails...</i>
                            )
                            :
                            (
                                messages?.length !== 0
                                    ?
                                    <tbody style={{ cursor: "pointer", fontSize: "11pt" }}>
                                        {

                                            messages?.map(message => {
                                                let date = message.dataHora.split('T')
                                                let hora = date.pop()
                                                hora = hora.split(':');
                                                hora.pop();
                                                let teaserText = `${message.assunto} - ${message.texto}`


                                                return (
                                                    <tr style={{...styles.fadeInDown, background:"#FBFCFC "}} 
                                                    onClick={() => handleSeeMessage(message.idMensagem)}
                                                    >
                                                        <td style={{ fontWeight: "normal" }}>{message.autorEmail}</td>
                                                        <td>
                                                            {
                                                                teaserText.length > 80
                                                                    ?
                                                                    `${teaserText.slice(0, 80)}...`
                                                                    :
                                                                    teaserText
                                                            }
                                                        </td>
                                                        <td>{date + " " + hora.join(':')}</td>
                                                    </tr>

                                                )
                                            })
                                        }

                                    </tbody>
                                    :
                                    <i className='text-info'>Sem Emails...</i>
                            )
                    )
            }
        </Table>
    );
}

export default ListComponent;