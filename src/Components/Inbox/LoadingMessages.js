import React from 'react';

import loading_messages from '../../Images/emails_loading.gif';
import no_internet from '../../Images/no_internet.gif';

import {
    Card,
    CardBody,
    CardTitle,
    Row,
    Col
} from "reactstrap";

function LoadingMessages({ flag }) {
    return (
        <CardBody>
            {
                flag === 'loading'
                    ?
                    (
                        <>
                            <p className="d-flex justify-content-center mt-4">A carregar os emails...</p>
                            <p className="d-flex justify-content-center mt-4"><img src={loading_messages} width="300px" /></p>
                        </>
                    )
                    :
                    <p className="d-flex justify-content-center mt-4"><img src={no_internet} width="300px" /></p>


            }
        </CardBody>
    );
}

export default LoadingMessages;