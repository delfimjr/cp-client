import React from 'react'

import { withRouter } from 'react-router-dom'
import { mutate } from 'swr'

import QuestionModal from '../ShareQuestionModal/index'
import Loading from '../Loading/'

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { pulse } from 'react-animations';
import Radium, { StyleRoot } from 'radium';

import './inbox.css'

import {
    FaRegFilePdf,
    FaRegFileAlt,
    FaRegFileExcel,
    FaRegFileWord,
    FaRegFileImage,
    FaRegFilePowerpoint,
    FaRegFileArchive,
} from 'react-icons/fa'

import {
    CardBody,
    CardTitle,
} from "reactstrap";

import { FiShare, FiArrowLeft, FiTrash2, FiUser } from 'react-icons/fi'


import history from '../../history'
import api from '../../Services/api'
import { API_URL } from '../../helpers/api';
import dateOrganizeHelper from '../../helpers/dateOrganizeHelper'
import {getHeaderWithToken} from "../../helpers/authHelper";

//CONSTS
const styles = {
    pulse: {
        animation: 'x 0.7s',
        animationName: Radium.keyframes(pulse, 'pulse')
    }
}

class SingleMessage extends React.Component {
    constructor() {
        super()

        this.state = {
            showQuestion: false,
            openedMessage: '',
            loading: true,
            loadingOnDelete: false
        }
    }

    async UNSAFE_componentWillMount() {
        const id = this.props.match.params.id

        await api.get(`mailBox/${id}`, {headers: getHeaderWithToken()}).then(
            response => {
                console.log(response.data)
                this.setState({ openedMessage: response.data })
                this.setState({ loading: false })
            }
        )
            .catch(() => {
                this.setState({ loading: false })
                toast.error('❌ Não foi possível carregar a mensagem. Tente novamente ou verifique a conexão')
            })
    }

    handleDeleteMessage = async id => {
        api.delete(`delete_inbox/${id}`, {headers: getHeaderWithToken()})
            .catch(() => toast.error('❌ Não foi possível excluir a mensagem. Tente novamente ou verifique a conexão'))

        const messages = this.props.location.state.messages

        const updatedMessages = messages.filter(msg => msg.ID !== id)
        mutate('exchangeMail', { messages: updatedMessages, total: messages.lenght - 1 }, false)

        history.goBack()
        toast.info('Mensagem excluida')

    }

    handleBack = () => {
        history.goBack()
    }

    handleShowQuestion = (estado) => {
        this.setState({ showQuestion: estado })
    }

    setShowModal = (estado) => {
        this.setState({ showQuestion: estado })
    }

    render() {
        const { openedMessage, loadingOnDelete, loading } = this.state

        return (
            <>
                <ToastContainer />
                < StyleRoot >
                    <CardTitle className="container p-3">
                        {
                            (loading)
                                ?
                                <div className="d-flex justify-content-center">
                                    <span className="spinner-border text-primary"/>
                                    <span className='text-center'>Aguarde, por favor...</span>
                                </div>
                                :
                                (
                                    <>
                                        <div className="row justify-content-between">
                                            <div>
                                                <h5 className="title-single-message">
                                                    {
                                                        openedMessage.subject !== ''
                                                            ?
                                                            openedMessage.subject
                                                            :
                                                            'Sem Assunto'

                                                    }
                                                </h5>
                                            </div>
                                            <div className="title-icons">
                                                <FiArrowLeft
                                                    onClick={this.handleBack}
                                                    className="mr-3 arrowLeft"
                                                    size={20}
                                                    data-toggle="tooltip"
                                                    data-placement="bottom"
                                                    title="voltar a caixa de entrada"
                                                    style={{ cursor: "pointer" }}
                                                />
                                                <FiShare
                                                    onClick={() => this.handleShowQuestion(true)}
                                                    className="mr-3 share"
                                                    size={20}
                                                    data-toggle="tooltip"
                                                    data-placement="bottom"
                                                    title="partilhar"
                                                    style={{ cursor: "pointer" }}
                                                />
                                                <FiTrash2
                                                    onClick={() => this.handleDeleteMessage(openedMessage.id)}
                                                    className="trash"
                                                    size={20}
                                                    data-toggle="tooltip"
                                                    data-placement="bottom"
                                                    title="apagar a mensagem"
                                                    style={{ cursor: "pointer" }}
                                                />
                                            </div>
                                        </div>
                                        <div className="row justify-content-between mt-2">
                                            <div>
                                                <h6>{openedMessage.fromEmail}</h6>
                                            </div>
                                            <div>
                                                <h6 style={{ fontWeight: 'normal', color: '#7F8C8D' }}>
                                                    {dateOrganizeHelper(openedMessage.received_at)}
                                                </h6>
                                            </div>
                                        </div>
                                        <div className="row justify-content-between">
                                            <div className="col-sm-6">
                                                <FiUser />&nbsp;({openedMessage.fromName})
                                        </div>
                                        </div>
                                        <div className="col-sm-6 mt-2">
                                            {openedMessage?.attachments && <h6>Attachments</h6>}
                                            {
                                                (openedMessage?.attachments)
                                                    ?
                                                    (
                                                        openedMessage.attachments.map(att => {
                                                            if (att.name.indexOf('.xlsx') !== -1) {
                                                                return (
                                                                    <a className='row decoration' href={`${API_URL}/files/${att.name}`} target="">
                                                                        <button className="att-button">
                                                                            <FaRegFileExcel style={{ color: '#2ECC71' }} /> {att.name.slice(14, att.name.length)}
                                                                        </button>

                                                                    </a>
                                                                )
                                                            }
                                                            else {
                                                                if (att.name.indexOf('.pptx') !== -1) {
                                                                    return (
                                                                        <a className='row decoration' href={`${API_URL}/files/${att.name}`} target="">
                                                                            <button className="att-button">
                                                                                <FaRegFilePowerpoint style={{ color: '#D35400' }} /> {att.name.slice(14, att.name.length)}
                                                                            </button>
                                                                        </a>
                                                                    )
                                                                }
                                                                else {
                                                                    if (att.name.indexOf('.doc') !== -1) {
                                                                        return (
                                                                            <a className='row decoration' href={`${API_URL}/files/${att.name}`} target="">
                                                                                <button className="att-button">
                                                                                    <FaRegFileWord style={{ color: '#3498DB' }} /> {att.name.slice(14, att.name.length)}
                                                                                </button>
                                                                            </a>
                                                                        )
                                                                    }
                                                                    else {
                                                                        if (att.name.indexOf('.pdf') !== -1) {
                                                                            return (
                                                                                <a className='row decoration' href={`${API_URL}/files/${att.name}`} target="">
                                                                                    <button className="att-button">
                                                                                        <FaRegFilePdf style={{ color: '#C0392B' }} /> {att.name.slice(14, att.name.length)}
                                                                                    </button>
                                                                                </a>
                                                                            )
                                                                        }
                                                                        else {
                                                                            if (att.name.indexOf('.png') !== -1 || att.name.indexOf('.jpeg') !== -1 || att.name.indexOf('.pjpeg') !== -1) {
                                                                                return (
                                                                                    <a className='row decoration' href={`${API_URL}/files/${att.name}`} target="">
                                                                                        <button className="att-button">
                                                                                            <FaRegFileImage style={{ color: '#F9E79F' }} /> {att.name.slice(14, att.name.length)}
                                                                                        </button>
                                                                                    </a>
                                                                                )
                                                                            }
                                                                            else {
                                                                                if (att.name.indexOf('.rar') !== -1 || att.name.indexOf('.zip') !== -1) {
                                                                                    return (
                                                                                        <a className='row decoration' href={`${API_URL}/files/${att.name}`} target="">
                                                                                            <button className="att-button">
                                                                                                <FaRegFileArchive style={{ color: '#78281F' }} /> {att.name.slice(14, att.name.length)}
                                                                                            </button>
                                                                                        </a>
                                                                                    )
                                                                                }
                                                                                else {
                                                                                    return (
                                                                                        <a className='row decoration' href={`${API_URL}/files/${att.name}`} target="">
                                                                                            <button className="att-button">
                                                                                                <FaRegFileAlt style={{ color: '#AEB6BF' }} /> {att.name.slice(14, att.name.length)}
                                                                                            </button>
                                                                                        </a>
                                                                                    )

                                                                                }
                                                                            }

                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        })

                                                    )

                                                    :
                                                    null
                                            }

                                        </div>
                                    </>
                                )
                        }

                    </CardTitle>
                    <CardBody className="">
                        <div className="body-card">
                            <pre style={styles.pulse} className='message-body'>
                                {openedMessage.body}
                            </pre>
                        </div>
                    </CardBody>
                    <QuestionModal
                        messageContent={openedMessage}
                        setShowModal={() => this.setShowModal()}
                        showQuestion={this.state.showQuestion}
                    />
                    {
                        (loadingOnDelete)
                            ?
                            <Loading />
                            :
                            null
                    }
                </StyleRoot >
            </>
        )
    }
}

export default withRouter(SingleMessage)
