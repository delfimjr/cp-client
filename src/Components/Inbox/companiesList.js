import React  from 'react';
import { useHistory } from 'react-router-dom';

import { useFetch } from '../../hooks/useFetch';
import LoadingMessages from './LoadingMessages';

import './inbox.css'


function CompaniesList() {
    const { push } = useHistory()
    const { data: companies, error } = useFetch('/mailBox/Emails/Company')

    function handleSeeMessagesList(key, emails, company) {
        push(`/dashboard/caixa-de-emails/company/${key}`, {
            messages: emails, company: company
        })
    }

    if (error) {

        return (
            <LoadingMessages flag="error" />
        )
    }


    if (!companies) {
        return (
            <LoadingMessages flag="loading" />
        )
    }
    //
    return (
        companies?.length > 0
            ?
            <div className="company-card">
                {
                    companies.map((company, i) => (
                        <div key={i + 1} onClick={() => handleSeeMessagesList(i + 1, company.emails, company.company)}>
                            <h5>{company.company.company}</h5>
                            <h6>{company.emails.length} Email(s)</h6>
                            <h6>{company.totalUnread} novo(s)</h6>
                        </div>
                    ))
                }
            </div>
            :
            <p className="text-center mt-5 font-weight-bold">Nenhuma mensagem proveniente de uma companhia</p>


    );
}

export default CompaniesList;
