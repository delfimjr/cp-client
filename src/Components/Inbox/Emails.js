import React, {useEffect, useState} from 'react';
import {useHistory, useLocation} from 'react-router-dom'
import {FiArrowLeft, FiChevronLeft, FiChevronRight} from 'react-icons/fi'

import api from '../../Services/api'
import './inbox.css'

import {StyleRoot} from 'radium';

import {CardBody, CardFooter} from "reactstrap";

import ListComponent from './ListComponent';
import LoadingMessages from './LoadingMessages';
import {getHeaderWithToken} from "../../helpers/authHelper";

//CONSTS
const LIMIT = 20

const Emails = (props) => {

    const history = useHistory()
    const {state} = useLocation()

    const [currentPage, setCurrentPage] = useState(1)
    const [totalPages, setTotalPages] = useState(1)
    const [searchField, setsearchField] = useState('')
    const [unread, setUnread] = useState('')
    const [totalEmails, setTotalEmails] = useState('')
    const [filteredMessages, setFilteredMessages] = useState([])

    //const { data, error, isValidating, mutate } = useFetch('mailBox')

    const indexOfLastPage = (currentPage > 0 && currentPage) * LIMIT
    const indexOfFirstPage = indexOfLastPage - LIMIT
    const messages = searchField === ''
        ?
        (
            state
                ?
                filteredByCompanyEmails().slice(indexOfFirstPage, indexOfLastPage)
                :
                props.data?.messages.slice(indexOfFirstPage, indexOfLastPage)
        )
        :
        filteredMessages

    console.log("STATE", state)
    console.log("MESSAGES", messages)

    useEffect(() => {
        if (props.data) {
            setUnread(props.data?.unread)
            setTotalEmails(props.data?.total)
            setTotalPages(Math.ceil((props.data?.total / LIMIT)))
        }
    }, [props.data])

    useEffect(() => {
        if (searchField !== '') {
            let filtered;
            if (state) {
                filtered = state.messages.filter(msg => {
                    return msg?.fromEmail?.toLowerCase().includes(searchField?.toLowerCase())
                        || msg?.subject?.toLowerCase().includes(searchField?.toLowerCase())
                        || msg?.body?.toLowerCase().includes(searchField?.toLowerCase())
                })
            } else {
                filtered = props.data.messages.filter(msg => {
                    return msg?.fromEmail?.toLowerCase().includes(searchField?.toLowerCase())
                        || msg?.subject?.toLowerCase().includes(searchField?.toLowerCase())
                        || msg?.body?.toLowerCase().includes(searchField?.toLowerCase())
                })
            }

            setFilteredMessages(filtered)
        }

    }, [searchField])

    function handleSearch(event) {
        setsearchField(event.target.value)
    }

    /*
        The emails filtered by Company comes from state const, declared in the beginning!
        And in the function bellow we just make sure that the Global data(emails list) 4
        from useFetch will only show the filtered emails
    */
    function filteredByCompanyEmails() {
        const ids = state.messages.map(msg => msg.id)
        const filteredEmails = ids.map(id => {
            const aux = props.data?.messages.filter(message => message.id === id)

            return props.data && aux[0]
        })
        return filteredEmails.filter(email => email && email)
    }

    async function handleSeeMessage(id, seen) {
        history.push(`/dashboard/caixa-de-emails/${id}`, {messages: props.data.messages})
        api.put(`mailBox/Message/${id}`,{}, {headers: getHeaderWithToken()})

        if (!seen) {
            const updatedMessages = messages?.map(message => {
                return message.id === id ? {...message, seen: true} : message
            })

            let unread = 0
            if (props.data.unread > 0) {
                unread = props.data.unread - 1
            }

            props.mutate({messages: updatedMessages, total: props.data.total, unread: unread}, false)
        }
    }

    if (props.error) {

        return (
            <LoadingMessages flag="error"/>
        )
    }


    if (!props.data) {
        return (
            <LoadingMessages flag="loading"/>
        )
    }

    return (
        <StyleRoot>
            <CardBody className="inbox_body">
                <div className="container">
                    {
                        state &&
                        <div className="row justify-content-end">
                            <FiArrowLeft
                                onClick={() => history.goBack()}
                                className="arrowLeft"
                                size={20}
                                data-toggle="tooltip"
                                data-placement="bottom"
                                title="voltar a lista de companhias"
                                style={{cursor: "pointer"}}
                            />
                        </div>
                    }
                    <div className="row justify-content-center">
                        <input
                            onChange={handleSearch}
                            className='form-control inbox-searchInput'
                            placeholder='procurar...'
                        />
                    </div>
                    <div className="row justify-content-end">
                        {
                            !state &&
                            (

                                (unread > 0)
                                    ?
                                    (
                                        (unread === 1)
                                            ? <span
                                                className='text-warning col-md-6'
                                                style={{fontWeight: 'bold'}}
                                            >
                                                {unread} email não lido
                                                        </span>
                                            :
                                            <span
                                                className='text-warning col-md-6'
                                                style={{fontWeight: 'bold'}}
                                            >
                                                {unread} emails não lidos
                                                 </span>
                                    )
                                    :
                                    <span className='text-success col-md-6' style={{fontWeight: 'bold'}}>Todos os emails foram lidos</span>
                            )
                        }
                    </div>
                    <div className="row justify-content-center">
                        <ListComponent messages={messages} handleSeeMessage={handleSeeMessage} inbox/>
                    </div>
                </div>
            </CardBody>
            <CardFooter style={{bottom: 0, paddingHorizontal: 0, position: 'absolute', background: 'white'}} className="container">
                <div className="row justify-content-between">
                    <div>
                        {
                            !totalEmails >= 0 && searchField === '' && !state
                                ?
                                <h6 className="totalMail-inbox">
                                    possui {totalEmails >= 0 && totalEmails} email(s)
                                </h6>
                                :
                                null
                        }
                    </div>
                    {
                        searchField === ''
                        &&
                        <div className="inbox-pagination">
                            {
                                state
                                    ?
                                    `${indexOfFirstPage + 1} - ${indexOfLastPage > state.messages.length ? state.messages.length : indexOfLastPage} de ${props.data && state.messages.length}`
                                    :
                                    `${indexOfFirstPage + 1} - ${indexOfLastPage > totalEmails ? totalEmails : indexOfLastPage} de ${props.data?.total}`
                            }

                            {
                                currentPage > 1
                                    ?
                                    <FiChevronLeft
                                        onClick={() => setCurrentPage(currentPage - 1)}
                                        className="inbox-pagination-button margin"
                                        size={20}
                                    />
                                    :
                                    <FiChevronLeft
                                        className="margin"
                                        color={`#616A6B`}
                                        size={20}
                                    />
                            }
                            {
                                state
                                    ?
                                    props.data && state.messages.length > indexOfLastPage
                                        ?
                                        <FiChevronRight
                                            onClick={() => setCurrentPage(currentPage + 1)}
                                            className="inbox-pagination-button"
                                            size={20}
                                        />
                                        :
                                        <FiChevronRight
                                            color={`#616A6B`}
                                            size={20}
                                        />
                                    :
                                    props.data?.messages.length > indexOfLastPage
                                        ?
                                        <FiChevronRight
                                            onClick={() => setCurrentPage(currentPage + 1)}
                                            className="inbox-pagination-button"
                                            size={20}
                                        />
                                        :
                                        <FiChevronRight
                                            color={`#616A6B`}
                                            size={20}
                                        />
                            }
                        </div>
                    }

                </div>
                {
                    /*state
                        ?
                        <>
                            <FiHome color="#99A3A4" size={18} />
                            <h6>{`Companhia: ${state.company.company}`}</h6>
                        </>
                        :
                        <h6>Principal</h6>*/
                }
            </CardFooter>
        </StyleRoot>
    )
}

export default Emails
