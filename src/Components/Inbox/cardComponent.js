import React from 'react';
import { Row, Col, Card } from 'reactstrap';

import './inbox.css'

const CardComponent = ({ children }) => {
    return (
        <Row>
            <Col md="12">
                <Card className="shadow-sm p-3 cardHeight-inbox ">
                    {children}
                </Card>
            </Col>
        </Row>
    )
}

export default CardComponent;