import React, { useState, useEffect } from 'react'
import { Switch, Route, useHistory, useLocation } from 'react-router-dom'

import { CardTitle } from 'reactstrap'
import { FiMail, FiUsers } from 'react-icons/fi'

import Emails from './Emails'
import SingleMessage from './SingleMessage'
import CardComponent from './cardComponent'
import CompaniesList from './companiesList'



const Inbox = (props) => {
    const { push } = useHistory()
    const { pathname } = useLocation()

    const [tabs, setTabs] = useState([
        { title: 'Todos', active: true, icon: 'inbox' },
        { title: 'Por Companhia', active: false, icon: 'groups' }
    ])
    const [tabContent, setTabContent] = useState('all')

    useEffect(() => {
        props.handlePageTitleChange('Caixa de Entrada')
        if (!pathname.includes('/dashboard/caixa-de-emails')) {
            const updatedTabs = tabs.map(tab => (
                tab.title === tabs[1].title
                    ?
                    { ...tab, active: true }
                    :
                    { ...tab, active: false }
            ))
            setTabs(updatedTabs)
        }
    }, [])

    function handleActiveTab(title, isActive) {
        if (!isActive) {
            const updatedTabs = tabs.map(tab => (
                tab.title === title
                    ?
                    { ...tab, active: true }
                    :
                    { ...tab, active: false }
            ))
            setTabs(updatedTabs)

            if (title !== 'Todos') {
                setTabContent('company')
                push("/dashboard/caixa-de-emails/company")
            }
            else {
                setTabContent('all')
                push("/dashboard/caixa-de-emails")
            }

        }

    }

    return (
        <>
            <CardComponent>
                <CardTitle>
                    <div className="inbox-tab">
                        {
                            tabs.map(tab => (
                                tab.active
                                    ?
                                    <div onClick={() => handleActiveTab(tab.title, tab.active)} className="active">
                                        <h5>{tab.title}</h5> &nbsp; &nbsp;
                                                <h6>
                                            {tab.icon === 'inbox' ? <FiMail size={20} /> : <FiUsers size={20} />}
                                        </h6>
                                    </div>
                                    :
                                    <div onClick={() => handleActiveTab(tab.title, tab.active)}>
                                        <h5>{tab.title}</h5> &nbsp; &nbsp;
                                                <h6>
                                            {tab.icon === 'inbox' ? <FiMail size={20} /> : <FiUsers size={20} />}
                                        </h6>
                                    </div>
                            ))
                        }
                    </div>
                </CardTitle>
                <Switch>
                    <Route
                        exact
                        path={["/dashboard/caixa-de-emails", "/dashboard/caixa-de-emails/company/:id"]}
                        render={() =>
                            <Emails
                                userObject={props.userObject}
                                handlePageTitleChange={props.handlePageTitleChange}
                                data={props.data}
                                error={props.error}
                                isValidating={props.isValidating}
                                mutate={props.mutate}
                            />}
                    />
                    <Route path="/dashboard/caixa-de-emails/company" component={CompaniesList} />
                    <Route path="/dashboard/caixa-de-emails/:id" component={SingleMessage} />
                </Switch>
            </CardComponent>
        </>
    )
}

export default Inbox