import React, { Component } from 'react'
import axios from 'axios';
import xlsxParser from 'xlsx-parse-json'
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export default class FileInput extends Component {

    constructor(props) {
        super(props)
        this.state = {
            selectedFile: null
        }
    }

    handleVerify = async (event) => {
        const { updateParsedExcelFile } = this.props
        let file = event.target.files
        let excelFile = document.getElementById('excel_input')

        let pos = file[0].name.indexOf('.xlsx')
        if (pos <= -1) {
            alert("Not permited file type!")
            excelFile.value = "";
        }
        else {

            xlsxParser
                .onFileSelection(file[0])
                .then(data => {
                    let parsedData = data
                    if (parsedData.Sheet1.length === 0) {
                        toast.error('O ficheiro carregado está vazio ou não obedece ao padrão')
                        
                        excelFile.value = "";
                    }
                    else {
                        updateParsedExcelFile(parsedData)
                    }
                })
                .catch((err) => console.log(err))
        }
    }

    render() {
        return (
            <>
                <input type="file" id="excel_input" onChange={this.handleVerify} />
            </>
        )
    }
}