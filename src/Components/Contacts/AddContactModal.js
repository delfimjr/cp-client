import React, {Component} from 'react'
import {FiUserPlus} from 'react-icons/fi'
import Modal from 'react-bootstrap/Modal'
import validator from 'validator';
import {toast} from 'react-toastify';

import InputFile from './FileInput'

import 'react-toastify/dist/ReactToastify.css';

import api from '../../Services/api'
import './styles.css'
import Loading from '../Loading';
import {API_URL} from '../../helpers/api';
import {getHeaderWithToken} from "../../helpers/authHelper";

export default class AddNewContact extends Component {
    constructor() {
        super()
        this.state = {
            formInput: {
                name: '',
                lastname: '',
                email: '',
                cell: '',
                whatsapp: '',
                gender: '',
                department: '',
                company: '',
                designation: '',
                groupName: ''
            },
            contactType: 'contactType',
            fileInputShown: false,
            emptinessAlert: false,
            nameEmptyBorder: '',
            lastNameEmptyBorder: '',
            genderEmptyBorder: '',
            departmentEmptyBorder: '',
            companyEmptyBorder: '',
            invalidEmailBorder: '',
            warningMessage: '',
            cellBorder: '',
            parsedExcelFile: [],
            loading: false,
            addContactToGroup: false,
            groups: [],
            chosenGroupId: ''
        }
    }

    componentWillMount() {
        api.get('groups', {headers: getHeaderWithToken()}).then(response => {
            this.setState({groups: response.data})
        })
    }

    //This method is responsible for showing and hidding the checkbox input
    handleFileInput = () => {
        let input = document.getElementById('check');
        if (input.checked) {
            this.setState({fileInputShown: true})
            console.log(this.state.fileInputShown)
        } else {
            this.setState({fileInputShown: false})
        }
    }

    //This method goes to FileInput class as props so that the parsedExcelFile must be
    //updated according with the excell content "uploaded" by the user.
    updateParsedExcelFile = async (jsonText) => {
        this.setState({parsedExcelFile: jsonText.Sheet1})
        toast.info("Ficheiro Carregado. Agora, clicar em registar")
    }

    //Based on flag means based on what is missing on user input or excel file
    //Basically this method cdefines the system output message for each missing I(and required) input
    validateBasedOnFlag = (flag) => {
        const {parsedExcelFile, fileInputShown} = this.state

        const {
            name,
            lastname,
            gender,
            department,
            company,
        } = this.state.formInput

        const requiredFlagExtern = flag.indexOf('requiredExtern')
        const bothEmptyFlagExtern = flag.indexOf('bothEmptyExtern')

        const requiredFlagIntern = flag.indexOf('requiredIntern')
        const bothEmptyFlagIntern = flag.indexOf('bothEmptyIntern')

        const invalidEmailIntern = flag.indexOf('invalidEmailIntern')

        if (requiredFlagExtern !== -1 || bothEmptyFlagExtern !== -1) {
            if (requiredFlagExtern > -1 && bothEmptyFlagExtern > -1) {
                if (parsedExcelFile.length > 0 && fileInputShown) {
                    toast.error('Name, LastName, Genero e Companhia devem ser preenchidos e email e celular nao podem estar ambos vazios. Verifique o ficheiro')
                } else {
                    if (name === '') {
                        this.setState({nameEmptyBorder: 'border border-warning'})
                    }
                    if (lastname === '') {
                        this.setState({lastNameEmptyBorder: 'border border-warning'})
                    }
                    if (gender === '') {
                        this.setState({genderEmptyBorder: 'border border-warning'})
                    }

                    if (company === '') {
                        this.setState({companyEmptyBorder: 'border border-warning'})
                    }

                    this.setState({warningMessage: 'preencha, por favor, os campos marcados'})
                    this.setState({emptinessAlert: true})
                }
            } else {
                if (requiredFlagExtern > -1) {
                    if (parsedExcelFile.length > 0 && fileInputShown) {
                        toast.error('Name, LastName, Genero e Companhia devem ser preenchidos e email e celular nao podem estar ambos vazios. Verifique o ficheiro')
                    } else {
                        if (name === '') {
                            this.setState({nameEmptyBorder: 'border border-warning'})
                        }
                        if (lastname === '') {
                            this.setState({lastNameEmptyBorder: 'border border-warning'})
                        }
                        if (gender === '') {
                            this.setState({genderEmptyBorder: 'border border-warning'})
                        }

                        if (company === '') {
                            this.setState({companyEmptyBorder: 'border border-warning'})
                        }

                        this.setState({warningMessage: 'preencha, por favor, os campos marcados'})
                        this.setState({emptinessAlert: true})
                    }
                } else {
                    this.setState({emptinessAlert: true})
                    this.setState({warningMessage: 'preencha pelo menos um dos campos marcados'})
                    this.setState({cellBorder: 'border border-warning'})
                    this.setState({invalidEmailBorder: 'border border-warning'})
                }
            }
        } else {
            if (requiredFlagIntern !== -1 || bothEmptyFlagIntern !== -1) {
                if (requiredFlagIntern > -1 && bothEmptyFlagIntern > -1) {
                    if (parsedExcelFile.length > 0 && fileInputShown) {
                        toast.error('Name, LastName, Genero e Departamento devem ser preenchidos e email e celular nao podem estar ambos vazios. Verifique o ficheiro')

                    } else {
                        if (name === '') {
                            this.setState({nameEmptyBorder: 'border border-warning'})
                        }
                        if (lastname === '') {
                            this.setState({lastNameEmptyBorder: 'border border-warning'})
                        }
                        if (gender === '') {
                            this.setState({genderEmptyBorder: 'border border-warning'})
                        }

                        if (department === '') {
                            this.setState({departmentEmptyBorder: 'border border-warning'})
                        }

                        this.setState({warningMessage: 'preencha, por favor, os campos marcados'})
                        this.setState({emptinessAlert: true})
                    }
                } else {
                    if (requiredFlagIntern > -1) {
                        if (parsedExcelFile.length > 0 && fileInputShown) {
                            toast.error('Name, LastName, Genero e Companhia devem ser preenchidos e email e celular nao podem estar ambos vazios. Verifique o ficheiro')
                        } else {
                            if (name === '') {
                                this.setState({nameEmptyBorder: 'border border-warning'})
                            }
                            if (lastname === '') {
                                this.setState({lastNameEmptyBorder: 'border border-warning'})
                            }
                            if (gender === '') {
                                this.setState({genderEmptyBorder: 'border border-warning'})
                            }

                            if (company === '') {
                                this.setState({departmentEmptyBorder: 'border border-warning'})
                            }
                            this.setState({emptinessAlert: true})
                            this.setState({warningMessage: 'preencha, por favor, os campos marcados'})
                        }
                    } else {
                        this.setState({emptinessAlert: true})
                        this.setState({warningMessage: 'preencha pelo menos um dos campos marcados'})
                        this.setState({cellBorder: 'border border-warning'})
                        this.setState({invalidEmailBorder: 'border border-warning'})
                    }
                }
            } else {
                if (invalidEmailIntern !== -1) {
                    this.setState({invalidEmailBorder: 'border border-warning'})
                    this.setState({emptinessAlert: true})
                    this.setState({warningMessage: 'O email introduzido é inválido!'})
                } else {
                    toast.error('O email XXX é inválido')
                }
            }
        }

    }

    //In the Excell File, the name and lastname are required
    //And the email & cell cannot be both empty
    //Essencially, this method just turn on(or off) the flag variable declared
    //at "updateParsedExcelFile" function.
    validateData = (data) => {

        var answer = ''

        if (this.state.contactType === 'Interno') {
            data.forEach(item => {
                if (!item.name || !item.lastname || !item.gender || item.gender === 'Género' || !item.department) {
                    answer += 'requiredIntern'
                } else {

                    //In a Excelfile at least one of them must be filled
                    if (!item.email && !item.cell) {
                        answer += 'bothEmptyIntern'
                    } else {
                        if (item.email !== '') {
                            const isEmail = validator.isEmail(item.email)
                            if (!isEmail) {
                                answer += 'invalidEmailIntern'
                            }
                        }
                    }
                }
            })
            return answer;
        } else {
            data.forEach(item => {
                if (!item.name || !item.lastname || !item.gender || item.gender === 'Género' || !item.company) {
                    answer += 'requiredExtern'
                } else {
                    //In a Excelfile at least one of them must be filled
                    if (!item.email && !item.cell) {
                        answer += 'bothEmptyExtern'
                    } else {
                        if (item.email !== '') {
                            const isEmail = validator.isEmail(item.email)
                            if (!isEmail) {
                                answer += 'invalidEmailIntern'
                            }
                        }
                    }
                }
            })
            return answer;
        }
    }

    handleOnChangeInput = async (event) => {
        const {name, value} = event.target

        await this.setState({formInput: {...this.state.formInput, [name]: value}})

        if (name === 'name') {
            this.setState({nameEmptyBorder: ''})
        }

        if (name === 'lastname') {
            this.setState({lastNameEmptyBorder: ''})
        }

        if (name === 'gender') {
            this.setState({genderEmptyBorder: ''})
        }

        if (name === 'company') {
            this.setState({companyEmptyBorder: ''})
        }

        if (name === 'department') {
            this.setState({departmentEmptyBorder: ''})
        }

        if (name === 'email') {
            this.setState({invalidEmailBorder: ''})
        }

        if (name === 'whatsapp') {
            this.setState({cellBorder: ''})
        }


        this.setState({emptinessAlert: false})
        this.setState({warningMessage: ''})
    }

    // handleContactType = async (event) => {
    //   const { name, value } = event.target
    //   await this.setState({ [name]: value })
    // }

    handleNumberFormatValidation = async (numbers) => {
        var result = '';


        for (const number of numbers) {
            const result1 = await fetch(`http://apilayer.net/api/validate?access_key=c25b46b053622e9d7e1755f7f2abcacc&number=${number}`)

            const result2 = await result1.json()

            if (!result2.valid) {
                result = {valid: result2.valid, number: result2.number}
            }
        }
        return result
    }

    handleStore = async (event) => {
        event.preventDefault()

        const {parsedExcelFile, formInput, addContactToGroup, chosenGroupId} = this.state
        if (addContactToGroup) {
            if (!chosenGroupId && !formInput.groupName) {
                toast.warn("You need to specify a group!");
            }
        }

        let dataToStore = parsedExcelFile?.length ? parsedExcelFile : [formInput]

        let numbers = [];
        let dataToStoreAux = [];
        for (const contact of dataToStore) {
            if (contact?.cell) {
                if ((!contact.cell.includes("+258") || !contact.cell.includes("258"))) {
                    numbers.push(`+258${contact.cell}`)
                    dataToStoreAux.push({...contact, cell: `+258${contact.cell}`})
                } else {
                    numbers.push(contact.cell)
                    dataToStoreAux.push(contact)
                }
            } else {
                dataToStoreAux.push(contact)
            }
        }

        dataToStore = dataToStoreAux;

        this.setState({loading: true})
        /*const numberFormatValidationResult = await this.handleNumberFormatValidation(numbers)
        this.setState({ loading: false })

        if (numberFormatValidationResult !== '') {
          toast.warn(`O numero de celular ${numberFormatValidationResult.number} é inválido.`)
          return
        }*/

        let cantBeBothEmptyDepartmentCompany = false;
        let cantBeBothEmptyEmailCell = false;
        let cantBeBasicInfoEmpty = false

        for (const contact of dataToStore) {
            if (!contact?.department && !contact?.company) {
                cantBeBothEmptyDepartmentCompany = true;
            }
            if (!contact?.cell && !contact?.email) {
                cantBeBothEmptyEmailCell = true
            }
            if (!contact?.name || !contact?.lastname || !contact?.gender) {
                cantBeBasicInfoEmpty = true
            }
        }

        if (cantBeBothEmptyDepartmentCompany) {
            toast.warn(`O contacto deve possuir departamento ou companhia. Por favor, certifique-se`)
            this.setState({loading: false});
            return
        } else {
            if (cantBeBothEmptyEmailCell) {
                toast.warn(`O contacto deve possuir celular ou email para que seja possivel enviar informcao. Por favor, certifique-se`)
                this.setState({loading: false});
                return
            } else {
                if (cantBeBasicInfoEmpty) {
                    toast.warn(`Nome, Celular e Genero nao devem estar vazios. Por favor, certifique-se`)
                    this.setState({loading: false});
                    return
                }
            }
        }

        this.setState({loading: true})

        const auxDataToStore = dataToStore.map(data => (
            {
                name: data.name,
                lastName: data.lastname,
                email: data.email,
                phone: data.cell,
                department: data.department,
                company: data.company,
                designation: data.designation,
                gender: data.gender
            }
        ))

        api.post('contacts', {contacts: auxDataToStore, groupId: chosenGroupId, groupName: formInput.groupName}, {headers: getHeaderWithToken()})
            .then(() => {
                this.setState({loading: false})
                this.props.updateContacts()
                this.props.handleAddContact(false, 'save')
            })
            .catch((err) => {
                this.setState({loading: false})
                if (err?.response?.data?.error) {
                    toast.error(err?.response?.data?.error)
                } else {
                    toast.error('❌ Não foi possível criar o grupo. Tente novamente ou verifique a conexão')
                }
            })

    }

    onChangeGroup = (event) => {
        this.setState({chosenGroupId: event.target.value})
    }

    render() {

        const {showAddNewContact} = this.props
        const {
            nameEmptyBorder,
            lastNameEmptyBorder,
            genderEmptyBorder,
            companyEmptyBorder,
            departmentEmptyBorder,
            warningMessage,
            invalidEmailBorder,
            cellBorder,
            fileInputShown,
            loading,
            addContactToGroup
        } = this.state

        return (
            <>
                <Modal
                    centered
                    size="lg"
                    show={showAddNewContact}
                    onHide={() => this.props.handleAddContact(false, 'close')}
                >
                    <Modal.Header style={{backgroundColor: '#FBFCFC'}} closeButton>
                        <div className="container">
                            <div className="row justify-content-center">
                                <h4 className=''>Novo Contacto</h4>
                            </div>
                        </div>
                    </Modal.Header>

                    <Modal.Body>
                        <div className="container">
                            <div className='row justify-content-end'>
                            </div>
                            <div className="flexed row justify-content-center">
                                <header>
                                    <FiUserPlus
                                        className="icon-register"
                                        size={70}
                                        color="#B2BABB"
                                    />
                                    {
                                        this.state.emptinessAlert
                                        &&
                                        <div
                                            className="message mt-2 mb-4 text-center"
                                            style={{background: '#F7DC6F', borderRadius: "2px", fontSize: "10.5pt"}}
                                        >
                                            {warningMessage}
                                        </div>
                                    }
                                </header>
                            </div>
                        </div>

                        <form onSubmit="">
                            <div className="container mt-3" style={{width: ''}}>

                                <>
                                    <div className='row justify-content-center'>
                                        <div className="col-sm-4">
                                            <input
                                                onChange={this.handleOnChangeInput}
                                                className={`form-control register-contacts ${nameEmptyBorder}`}
                                                type="text"
                                                name="name"
                                                id=""
                                                placeholder='Introduzir o Nome'
                                                disabled={fileInputShown}
                                            />
                                        </div>
                                        <div className="col-sm-4">
                                            <input
                                                onChange={this.handleOnChangeInput}
                                                className={`form-control register-contacts ${lastNameEmptyBorder}`}
                                                type="text"
                                                name="lastname"
                                                id=""
                                                placeholder='Introduzir o Apelido'
                                                disabled={fileInputShown}
                                            />
                                        </div>
                                    </div>

                                    <div className='row justify-content-center mt-3'>
                                        <div className="col-sm-4">
                                            <select
                                                onChange={this.handleOnChangeInput}
                                                name="gender"
                                                id=""
                                                className={`form-control gender register-contacts ${genderEmptyBorder}`}
                                                disabled={fileInputShown}
                                            >
                                                <option value="Gender">Género</option>
                                                <option value="F">Femenino</option>
                                                <option value="M">Masculino</option>
                                            </select>
                                        </div>
                                        <div className="col-sm-4">
                                            <input
                                                onChange={this.handleOnChangeInput}
                                                className={`${cellBorder} form-control register-contacts`}
                                                type="text"
                                                name="cell"
                                                id=""
                                                placeholder='Celular(ex: 840396628)'
                                                disabled={fileInputShown}
                                            />
                                        </div>
                                    </div>

                                    <div className='row justify-content-center mt-3'>
                                        <div className="col-sm-8">
                                            <input
                                                onChange={this.handleOnChangeInput}
                                                className={`${invalidEmailBorder} form-control register-contacts`}
                                                type="email"
                                                name="email"
                                                id=""
                                                placeholder='Introduzir o E-mail'
                                                disabled={fileInputShown}
                                            />
                                        </div>
                                    </div>
                                </>

                                <div className='row justify-content-center mt-3'>
                                    <div className="col-sm-8">
                                        <input
                                            onChange={this.handleOnChangeInput}
                                            className={`form-control register-contacts ${departmentEmptyBorder}`}
                                            type="text"
                                            name="department"
                                            id=""
                                            placeholder='Departamento'
                                            disabled={fileInputShown}
                                        />
                                    </div>
                                </div>

                                <div className='row justify-content-center mt-3'>
                                    <div className="col-sm-4">
                                        <input
                                            onChange={this.handleOnChangeInput}
                                            className={`form-control register-contacts ${companyEmptyBorder}`}
                                            type="text"
                                            name="company"
                                            id=""
                                            placeholder='Companhia'
                                            disabled={fileInputShown}
                                        />
                                    </div>
                                    <div className="col-sm-4">
                                        <input
                                            onChange={this.handleOnChangeInput}
                                            className='form-control register-contacts'
                                            type="text"
                                            name="designation"
                                            id=""
                                            placeholder='Cargo/Função (opcional)'
                                            disabled={fileInputShown}
                                        />
                                    </div>
                                </div>

                                <div style={{display: "flex", flexDirection: "column", justifyContent: "center"}}
                                     className='mt-3'>
                                    <div style={{display: "flex", flexDirection: "column", alignItems: "center"}}>
                                        <p className="text-info">Adicionar o(s) novo(s) contacto(s) em um grupo?</p>
                                        <div>
                                            <label>
                                                <input checked={addContactToGroup && "checked"}
                                                       onChange={() => this.setState({addContactToGroup: !addContactToGroup})}
                                                       type="radio" name="group"/>Sim
                                            </label>
                                            <label><input checked={!addContactToGroup && "checked"}
                                                          onChange={() => this.setState({addContactToGroup: !addContactToGroup})}
                                                          type="radio" name="group"/>Não</label>
                                        </div>
                                        {
                                            addContactToGroup
                                            &&
                                            <div className="text-center">
                                                <select onChange={this.onChangeGroup}
                                                        className='form-control col-sm-12'>
                                                    <option value=''>Select an existing group</option>
                                                    {
                                                        this.state.groups.map(group => (
                                                            <option value={group.id}>{group.name}</option>
                                                        ))
                                                    }
                                                </select>
                                                <p>Ou</p>
                                                <input
                                                    onChange={this.handleOnChangeInput}
                                                    className="form-control"
                                                    type="text"
                                                    name="groupName"
                                                    value={this.state.formInput.groupName}
                                                    placeholder="Adicionar novo grupo"
                                                />
                                            </div>
                                        }
                                    </div>
                                    <div className="col-sm-12 text-center">
                                        <input
                                            className='mr-2'
                                            type="checkbox"
                                            name="nome"
                                            id="check"
                                            value=''
                                            onChange={this.handleFileInput}
                                        />
                                        <label className='col-form-label' htmlFor="fileInput">Pretende carregar ficheiro
                                            excel?</label>
                                    </div>
                                </div>
                                {
                                    (fileInputShown)
                                        ?
                                        (
                                            <div className='mt-3' style={{
                                                display: 'flex',
                                                flexDirection: 'column',
                                                alignItems: 'center'
                                            }}>
                                                {/*<div>
                                                    <a className='decoration text-info'
                                                       href={`${API_URL}/files/1619706833200-ExcelTemplate.xlsx`}
                                                       target="">
                                                        Baixar Ficheiro Modelo
                                                    </a>
                                                </div>*/}
                                                <div className="col-sm-4 ">
                                                    <InputFile updateParsedExcelFile={this.updateParsedExcelFile}/>
                                                </div>
                                            </div>
                                        )
                                        : null
                                }
                            </div>
                        </form>
                    </Modal.Body>
                    <Modal.Footer>

                        <button
                            onClick={this.handleStore}
                            className='btn btn-primary'
                            type="submit"
                        >
                            Registar
                        </button>

                    </Modal.Footer>
                    {
                        (loading)
                            ?
                            <Loading/>
                            :
                            null
                    }
                </Modal>

            </>


        )
    }
}
