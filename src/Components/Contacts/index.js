import React, {Component} from 'react'
import {Route, Link, Switch} from 'react-router-dom'
import {FaPlus, FaRegTrashAlt, FaRegEdit, FaFileContract} from 'react-icons/fa'
import {FiList} from 'react-icons/fi'
import {ToastContainer, toast} from 'react-toastify';
import {Table, Col, Row, Card, CardBody, CardTitle} from 'reactstrap'


import history from '../../history'
import EditContactModal from './EditContactModal'
import AddContactModal from './AddContactModal'

import api from '../../Services/api'

import 'react-toastify/dist/ReactToastify.css';
import './styles.css'
import {getHeaderWithToken} from "../../helpers/authHelper";

export default class Contacts extends Component {
    constructor() {
        super()
        this.state = {
            showAddNewContact: false,
            contacts: [],
            selectContactId: '',
            searchfield: '',
            selectedContactType: '',
            loading: true,
        }
    }

    UNSAFE_componentWillMount() {
        this.props.handlePageTitleChange('Contactos')
        this.fetchContacts()
    }

    fetchContacts = () => {
        api.get(`contacts`, {headers: getHeaderWithToken()}).then(response => {
            this.setState({contacts: response.data})
            this.setState({loading: false})
        })
            .catch(() => {
                this.setState({loading: false})
            })
    }

    handleDelete = async id => {
        const {contacts} = this.state

        await api.delete(`contacts/${id}`, {headers: getHeaderWithToken()})
            .then(response => {
                const auxContacts = contacts.filter(contact => contact.id != response.data.id)
                this.setState({contacts: auxContacts});
            })
            .catch(err => {
                if (err?.response?.data?.error) {
                    toast.error(err?.response?.data?.error)
                } else {
                    toast.error('An Error Ocurred')
                }

            })
    }

    handleSelectedId = id => {
        this.setState({selectContactId: id})
        history.push(`/dashboard/configuracoes/contactos/${id}`)
    }

    updateContacts = () => {
        api.get(`contacts`, {headers: getHeaderWithToken()}).then(response => {
            if (response.data[0].id) {
                this.setState({contacts: response.data})
            }
        })
            .catch((err) => alert("something went wrong, check you connection!"))
    }

    onSearchChange = (event) => {
        this.setState({searchfield: event.target.value})
    }

    handleSelect = (event) => {
        this.setState({loading: true})
        this.fetchContacts(event.target.value)
        //alert(event.target.value)
    }


    handleAddContact = (flag, action) => {
        this.setState({showAddNewContact: flag})
        if (action === 'save') {
            toast.success('Contacto Guardado')
        }
    }

    render() {
        const {contacts, searchfield, loading, showAddNewContact} = this.state

        const filteredContacts = contacts?.filter(contact => {
            return (contact?.name + contact.lastName).toLowerCase().includes(searchfield?.toLowerCase())
        })

        return (
            <Row>
                <ToastContainer/>
                <Col md="12">
                    <Card className="shadow-sm p-3 cardHeight-inbox">
                        <CardTitle>
                            <span style={{display: "flex", justifyContent: "space-between"}}>
                                <div>
                                    <h4 className="title-contacts">
                                        Lista de contactos
                                        &nbsp;
                                        <FiList
                                            size={26}
                                            style={{color: "#B2BABB "}}
                                        />
                                    </h4>
                                </div>

                                <div>
                                    <button
                                        onClick={() => this.handleAddContact(true)}
                                        className=" btn addButton"
                                        style={{color: 'white'}}
                                    >
                                        Novo&nbsp;<FaPlus/>
                                    </button>
                                </div>
                            </span>
                        </CardTitle>
                        <CardBody>
                            <div className="row pl-2">
                                <input
                                    onChange={this.onSearchChange}
                                    className='form-control searchInput-contacts col-sm-2 mr-2'
                                    placeholder='procurar...'
                                />
                                {/* <select
                                    onChange={this.handleSelect}
                                    className='form-control searchInput-contacts col-sm-2'
                                    name=""
                                    id=""
                                >
                                    <option value="all">Todos</option>
                                    <option value="intern">Internos</option>
                                    <option value="extern">Externos</option>
                                </select> */}
                            </div>

                            <div className="row justify-content-center p-5 ">
                                {
                                    (this.state.loading)
                                        ?
                                        <div class="d-flex justify-content-center">
                                            <span class="spinner-border text-primary"></span>
                                            <span className='text-center'>Aguarde, por favor...</span>
                                        </div>
                                        :
                                        (filteredContacts?.length !== 0)
                                            ?
                                            <Table hover style={{cursor: 'pointer'}} borderless responsive>
                                                <thead className="text-info" style={{backgroundColor: '', color: ''}}>
                                                <tr>
                                                    <th scope="col">Nr.</th>
                                                    <th scope="col">Nome</th>
                                                    <th scope="col">Email</th>
                                                    <th scope="col">Celular</th>
                                                    <th
                                                        className='text-center'
                                                        style={{backgroundColor: '', color: ''}}
                                                        scope='col'
                                                        onClick={this.markAll}>Editar
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                {
                                                    (!contacts.length)
                                                        ? <h2>Loading...</h2>
                                                        :
                                                        filteredContacts?.map(
                                                            (contact, i) =>
                                                                <tr key={contact.id}>
                                                                    <th scope="row">{i + 1}</th>
                                                                    <td>{contact.name + ' ' + contact.lastName}</td>
                                                                    <td>{contact.email}</td>
                                                                    <td>{contact.phone}</td>
                                                                    <td className='text-center'>

                                                                        <FaRegEdit
                                                                            onClick={() => {
                                                                                this.handleSelectedId(contact.id)
                                                                            }}
                                                                            className='mr-4 editIcons'
                                                                            style={{color: '#2C3E50 '}}
                                                                        />

                                                                        <FaRegTrashAlt
                                                                            onClick={() => this.handleDelete(contact.id)}
                                                                            className='editIcons'
                                                                            style={{color: '#C0392B'}}
                                                                        />

                                                                    </td>
                                                                </tr>
                                                        )
                                                }
                                                </tbody>
                                            </Table>
                                            :
                                            (
                                                <div className='container'>
                                                    <h3 className='row justify-content-center text-secondary'>Não
                                                        encontrou nenhum contacto</h3>
                                                </div>
                                            )
                                }
                            </div>
                            <Route
                                exact
                                path="/dashboard/configuracoes/contactos/:id"
                                render={() => {
                                    return (
                                        <EditContactModal updateContacts={this.updateContacts}/>
                                    );
                                }}
                            />
                        </CardBody>
                    </Card>
                </Col>
                {
                    showAddNewContact &&
                    <AddContactModal
                        updateContacts={this.updateContacts}
                        showAddNewContact={showAddNewContact}
                        handleAddContact={this.handleAddContact}
                    />
                }
            </Row>
        )
    }
}
