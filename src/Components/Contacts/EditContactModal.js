import React, { Component } from 'react'
import { withRouter } from 'react-router'; //to access query id params
import Modal from 'react-bootstrap/Modal'
import history from '../../history'
import api from '../../Services/api'

import icon from '../../Images/profile.png'
import './styles.css'
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {getHeaderWithToken} from "../../helpers/authHelper";

//toast.configure()

class EditContactModal extends Component {
    constructor() {
        super()

        this.state = {
            show: '',

            formInput: {
                id: '',
                name: '',
                lastname: '',
                gender: '',
                email: '',
                cell: '',
                whatsApp: '',
                department: '',
                company: '',
                designation: ''
            },

            oldData: ''
        }
    }

    UNSAFE_componentWillMount() {
        const id = this.props.match.params.id

        const { formInput } = this.state
        if (id !== 'internos' && id !== 'externos' && id !== 'criar-novo') {
            this.setState({ show: true })

            api.get(`contacts/${id}`, {headers: getHeaderWithToken()})
                .then(async response => {
                    this.setState({
                        formInput: {
                            ...formInput,
                            id: response.data[0].id,
                            name: response.data[0].name,
                            lastname: response.data[0].lastName,
                            gender: response.data[0].gender,
                            email: response.data[0].email,
                            cell: response.data[0].phone,
                            whatsApp: response.data[0].whatsApp,
                            department: response.data[0].department,
                            company: response.data[0].company,
                            designation: response.data[0].designation
                        }
                    })
                })
                .catch(() => alert('failed to fecth data, Sorry! Check your connection'))
        }
        else {
            this.setState({ show: false })
        }
    }

    handleCloseModal = () => {
        this.setState({ show: false })
        history.push('/dashboard/configuracoes/contactos')
    }

    handleOnChangeInput = async (event) => {
        const { name, value } = event.target

        await this.setState({ formInput: { ...this.state.formInput, [name]: value } })
        console.log(this.state.formInput)
    }

    handleStore = async (event) => {
        event.preventDefault()
        const { formInput } = this.state
        const {
            lastname,
            cell,
        } = formInput

        await api.put('contacts', { ...formInput, lastName: lastname, phone: cell }, {headers: getHeaderWithToken()})
            .then(() => {
                toast.info(`Contacto actualizado`)
                this.props.updateContacts();
                history.push('/dashboard/configuracoes/contactos')
            })
            .catch((err) => {
                if (err?.response?.data?.error) {
                    toast.error(err?.response?.data?.error)
                }
                else {
                    toast.error('❌ Não foi possível actualiar contacto. Tente novamente ou verifique a conexão')
                }
            })

    }

    //Based on flag means based on what is missing on user input or excel file
    //Basically this method cdefines the system output message for each missing I(and required) input
    validateBasedOnFlag = (flag) => {
        const requiredFlagExtern = flag.indexOf('requiredExtern')
        const bothEmptyFlagExtern = flag.indexOf('bothEmptyExtern')

        const requiredFlagIntern = flag.indexOf('requiredIntern')
        const bothEmptyFlagIntern = flag.indexOf('bothEmptyIntern')

        if (requiredFlagExtern !== -1 || bothEmptyFlagExtern !== -1) {

            if (requiredFlagExtern > -1 && bothEmptyFlagExtern > -1) {
                alert('Name, LastName, Genero e Companhia devem ser preenchidos e email e celular nao podem estar ambos vazios!')
            }
            else {
                if (requiredFlagExtern > -1) {
                    alert('Name, LastName, Genero Companhia nao podem ser vazios!')
                }
                else {
                    alert('Email e Cell nao podem estar ambos vazios!')
                }
            }
        }
        else {
            if (requiredFlagIntern > -1 && bothEmptyFlagIntern > -1) {
                alert('Name, Lastname, Genero e Departamento devem ser preenchidos e email e celular nao podem estar ambos vazios!')
            }
            else {
                if (requiredFlagIntern > -1) {
                    alert('Name, LastName, Genero e Departamento nao podem ser vazios!')
                }
                else {
                    alert('Email e Cell nao podem estar ambos vazios!')
                }
            }
        }

    }

    //In the Excell File, the name and lastname are required
    //And the email & cell cannot be both empty 
    //Essencially, this method just turn on(or off) the flag variable declared 
    //at "updateParsedExcelFile" function.
    validateData = (data) => {
        var answer = ''
        const { oldData } = this.state

        if (oldData.department) {
            data.forEach(item => {
                if (!item.name || !item.lastname || !item.gender || item.gender === 'Género' || !item.department) {
                    answer += 'requiredIntern'
                }
                else {

                    //In a Excelfile at least one of them must be filled
                    if (!item.email && !item.cell) {
                        answer += 'bothEmptyIntern'
                    }
                }
            })
            return answer;
        }
        else {
            data.forEach(item => {
                if (!item.name || !item.lastname || !item.gender || item.gender === 'Género' || !item.company) {
                    answer += 'requiredExtern'
                }
                else {
                    //In a Excelfile at least one of them must be filled
                    if (!item.email && !item.cell) {
                        answer += 'bothEmptyExtern'
                    }
                }
            })
            return answer;
        }
    }

    render() {
        const {
            name,
            lastname,
            gender,
            email,
            cell,
            department,
            company,
            designation
        } = this.state.formInput

        return (
            <>
                <Modal
                    size="lg"
                    show={this.state.show}
                    class="modal fade modal-dialog"
                    onHide={this.handleCloseModal}
                    centered
                >
                    <Modal.Body>
                        <div className='container p-2 bg p-3 mb-3 text-center' style={{ minWidth: '300px', backgroundColor: 'white' }}>
                            <header>
                                <img style={{ width: '100px', height: '100px' }} src={icon} alt='contact register icon' />
                            </header>
                            <form onSubmit="">
                                <div className="container mt-2" style={{ width: '100%' }}>

                                    <div className='row'>
                                        <div className="col-sm-4">
                                            <label className="edit-contact-label" htmlFor="name col-form-label" id='first'>Nome</label>
                                        </div>
                                        <div className="col-sm-4">
                                            <label className="edit-contact-label" htmlFor="lastname col-form-label second" id='second'>Apelido</label>
                                        </div>
                                    </div>

                                    <div className='row justify-content-center'>
                                        <div className="col-sm-4">
                                            <input
                                                onChange={this.handleOnChangeInput}
                                                className='form-control register-contacts'
                                                type="text"
                                                name="name"
                                                id=""
                                                value={name}
                                                placeholder='Introduzir o Nome' />
                                        </div>
                                        <div className="col-sm-4">
                                            <input
                                                onChange={this.handleOnChangeInput}
                                                className='form-control register-contacts'
                                                type="text"
                                                name="lastname"
                                                id=""
                                                value={lastname}
                                                placeholder='Introduzir o Apelido' />
                                        </div>
                                    </div>

                                    <div className='row mt-2'>
                                        <div className="col-sm-4">
                                            <label className="edit-contact-label" htmlFor="gender col-form-label" id='gender'>Género</label>
                                        </div>
                                        <div className="col-sm-4">
                                            <label className="edit-contact-label" htmlFor="cell col-form-label second" id='second'>Celular</label>
                                        </div>
                                    </div>

                                    <div className='row justify-content-center'>
                                        <div className="col-sm-4">
                                            <input
                                                onChange={this.handleOnChangeInput}
                                                className='form-control register-contacts'
                                                type="text"
                                                name="gender"
                                                id=""
                                                value={gender}
                                                placeholder='Género' />
                                        </div>
                                        <div className="col-sm-4">
                                            <input
                                                onChange={this.handleOnChangeInput}
                                                className='form-control register-contacts'
                                                type="text"
                                                name="cell"
                                                id=""
                                                value={cell}
                                                placeholder='Número de celular' />
                                        </div>
                                    </div>
                                    <div className='row mt-2'>
                                        <div className="col-sm-4">
                                            <label className="edit-contact-label" htmlFor="gender col-form-label" id='gender'>E-mail</label>
                                        </div>
                                    </div>

                                    <div className='row justify-content-center'>
                                        <div className="col-sm-8">
                                            <input
                                                onChange={this.handleOnChangeInput}
                                                className='form-control register-contacts'
                                                type="email"
                                                name="email"
                                                id=""
                                                value={email}
                                                placeholder='Introduzir o E-mail' />
                                        </div>
                                    </div>


                                    <>
                                        <div className='row mt-2'>
                                            <div className="col-sm-4">
                                                <label className="edit-contact-label" htmlFor="gender col-form-label" id='company'>Companhia</label>
                                            </div>
                                            <div className="col-sm-4">
                                                <label className="edit-contact-label" htmlFor="cell col-form-label second" id='designation'>Designação</label>
                                            </div>
                                        </div>

                                        <div className='row justify-content-center'>
                                            <div className="col-sm-4">
                                                <input
                                                    onChange={this.handleOnChangeInput}
                                                    className='form-control register-contacts'
                                                    type="text"
                                                    name="company"
                                                    id=""
                                                    value={company}
                                                    placeholder='Companhia' />
                                            </div>
                                            <div className="col-sm-4">
                                                <input
                                                    onChange={this.handleOnChangeInput}
                                                    className='form-control register-contacts'
                                                    type="text"
                                                    name="designation"
                                                    id=""
                                                    value={designation}
                                                    placeholder='Cargo/Função (opcional)' />
                                            </div>
                                        </div>
                                    </>

                                    <>
                                        <div className='row mt-2'>
                                            <div className="col-sm-4">
                                                <label className="edit-contact-label" htmlFor="gender col-form-label" id='department'>Departamento</label>
                                            </div>
                                        </div>

                                        <div className='row justify-content-center'>
                                            <div className="col-sm-8">
                                                <input
                                                    onChange={this.handleOnChangeInput}
                                                    className='form-control register-contacts'
                                                    type="text"
                                                    name="department"
                                                    id=""
                                                    value={department}
                                                    placeholder='Departamento' />
                                            </div>
                                        </div>
                                    </>

                                </div>
                            </form>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <div className="container">
                            <div className="row justify-content-end">
                                <button
                                    onClick={this.handleCloseModal}
                                    className="btn btn-secondary col-2 mr-2"
                                >
                                    Cancelar
                            </button>
                                <button
                                    onClick={this.handleStore}
                                    className="btn btn-primary col-2"
                                >
                                    Guardar
                            </button>
                            </div>
                        </div>
                    </Modal.Footer>
                </Modal>
            </>
        )
    }
}

export default withRouter(EditContactModal)
