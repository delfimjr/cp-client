import React, {Component} from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {FaUserCog, FaUsers} from "react-icons/fa";
import {faAddressBook, faCaretDown, faCogs} from '@fortawesome/free-solid-svg-icons';
import {BrowserRouter as Router, Link} from 'react-router-dom';
import {getUserPermissions} from "../../helpers/authHelper";


export default class DropdownConfigurations extends Component {

    constructor(props) {
        super(props);
        this.state = {
            classeDiv: 'esconder'
        }
    }

    alterarEstado() {
        var estado;
        if (this.state.classeDiv === 'ver') {
            estado = "esconder";
        } else {
            estado = "ver";
        }
        this.setState({
            classeDiv: estado
        })
    }


    render() {
        const {handlePageTitleChange} = this.props;
        return (
            <div className="dropdown">
                <div className="button" onClick={this.alterarEstado.bind(this)}>
                    <Link>
                        <FontAwesomeIcon
                            icon={faCogs}
                            color="white"
                            style={{marginRight: '4%'}}
                        />Configurações
                        <FontAwesomeIcon
                            icon={faCaretDown}
                            color="white"
                            style={{marginLeft: "28%"}}
                        />
                    </Link>
                </div>

                <ul className={`${this.state.classeDiv} dropdownItems`}>
                    {
                        getUserPermissions().includes('P_MANAGE_CONTACTS')
                        &&
                        <li>
                            <Link to="/dashboard/configuracoes/contactos">
                                <FontAwesomeIcon
                                    icon={faAddressBook}
                                    color="white"
                                    className="sideIcons"
                                />Contactos
                            </Link>
                        </li>
                    }

                    {
                        getUserPermissions().includes('P_MANAGE_GROUPS')
                        &&
                        <li>
                            <Link to="/dashboard/configuracoes/grupos">
                                <FaUsers
                                    color="white"
                                    className="sideIcons"
                                />Grupos
                            </Link>
                        </li>
                    }

                    {
                        this.props.userObject.user.super && getUserPermissions().includes('P_MANAGE_USERS')
                            &&
                            <li>
                                <Link to="/dashboard/configuracoes/administradores">
                                    <FaUserCog className="sideIcons"/>Administradores
                                </Link>
                            </li>
                    }

                </ul>
            </div>
        );
    }
}
