import React from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faEnvelope, faPlus, faPaperPlane, faCaretDown} from '@fortawesome/free-solid-svg-icons';
import {FaInbox} from "react-icons/fa";
import {BrowserRouter as Router, Link} from 'react-router-dom';
import {getUserPermissions} from "../../helpers/authHelper";


class Dropdown extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            classeDiv: 'esconder'
        }

    }

    alterarEstado() {
        var estado;
        if (this.state.classeDiv === 'ver') {
            estado = "esconder";
        } else {
            estado = "ver";
        }
        this.setState({
            classeDiv: estado
        })
    }


    render() {

        return (
            <div className="dropdown">
                <div className="button" onClick={this.alterarEstado.bind(this)}>
                    <Link>
                        <FontAwesomeIcon
                            icon={faEnvelope}
                            color="white"
                            className="sideIcons"
                        />Mensagens
                        <FontAwesomeIcon
                            icon={faCaretDown}
                            color="white"
                            style={{marginLeft: "35%"}}/>
                    </Link>

                </div>

                <ul className={`${this.state.classeDiv} dropdownItems`}>
                    {
                        getUserPermissions().includes('P_SEND_EMAILS')
                        &&
                        <li>
                            <Link to='/dashboard/enviar-mensagem'>
                                <FontAwesomeIcon
                                    icon={faPlus}
                                    color="white"
                                    className="sideIcons"
                                />Enviar Mensagem
                            </Link>
                        </li>
                    }

                    {
                        getUserPermissions().includes('P_VIEW_EMAILS')
                        &&
                        <li>
                            <Link
                                to="/dashboard/caixa-de-emails">
                                <FaInbox className="sideIcons"/>Emails Recebidos
                            </Link>
                        </li>}

                    {
                        getUserPermissions().includes('P_LIST_MESSAGES')
                        &&
                        <li>
                            <Link to="/dashboard/mensagens-enviadas">
                                <FontAwesomeIcon
                                    icon={faPaperPlane}
                                    color="white"
                                    className="sideIcons"/>Mensagens enviadas
                            </Link>
                        </li>
                    }

                </ul>
            </div>
        );
    }
}

export default Dropdown;
