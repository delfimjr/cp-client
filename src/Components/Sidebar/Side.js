import React, {Component} from "react";
import '../Estilo.css';
import port from '../../Images/port.png';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faHome, faUserAlt, faBullhorn, faAddressBook, faCogs} from '@fortawesome/free-solid-svg-icons';
import DropdownMessages from './DropdownMessages.js';
import DropDownConfigurations from './DropDownConfigurations';
import {BrowserRouter as Router, Link} from 'react-router-dom';
import DropDownPublications from "./DropDownPublications";
import {getUserPermissions} from "../../helpers/authHelper";

export default class Side extends Component {

    constructor(props) {
        super(props);
        console.log(props.userObject.user.permissions)
    }

    render() {
        return (
            <div class="wrapper">
                <nav id="sidebar" className={this.props.classeSecundaria}>
                    <div class="sidebar-header">
                        <img src={port} id="logo"/>
                    </div>

                    <ul class="list-unstyled components">
                        <li>
                            <Link className="side-link" to="/dashboard/">
                                <FontAwesomeIcon
                                    icon={faHome}
                                    color="white"
                                    className="sideIcons"
                                /> Home
                            </Link>
                        </li>

                        <li>
                            <Link to="/dashboard/perfil">
                                <FontAwesomeIcon
                                    icon={faUserAlt}
                                    color="white"
                                    className="sideIcons"
                                /> Meu Perfil
                            </Link>
                        </li>

                        <li>
                            {
                                this.props.userObject.user.permissions.includes('P_MANAGE_MESSAGES')
                                &&
                                <DropdownMessages/>
                            }
                        </li>

                        <li>
                            {
                                getUserPermissions().includes('P_MANAGE_PUBLICATIONS')
                                &&
                                <DropDownPublications/>
                            }
                        </li>

                        <li>
                            {
                                getUserPermissions().includes('P_PLATFORM_CONFIGURATION')
                                &&
                                <DropDownConfigurations userObject={this.props.userObject}/>
                            }
                        </li>
                    </ul>
                </nav>
            </div>
        );
    }
}
