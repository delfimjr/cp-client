import React, {Component} from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {FaUserCog, FaUsers} from "react-icons/fa";
import {faAddressBook, faBullhorn, faCaretDown, faCogs, faPlus} from '@fortawesome/free-solid-svg-icons';
import {BrowserRouter as Router, Link} from 'react-router-dom';
import {getUserPermissions} from "../../helpers/authHelper";


export default class DropDownPublications extends Component {

    constructor(props) {
        super(props);
        this.state = {
            classeDiv: 'esconder'
        }
    }

    alterarEstado() {
        var estado;
        if (this.state.classeDiv === 'ver') {
            estado = "esconder";
        } else {
            estado = "ver";
        }
        this.setState({
            classeDiv: estado
        })
    }


    render() {
        const {handlePageTitleChange} = this.props;
        return (
            <div className="dropdown">
                <div className="button" onClick={this.alterarEstado.bind(this)}>
                    <Link>
                        <FontAwesomeIcon
                            icon={faBullhorn}
                            color="white"
                            className="sideIcons"
                        />Publicações
                        <FontAwesomeIcon
                            icon={faCaretDown}
                            color="white"
                            style={{marginLeft: "28%"}}
                        />
                    </Link>
                </div>

                <ul className={`${this.state.classeDiv} dropdownItems`}>
                    {
                        getUserPermissions().includes('P_VIEW_EMAILS')
                        &&
                        <li>
                            <Link to='/dashboard/criar-publicacao'>
                                <FontAwesomeIcon
                                    icon={faPlus}
                                    color="white"
                                    className="sideIcons"
                                />Criar Publicação
                            </Link>
                        </li>
                    }

                    {
                        <li>
                            <Link to="/dashboard/publicacoes">
                                <FaUsers
                                    color="white"
                                    className="sideIcons"
                                />Publicações
                            </Link>
                        </li>
                    }

                </ul>
            </div>
        );
    }
}
