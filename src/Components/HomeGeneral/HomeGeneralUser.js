import React, { Component } from 'react'
import Header from '../SignInHeader/Header'
import VerPublicacao from '../Publicacoes/VerPublicacao'
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardMedia from '@material-ui/core/CardMedia';
import HomeBackground from '../../Images/HomeBackground.jpg';
import Footer from '../Footer/Footer'
import './homeGeneralUser.css'

const useStyles = makeStyles({
  media: {
    height: /*340*/570,
  }
});

function MediaCard() {
  const classes = useStyles();

  return (
    <Card style={{marginTop:"55px", marginBottom:"55px"}} id="bg">
      <CardActionArea>
        <CardMedia
          className={classes.media}
          image={HomeBackground}
        />
        <p style={{fontWeight:"bold", fontFamily: "Arial", fontSize: /*"32pt"*/"36pt", color: "white", textShadow: "2px 2px 10px black", position: "absolute", left: "110px", top: "120px"}}>
            Bem-vindo a plataforma de comunicação do MPDC
        </p>
        <p style={{fontWeight:"bold", fontFamily: "Arial", fontSize: "30pt", color: "white", textShadow: "2px 2px 10px black", position: "absolute", left: "300px", top: "240px"}}>Saiba o que acontece no Porto de Maputo</p>
      </CardActionArea>
    </Card>
  );
}


export default class HomeGeneralUser extends Component {

    render(){
        return(
            <div id="homeId">
              <Header login = 'homepage'/>
              <MediaCard/>
              <div id="homePublications">
                <VerPublicacao cardId='homecard'/>
              </div>
              <Footer classname='homefooter'/>
            </div>
        );
    }
}
