import React, { Component } from "react";
import { BrowserRouter as Router, Link } from 'react-router-dom';
import port from '../../Images/Port_Logo.png'
import {Button} from 'react-bootstrap';

import './styles.css'

export default class Header extends Component {
    render() {
        if (this.props.login === 'login')
            return (
                <nav className="navbar navbar-expand-lg navbar-light fixed-top warning">
                    <div className="container">
                        <Link className="navbar-brand font-weight-bold" to={"/"}>
                            <img
                                alt='Porto Maputo Logo'
                                style={{ width: '10%', height: '10%' }}
                                src={port} id="logo"
                            />
                            MPDC Communicate
                        </Link>
                    </div>
                </nav>
            );
        else {
            return (
                <nav className="navbar navbar-expand-lg navbar-light fixed-top warning">
                    <div className="container">
                        <Link className="navbar-brand font-weight-bold" to={"/"}>
                            <img
                                alt='Porto Maputo Logo'
                                style={{ width: '10%', height: '10%' }}
                                src={port} id="logo"
                            />
                            MPDC Communicate
                        </Link>
                    </div>

                    <Link to={"/login"}><Button variant="outline-light">Sign In</Button></Link>
                </nav>
            )
        }
    }
}
