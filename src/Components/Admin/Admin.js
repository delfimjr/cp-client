import React, {Component, useEffect, useState} from 'react'
import {Redirect} from 'react-router-dom'
import {FiAlignLeft} from 'react-icons/fi'
import RoutesAdmin from '../../Routes/routesAdmin'
import User from '../UserHeader/User'
import Side from '../Sidebar/Side'


import admin from './Admin.css'
import Footer from '../Footer/Footer'

import {BrowserRouter} from 'react-router-dom';
import Loading from '../Loading'
import {useFetch} from '../../hooks/useFetch'
import {getUserPermissions} from "../../helpers/authHelper";

const Admin = () => {
    //Fetching Email Data
    const {data, error, isValidating, mutate} = useFetch('mailBox')


    const [redirect, setRedirect] = useState(false)
    const [pageTitle, setPageTitle] = useState('Dashboard')
    const [user, setUser] = useState(null)
    const [classDiv, setClassDiv] = useState('show')
    const [divId, setDivId] = useState('content')

    useEffect(() => {
        if (sessionStorage.getItem("id")) {
            setUser(JSON.parse(sessionStorage.getItem('id')))
            if (sessionStorage.getItem("pageTitle")) {
                setPageTitle(JSON.parse(sessionStorage.getItem('pageTitle')).title)
            }
        } else {
            setRedirect(true)
        }
    }, [])

    function changeState() {
        let status;
        let idDaDiv;
        if (classDiv === 'show') {
            status = "hide";
            idDaDiv = "content1";
        } else {
            status = "show";
            idDaDiv = "content";
        }
        setClassDiv(status)
        setDivId(idDaDiv)
    }

    function setCurrentProfile() {
        setUser(JSON.parse(sessionStorage.getItem('id')))
    }

    function updateLoggedUserProfilePicture(profilePicture) {
        setUser({...user, user: {...user.user, profilePicture: profilePicture}})
    }

    useEffect(() => {
        if (user) {
            sessionStorage.setItem('id', JSON.stringify(user));
        }
    }, [user])

    function handlePageTitleChange(title) {
        setPageTitle(title)
        sessionStorage.setItem('pageTitle', JSON.stringify({title: title}));
    }

    function logout() {
        sessionStorage.setItem("id", '')
        sessionStorage.clear();
        setRedirect(true)
    }

    if (redirect) {
        return (<Redirect to={("/")}/>)
    }

    if (!user) {
        return <Loading/>
    }

    return (
        <div>
            <div id={divId}>
                <nav className="navbar navbar-expand" id="navHead">
                    <div className="container-fluid">
                        <div className="navbar-header">
              <span
                  style={{marginRight: "5px"}}
                  id="sidebarCollapse"
                  className="btn"
              >
                <FiAlignLeft
                    className="menuIcon"
                    color="#616A6B "
                    size={30}
                    onClick={changeState}
                    style={{marginRight: ''}}
                />
              </span>
                            <i className='normalFont'>
                                {pageTitle}
                            </i>

                        </div>

                        {
                            <User
                                messages={data}
                                userObject={user}
                                logout={logout}
                            />
                        }
                    </div>
                </nav>

                {
                    <>
                        <Side
                            userObject={user}
                            handlePageTitleChange={handlePageTitleChange}
                            classeSecundaria={classDiv}
                        />

                        <RoutesAdmin
                            updateLoggedUserProfilePicture={updateLoggedUserProfilePicture}
                            handlePageTitleChange={handlePageTitleChange}
                            userObject={user}
                            setCurrentProfile={setCurrentProfile}
                            data={data}
                            error={error}
                            isValidating={isValidating}
                            mutate={mutate}
                        />
                    </>
                }
                <Footer classname='footer'/>
            </div>
        </div>
    )
}

export default Admin;


// export default class Admin extends Component {

//   constructor(props) {
//     super(props);
//     this.state = {
//       redirect: false,
//       route: 'Conteudo',
//       classeDiv: 'show',
//       divId: 'content',
//       user: {},
//       pageTitle: 'Dashboard'
//       //comentarios: []
//     }
//     this.logout = this.logout.bind(this);
//   }

//   componentWillMount() {
//     if (sessionStorage.getItem("id")) {
//       this.setState({ user: JSON.parse(sessionStorage.getItem('id')) })
//       if (sessionStorage.getItem("pageTitle")) {
//         this.setState({ pageTitle: JSON.parse(sessionStorage.getItem('pageTitle')).title })
//       }
//     }
//     else {
//       this.setState({ redirect: true });
//     }
//   }

//   alterarEstado() {
//     var estado;
//     var idDaDiv;
//     if (this.state.classeDiv === 'show') {
//       estado = "hide";
//       idDaDiv = "content1";
//     } else {
//       estado = "show";
//       idDaDiv = "content";
//     }
//     this.setState({
//       classeDiv: estado,
//       divId: idDaDiv
//     })
//   }

//   setCurrentProfile = () => {
//     this.setState({ user: JSON.parse(sessionStorage.getItem('id')) })
//   }

//   handlePageTitleChange = title => {
//     this.setState({ pageTitle: title })
//     sessionStorage.setItem('pageTitle', JSON.stringify({ title: title }));
//   }

//   logout() {
//     sessionStorage.setItem("id", '')
//     sessionStorage.clear();
//     this.setState({ redirect: true });
//   }

//   render() {
//     if (this.state.redirect) {
//       return (<Redirect to={("/")} />)
//     }

//     const { pageTitle } = this.state

//     return (
//       <div>
//         <div id={`${this.state.divId}`}>
//           <nav className="navbar navbar-expand" id="navHead">
//             <div className="container-fluid">
//               <div className="navbar-header">
//                 <span
//                   style={{ marginRight: "5px" }}
//                   id="sidebarCollapse"
//                   className="btn"
//                 >
//                   <FiAlignLeft
//                     className="menuIcon"
//                     color="#616A6B "
//                     size={30}
//                     onClick={this.alterarEstado.bind(this)}
//                     style={{ marginRight: '' }}
//                   />
//                 </span>
//                 <i className='normalFont'>
//                   {pageTitle}
//                 </i>

//               </div>

//               <User
//                 handlePageTitleChange={this.handlePageTitleChange}
//                 userObject={this.state.user}
//                 logout={this.logout}
//               />
//             </div>
//           </nav>

//           <Side
//             userObject={this.state.user}
//             handlePageTitleChange={this.handlePageTitleChange}
//             classeSecundaria={this.state.classeDiv}
//           />

//           {/*** Performing Routing in Dashboard*/}
//           <RoutesAdmin
//             handlePageTitleChange={this.handlePageTitleChange}
//             userObject={this.state.user}
//             setCurrentProfile={this.setCurrentProfile}
//           />

//           <Footer classname='footer' />
//         </div>
//       </div>
//     )
//   }
// }
