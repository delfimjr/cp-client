import React from 'react'
import './styles_input.css'

const Input = ({ name, ...rest }) => {
    
    return (
        <div className="input-block">
            <input type="text" name={name} {...rest} />
        </div>
    )
}

export default Input