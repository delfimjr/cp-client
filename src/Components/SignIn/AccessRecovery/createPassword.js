import React, { useState, useEffect } from 'react'
import { useHistory, useLocation, Redirect } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';

import Header from '../../SignInHeader/Header'
import api from '../../../Services/api'

import 'react-toastify/dist/ReactToastify.css';
import '../styles.css'

const CreatePassword = () => {
    const history = useHistory()
    const location = useLocation()

    const [formData, setFormData] = useState({
        password_1: '',
        password_2: '',
    })
    const [minCharacters, setMinCharacters] = useState('')
    const [mismatchAlert, setMisMatchAlert] = useState('')

    useEffect(() => {
        if (formData.password_1 !== '') {
            if (formData.password_1.length < 6) {
                setMinCharacters('A senha deve ter no mínimo 6 caracters')
            }
            else {
                setMinCharacters('')
                if (formData.password_2 !== '' && formData.password_2 !== formData.password_1) {
                    setMisMatchAlert('As senhas devem ser iguais!')
                }
                else {
                    setMisMatchAlert('')
                }
            }
        }

    }, [formData])


    function handleFormData(event) {
        const { name, value } = event.target
        setFormData({ ...formData, [name]: value })
    }

    function handleSubmit(event) {
        event.preventDefault()
        const email = location.state.email
        const { password_1, password_2 } = formData

        if (password_1 !== '' && password_2 !== '') {
            if (password_1 === password_2) {
                if (password_1.length >= 6 & password_2.length >= 6) {
                    api.put('changePassword', { password_1, email }).then(() => {
                        toast.success(`✅ Senha alterada`)
                        //Passar a mensagem de sucesso para a tela de Login
                        history.push('/login')
                    })
                        .catch(() => {
                            toast.error(`❌ Não foi possível criar a nova senha. Tente novamente ou verifique a conexão`)
                        })
                }
                else {
                    setMinCharacters('A senha deve ter no mínimo 6 caracters')
                }
            }
            else {
                setMisMatchAlert('As senhas devem ser iguais!')
            }
        }
        else {
            toast.warning('Todos os campos devem ser preenchidos')
        }


    }

    if (JSON.parse(sessionStorage.getItem("email")) === "" || JSON.parse(sessionStorage.getItem("email")) === null) {
        return (<Redirect to={("/")} />)
    }
    else {

        return (
            <>
                <Header />
                <ToastContainer />
                <div className="auth-wrapper mt-3">
                    <div className="auth-inner">
                        <form>
                            <h3 className='mb-3 text-warning font-weight-bold'>Criar uma nova senha! &#128515;</h3>

                            <div className="form-group text-justify font-weight-light">

                                <input
                                    type="password"
                                    name='password_1'
                                    className="form-control mb-0 signIn"
                                    placeholder="Introduzir a nova senha"
                                    onChange={handleFormData}
                                />
                                {
                                    (minCharacters.length !== 0)
                                        ?
                                        <span style={{ fontWeight: '480' }} className='text-danger'>{minCharacters}</span>
                                        :
                                        (
                                            (formData.password_1.length !== 0)
                                                ?
                                                <span style={{ fontWeight: '480' }} className='text-info'>&#10004;A senha é válida</span>
                                                :
                                                <span></span>
                                        )

                                }

                                {
                                    (formData.password_1.length >= 6)
                                        ?
                                        <input
                                            type="password"
                                            name='password_2'
                                            className="form-control mt-3 signIn"
                                            placeholder="Confirmar a nova senha"
                                            onChange={handleFormData}
                                        />
                                        :
                                        <input
                                            type="password"
                                            name='password_2'
                                            className="form-control mt-3 signIn"
                                            placeholder="Confirmar a nova senha"
                                            disabled
                                            onChange={handleFormData}
                                        />


                                }



                                {
                                    (mismatchAlert.length !== 0)
                                        ?
                                        <span style={{ fontWeight: '480' }} className='text-danger'>{mismatchAlert}</span>
                                        :
                                        (
                                            formData.password_2 !== ''
                                                ?
                                                <span style={{ fontWeight: '480' }} className='text-info'>&#10004;senha confirmada!</span>
                                                :
                                                <span></span>
                                        )

                                }
                            </div>
                            <div className='text-right mt-5'>
                                <button
                                    onClick={handleSubmit}
                                    className="btn btn-success col-sm-4"
                                >
                                    Guardar
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </>
        );
    }


}

export default CreatePassword




