import React, { useState } from 'react'
import { Link, useHistory } from 'react-router-dom';
import validator from 'validator'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Header from '../../SignInHeader/Header'
import api from '../../../Services/api'
import '../styles.css'

const RecLogin = () => {

    const [email, setEmail] = useState('')
    const [recoveryField, setRecoveryField] = useState(false)
    const [recoveryCode, setRecoveryCode] = useState('')

    const history = useHistory()


    function handleEmailChange(event) {
        const { value } = event.target
        setEmail(value)
    }

    function handleRecoveryCode(event) {
        const { value } = event.target
        setRecoveryCode(value)
    }

    function handleSubmitEmail(event) {
        event.preventDefault()

        if (email !== '') {
            let isEmail = validator.isEmail(email)
            if (isEmail) {
                api.post('checkEmail_communications', { email }).then(() => {
                    setRecoveryField(true)
                })
                    .catch(() => alert('NOOO'))
            }
            else {
                alert('email invalido')
            }
        }
        else {
            alert('empty')
        }
    }

    function handleSubmitRecoveryCode(event) {
        event.preventDefault()
        //alert(recoveryField)
        if (recoveryCode !== '') {
            api.post('checkRecoveryCode_communications', { recoveryCode })
                .then(() => {
                    sessionStorage.setItem('email', JSON.stringify(email));
                    history.push('/create-password', {email: email})
                })
                .catch(() => {
                    toast.error(`Codigo de recuperação inválido. Por favor, confirme!`)
                })
        }
        else{
            alert('empty')
        }
    }


    return (
        <>
            <ToastContainer />
            <Header />
            <div className="auth-wrapper mt-3">
                <div className="auth-inner">
                    <form>
                        <h2 className='mb-5 text-warning font-weight-bold'>Recuperação de Acesso</h2>

                        <div className="form-group text-justify font-weight-light">
                            {
                                (!recoveryField)
                                    ?
                                    <>
                                        <label className='mb-3 font-weight-bold'>
                                            Por favor, introduza o endereço de e-mail
                                        </label>

                                        <input
                                            type="email"
                                            name='email'
                                            className="form-control signIn"
                                            placeholder="endereço de e-mail"
                                            onChange={handleEmailChange}
                                        />
                                    </>
                                    :
                                    <form>
                                        <label htmlFor="">
                                            Enviamos um código de confirmação para o teu endereço de e-mail.
                                            <br />
                                            <br />
                                            Por favor, escreva o código no campo abaixo
                                        </label>

                                        <input
                                            type="text"
                                            name='code'
                                            className="form-control signIn"
                                            placeholder="código de recuperação"
                                            autoComplete="off"
                                            onChange={handleRecoveryCode}
                                        />
                                    </form>
                            }


                        </div>
                        <div className='text-right mt-5'>
                            {
                                (!recoveryField)
                                    ?
                                    <>
                                        <Link className="" to="/login">
                                            <button className="btn btn-danger mr-2">Cancelar</button>
                                        </Link>
                                        <button
                                            onClick={handleSubmitEmail}
                                            className="btn btn-success"
                                        >
                                            Seguinte
                                        </button>
                                    </>
                                    :
                                    <button
                                        onClick={handleSubmitRecoveryCode}
                                        className="btn btn-success"
                                    >
                                        Seguinte
                                    </button>
                            }


                        </div>
                    </form>
                </div>
            </div>
        </>
    );
}

export default RecLogin




