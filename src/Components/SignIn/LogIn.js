import React, { useState } from "react";
import { Redirect } from 'react-router-dom';
import { FiLogIn, FiInfo } from 'react-icons/fi'
import { Link } from 'react-router-dom';
import api from '../../Services/api'

import Header from '../SignInHeader/Header'
import Input from "./input";
import Loading from '../Loading'

import port from '../../Images/port2.png'

const Login = () => {

    const [formInput, setFormInput] = useState({
        username: '',
        password: ''
    })
    const [loading, setLoading] = useState(false)
    const [redirect, setRedirect] = useState(false)
    const [userNameIsEmptyBorder, setUserNameIsEmptyBorder] = useState('')
    const [passwordIsEmptyBorder, setPasswordIsEmptyBorder] = useState('')
    const [alertMessage, setAlertMessage] = useState('')

    function handleInputChange(event) {
        const { name, value } = event.target
        setFormInput({ ...formInput, [name]: value })

        setAlertMessage('')
        setUserNameIsEmptyBorder('')
        setPasswordIsEmptyBorder('')
    }

    function handleSubmit(event) {
        event.preventDefault()
        const { username, password } = formInput
        if (username !== '' && password !== '') {

            const data = {
                username: username.toLocaleLowerCase(),
                password
            }
            setLoading(true)
            api.post('/auth', data).then(response => {
                setLoading(false)
                sessionStorage.setItem('id', JSON.stringify(response.data));
                setRedirect(true)
                setLoading(false)

            })
                .catch(err => {
                    setLoading(false)
                    if (err?.response?.data?.error) {
                        setAlertMessage(err?.response?.data?.error)
                    }
                })
        }
        else {
            setAlertMessage('Por favor, preencha todos campos')
            setUserNameIsEmptyBorder('#F7DC6F')
            setPasswordIsEmptyBorder('#F7DC6F')
        }
    }

    if (redirect || sessionStorage.getItem("id")) {
        return (<Redirect to={("/dashboard")} />)
    }

    return (
        <>
            <Header login='login' />
            <div className="auth-wrapper mt-3 body">
                <div className="auth-inner">
                    <header>
                        <h4>Bem Vindo(a)</h4>
                        <img src={port} alt="Port logo" />
                    </header>
                    <div
                        className="mb-2"
                        style={{ background: "#F7DC6F", borderRadius: "2px", fontSize: "10.5pt", fontWeight: 'bold' }}
                    >
                        {
                            alertMessage !== '' &&
                            <div>
                                <FiInfo size={16} /> {alertMessage} &#128533;
                            </div>
                        }
                    </div>

                    <Input
                        name="username"
                        type="text"
                        className="login-input"
                        placeholder="Nome de utilizador"
                        style={{ borderColor: `${userNameIsEmptyBorder}` }}
                        autoComplete="off"
                        onChange={handleInputChange}
                    />
                    <br />
                    <Input
                        name="password"
                        type="password"
                        className="login-input"
                        placeholder="Senha"
                        style={{ borderColor: `${passwordIsEmptyBorder}` }}
                        autoComplete="off"
                        onChange={handleInputChange}
                    />
                    <br />
                    <button
                        onClick={handleSubmit}
                        className="login-button"
                    >
                        Entrar <FiLogIn />
                    </button>
                    <br />
                    <Link className="nav-link" to={"/recuperar-acesso"}>Esqueceu a Palavra Passe?</Link>
                </div>
                {
                    loading && <Loading />
                }
            </div>

        </>
    )
}

export default Login
