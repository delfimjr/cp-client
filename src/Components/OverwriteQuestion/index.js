import React from 'react'
import Modal from 'react-bootstrap/Modal'
import { } from 'react-icons/ti'
import { FaQuestion } from 'react-icons/fa'

import history from '../../history'

import '../Inbox/inbox.css'

export default class OverWriteQuestion extends React.Component {


    handleForward = () => {
        const { overWriteText } = this.props

        if (overWriteText === 'isMessage') {
            const { messageContent } = this.props

            sessionStorage.setItem('message', JSON.stringify(messageContent));
            history.push('/dashboard/enviar-mensagem')
        }
        else {
            const { messageContent } = this.props

            sessionStorage.setItem('publish', JSON.stringify(messageContent));
            history.push('/dashboard/criar-publicacao')
        }
    }

    handleWriteMessage = () => {
        const { messageContent } = this.props

        sessionStorage.setItem('message', JSON.stringify(messageContent));
        history.push('/dashboard/enviar-mensagem')
    }

    handlePublish = () => {
        const { messageContent } = this.props

        sessionStorage.setItem('publish', JSON.stringify(messageContent));
        history.push('/dashboard/criar-publicacao')
    }

    render() {
        const { overWriteText } = this.props
        return (
            <Modal
                animation
                size="sm"
                show={this.props.showOverWrite}
                class="modal fade modal-dialog"
                onHide={() => this.props.setShowModal(false)}
                centered
            >

                <Modal.Body style={{ backgroundColor: '#FFFFFF' }} className='rounded'>

                    <div className='row text-center'>
                        <FaQuestion className='text-warning col-sm-12' style={{ width: '100px', height: '100px' }} />
                    </div>


                    {
                        (overWriteText === 'isMessage')
                            ?
                            (
                                <div className='text-center'>
                                    <span className='row-sm-12 text-center font-weight-normal'>Tem uma mensagem não enviada que ficou guardada!</span>
                                    <span className='row-sm-12 text-center font-weight-normal'>Deseja substituir por esta?</span>
                                </div>
                            )
                            :
                            (
                                <div className='text-center'>
                                    <span className='row-sm-12 text-center font-weight-normal'>Tem uma publicação não partilhada que ficou guardada!</span>
                                    <span className='row-sm-12 text-center font-weight-normal'>Deseja substituir por esta?</span>
                                </div>
                            )
                    }


                    <div className='row' style={{ fontFamily: '' }}>
                        <button
                            onClick={() => this.props.setShowModal(false)}
                            className="btn btn-danger col-sm-6 rounded-0 border-0"
                            type="button"
                        >
                            Não
                        </button>
                        <button
                            onClick={this.handleForward}
                            className=" btn btn-success col-sm-6 rounded-0 border-0"
                            type="button"
                        >
                            Sim
                        </button>
                    </div>
                </Modal.Body>
            </Modal>

        )
    }
}



