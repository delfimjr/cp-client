import React, { Component } from 'react'
import { FaSave } from "react-icons/fa";
import Avatar from 'react-avatar-edit'

import Modal from 'react-bootstrap/Modal'

export default class AvatarModal extends Component {

    constructor(props) {
        super(props)
        this.state = {
            preview: null,
            src: ''
        }

        this.onCrop = this.onCrop.bind(this)
        this.onClose = this.onClose.bind(this)
        this.handleCloseModal = this.handleCloseModal.bind(this)
        this.handleSavePicture = this.handleSavePicture.bind(this)
    }

    onClose() {
        this.setState({ preview: null })
    }

    handleCloseModal() {
        var retVal = window.confirm("Todas alterações serão perdidas.Continuar?");
        if (retVal === true)
            this.props.setAvatarModal(false);
    }

    handleSavePicture() {
        this.props.updatePicSource(this.state.preview)
        this.props.setAvatarModal(false)
    }

    onCrop(preview) {
        this.setState({ preview })
    }

    render() {

        return (
            <Modal
                size="md"
                show={this.props.showAvatarModal}
                class="modal fade modal-dialog"
                centered
            >
                <Modal.Header >
                    <h4>Carregar Imagem</h4>
                </Modal.Header>
                <Modal.Body>
                    <div className = 'row justify-content-center'>
                        <Avatar
                            width={390}
                            height={295}
                            onCrop={this.onCrop}
                            onClose={this.onClose}
                            //onBeforeFileLoad={this.onBeforeFileLoad}
                            src={this.state.src}
                        />
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <button
                        onClick={this.handleCloseModal}
                        className="btn btn-danger"
                        type="button"
                    >
                        Cancelar
                    </button>
                    <button
                        onClick={this.handleSavePicture}
                        className="btn btn-success"
                        type="button"
                    >
                        <FaSave />Guardar
                    </button>
                </Modal.Footer>
            </Modal>
        )
    }
}