import React, { Component } from "react";
import ReactDOM from 'react-dom';
import validator from 'validator';
import passwordComplexity from 'joi-password-complexity'
import { Alert, Col, Row, Card, CardBody } from "reactstrap"
import AvatarModal from './AvatarModal'
import Loading from '../Loading/index'

import { FaPlus, FaSave } from "react-icons/fa";
import { FcSynchronize } from 'react-icons/fc'
import { FiArrowLeft } from 'react-icons/fi'
import noPicture from '../../Images/profile.png'

import api from '../../Services/api'

import './styles.css'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { API_URL } from "../../helpers/api";
import {getHeaderWithToken} from "../../helpers/authHelper";

export default class Profile extends Component {

    constructor(props) {
        super(props)
        this.state = {
            picSource: noPicture,
            email: '',
            name: '',
            lastName: '',
            userName: '',
            profilePicture: this.props.userObject.user.profilePicture,
            showAvatarModal: false,
            emailIsEmptyBorder: '',
            lastNameIsEmptyBorder: '',
            nameIsEmptyBorder: '',
            userNameIsEmptyBorder: '',
            emptinessAlert: false,
            isValidEmail: true,
            loading: false,
            currentPassword: '',
            newPassword: '',
            changePassword: false,
            passwordComplexityBorder: ''
        }
    }

    UNSAFE_componentWillMount() {
        this.props.handlePageTitleChange('Meu Perfil')

        api.get(`communications/${this.props.userObject.user.id}`, {headers: getHeaderWithToken()})
            .then(response => {
                this.setState({
                    userName: response.data.userName,
                    name: response.data.name,
                    lastName: response.data.lastName,
                    email: response.data.email
                })
            })
    }

    saveModificationsHandler = () => {
        const {
            name,
            lastName,
            email,
            userName,
            nameIsEmptyBorder,
            lastNameIsEmptyBorder,
            emailIsEmptyBorder,
            userNameIsEmptyBorder
        } = this.state

        if (name && lastName && userName && email) {
            const { userObject } = this.props;

            if (!validator.isEmail(email)) {
                this.setState({ emailIsEmptyBorder: 'border border-warning' })
                this.setState({ isValidEmail: false })

                return;
            }

            api.put(`communications/${this.props.userObject.user.id}`, {
                userName,
                email,
                name,
                lastName
            }, {headers: getHeaderWithToken()})
                .then(response => {
                    toast.success('O Perfil foi actualizado')
                })
                .catch(err => {
                    toast.error('Failed to save changes!')
                })
        }
        else {
            if (!name) {
                this.setState({ nameIsEmptyBorder: 'border border-danger' })
            }
            if (!lastName) {
                this.setState({ lastNameIsEmptyBorder: 'border border-danger' })
            }
            if (!email) {
                this.setState({ emailIsEmptyBorder: 'border border-danger' })
            }
            if (!userName) {
                this.setState({ userNameIsEmptyBorder: 'border border-danger' })
            }
        }
    }

    nameChangeHandler = (event) => {
        this.setState({ name: event.target.value })
        this.setState({ nameIsEmptyBorder: '' })
        this.setState({ emptinessAlert: false })
    }

    lastNameChangeHandler = (event) => {
        this.setState({ lastName: event.target.value })
        this.setState({ lastNameIsEmptyBorder: '' })
        this.setState({ emptinessAlert: false })
    }

    emailChangerHandler = (event) => {
        this.setState({ email: event.target.value })
        this.setState({ emailIsEmptyBorder: '' })
        this.setState({ emptinessAlert: false })
        this.setState({ isValidEmail: true })
    }

    userNameChangeHandler = event => {
        this.setState({ userName: event.target.value })
        this.setState({ userNameIsEmptyBorder: '' })
        this.setState({ emptinessAlert: false })
    }

    currentPasswordChangeHandler = event => {
        this.setState({ currentPassword: event.target.value })
    }

    newPasswordChangeHandler = event => {
        this.setState({ newPassword: event.target.value })
    }

    setAvatarModal = (estado) => {
        this.setState({ showAvatarModal: estado })
    }

    updatePicSource = async (text) => {
        const { userObject } = this.props;

        this.setState({ loading: true })
        await api.put(`communications/${userObject.user.id}/profilePicture`, { encodedImage: text }, {headers: getHeaderWithToken()}).then(async response => {
            this.props.updateLoggedUserProfilePicture(response.data.pictureUrl);
            this.setState({ loading: false })
            toast.success('Foto de perfil actualizada')
        })
            .catch((err) => {
                console.log(err)
                this.setState({ loading: false })
                toast.error('Falha na actualização da Foto de Perfil. Por favor, verifique a tua Internet')
            })
    }

    saveNewPassword = () => {
        const { currentPassword, newPassword } = this.state;

        if (currentPassword && newPassword) {
            const label = "Password"
            const complexityOptions = {
                min: 6,
                max: 30,
                lowerCase: 1,
                upperCase: 1,
                numeric: 1,
                requirementCount: 3,
            };
            const complexityStatus = passwordComplexity(complexityOptions, label).validate(newPassword)

            if (complexityStatus?.error) {
                toast.error("A senha deve conter pelo menos 6 caracteres. Procure uma senha complexa: Pelo menos uma letra minuscula, uma maiuscula, um numero e um caracter especial")
                return;
            }
            const { userObject } = this.props;

            api.put(`auth/${userObject.user.id}/passwordChange`, {
                oldPassword: currentPassword,
                newPassword
            }, {headers: getHeaderWithToken()}).then(() => {
                toast.success('A tua senha foi alterada.')
                this.setState({
                    currentPassword: '',
                    newPassword: '',
                    changePassword: !this.state.changePassword
                })

            })
                .catch(err => {
                    if (err?.response?.data?.error) {
                        toast.error(err?.response?.data?.error)
                    }
                    else {
                        toast.error('An Error Ocurred')
                    }
                })
        }
        else {
            toast.warn('Escreva a senha actual a nova')
        }

    }

    render() {
        const {
            email,
            emptinessAlert,
            isValidEmail,
            lastName,
            loading,
            name,
            picSource,
            profilePicture,
            showAvatarModal,
            userName,
            emailIsEmptyBorder,
            lastNameIsEmptyBorder,
            nameIsEmptyBorder,
            userNameIsEmptyBorder,
            changePassword,
            newPassword,
            currentPassword
        } = this.state

        return (
            <>
                <ToastContainer />
                <div /*className="content"*/>
                    <Row>
                        <Col md="12">
                            <Card className="shadow-sm p-3 cardHeight-inbox">
                                <CardBody>
                                    <div
                                        style={{
                                            margin: 'auto',
                                            textAlign: 'center',
                                            height: '200px',
                                            maxHeight: '200px',
                                            width: '200px',
                                            maxWidth: '200px'
                                        }}
                                        className='row justify-content-center rounded-circle'
                                    >
                                        {
                                            (this.props.userObject.user.profilePicture === undefined || this.props.userObject.user.profilePicture === null || this.props.userObject.user.profilePicture === '')
                                                ?
                                                <img
                                                    style={{ width: '185px', height: '185px' }}
                                                    src={this.state.picSource} alt='Profile Picture'
                                                />
                                                :
                                                <img
                                                    style={{ width: '185px', height: '185px' }}
                                                    src={`${API_URL}/files/${this.props.userObject.user.profilePicture}`}
                                                    alt='Profile Picture'
                                                />
                                        }
                                    </div>

                                    <div
                                        style={{ marginTop: '10px' }}
                                        className='row justify-content-center max-width: 100%'
                                    >
                                        <button
                                            onClick={() => this.setAvatarModal(true)}
                                            className='btn text-center'
                                            type="button"
                                            style={{ backgroundColor: '#40E0D0', fontWeight: '' }}
                                        >
                                            Buscar Foto
                                        <FaPlus />
                                        </button>
                                    </div>
                                    {
                                        !emptinessAlert
                                            ?
                                            <div className="row justify-content-center mt-1">
                                                <Alert className="col-6 text-center" color="">
                                                    <span></span>
                                                </Alert>
                                            </div>
                                            :
                                            <div className="row justify-content-center mt-1">
                                                <Alert className="col-6 text-center" color="warning">
                                                    <span>Por favor, preencha os campos em falta</span>
                                                </Alert>
                                            </div>
                                    }

                                    {/*The Form!*/}
                                    <form>
                                        <div
                                            className='form-group row justify-content-center'
                                            style={{ margin: 'auto', marginTop: '' }}
                                        >
                                            <label className='col-md-1 col-form-label edit-contact-label'>Nome</label>
                                            <div className='col-md-3'>
                                                <input

                                                    onChange={this.nameChangeHandler}
                                                    className={`form-control ${nameIsEmptyBorder} register-contacts`}
                                                    type='text'
                                                    value={name}
                                                />
                                            </div>
                                            <label className='col-md-1 col-form-label edit-contact-label'>Apelido</label>
                                            <div className='col-md-3'>
                                                <input
                                                    onChange={this.lastNameChangeHandler}
                                                    className={`form-control ${this.state.lastNameIsEmptyBorder} register-contacts`}
                                                    type='text'
                                                    required
                                                    value={lastName}
                                                />
                                            </div>
                                        </div>

                                        <div
                                            className='form-group row justify-content-center'
                                            style={{ margin: 'auto', marginTop: '10px' }}
                                        >
                                            <div class="input-group col-md-8">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">@</span>
                                                </div>
                                                <input
                                                    type="email"
                                                    className={`form-control ${emailIsEmptyBorder} register-contacts`}
                                                    value={email}
                                                    onChange={this.emailChangerHandler}
                                                />
                                            </div>
                                        </div>


                                        {
                                            !isValidEmail
                                                ?
                                                <div className='form-group row justify-content-center' style={{ margin: 'auto', marginTop: '', height: '2px' }}>
                                                    <div class='form-group col-md-8'>
                                                        <span className='text-warning'>O Email é inválido</span>
                                                    </div>
                                                </div>
                                                :
                                                ''
                                        }


                                        <div className='form-group row justify-content-center' style={{ margin: 'auto', marginTop: !isValidEmail ? '20px' : '10px' }}>
                                            <div class="form-group col-md-8">
                                                <span className="edit-contact-label">User Name</span>
                                            </div>
                                        </div>

                                        <div
                                            className='form-group row justify-content-center'
                                            style={{ margin: 'auto', marginTop: '2px' }}
                                        >
                                            <div class="input-group col-md-8">
                                                <input
                                                    type="text"
                                                    className={`form-control ${userNameIsEmptyBorder} register-contacts`}
                                                    value={userName}
                                                    onChange={this.userNameChangeHandler}
                                                />
                                            </div>
                                        </div>
                                        {
                                            changePassword
                                            &&
                                            <div
                                                className='form-group row justify-content-center'
                                                style={{ margin: 'auto', marginTop: '2%' }}
                                            >
                                                <label className='col-md-1 col-form-label edit-contact-label'>Actual</label>
                                                <div className='col-md-3'>
                                                    <input

                                                        onChange={this.currentPasswordChangeHandler}
                                                        className={`form-control register-contacts`}
                                                        type='password'
                                                        value={currentPassword}
                                                    />
                                                </div>
                                                <label className='col-md-1 col-form-label edit-contact-label'>Nova</label>
                                                <div className='col-md-3'>
                                                    <input
                                                        onChange={this.newPasswordChangeHandler}
                                                        className={`form-control register-contacts`}
                                                        type='password'
                                                        required
                                                        value={newPassword}
                                                    />
                                                </div>
                                            </div>
                                        }

                                        <div
                                            className='form-group row justify-content-center'
                                            style={{ margin: 'auto', marginTop: '30px' }}
                                        >
                                            <div class="form-group col-md-8 text-left">
                                                <button
                                                    onClick={() => this.setState({ changePassword: !changePassword })}
                                                    className='btn btn-secondary'
                                                    type="button"
                                                >
                                                    {!changePassword ? "Alterar Senha" : "Voltar"} &nbsp;
                                                    {
                                                        !changePassword
                                                            ?
                                                            <i><FcSynchronize /></i>
                                                            :
                                                            <i><FiArrowLeft /></i>
                                                    }
                                                </button>
                                                {
                                                    changePassword
                                                    &&
                                                    <button
                                                        onClick={this.saveNewPassword}
                                                        className='btn btn-success ml-2'
                                                        type="button"
                                                    >
                                                        Guardar &nbsp;
                                                        <i><FaSave /></i>
                                                    </button>
                                                }
                                            </div>
                                        </div>

                                        {
                                            !changePassword
                                            &&
                                            <div
                                                className='form-group row justify-content-center'
                                                style={{ margin: 'auto', marginTop: '30px' }}
                                            >
                                                <div class="form-group col-md-8 text-right">
                                                    <button
                                                        onClick={this.saveModificationsHandler}
                                                        className='btn btn-info'
                                                        type="button"
                                                    >
                                                        Guardar &nbsp;
                                                    <i><FaSave /></i>
                                                    </button>
                                                </div>
                                            </div>
                                        }

                                    </form>
                                    <AvatarModal
                                        updatePicSource={this.updatePicSource}
                                        setAvatarModal={this.setAvatarModal}
                                        showAvatarModal={this.state.showAvatarModal}
                                    />

                                </CardBody>
                            </Card>
                        </Col>
                    </Row>
                    {
                        loading
                            ?
                            <Loading />
                            :
                            null
                    }
                </div>
            </>
        );
    }

}
