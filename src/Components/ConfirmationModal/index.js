import React, { Component } from 'react'


import Modal from 'react-bootstrap/Modal'

import './index.css'

export default class ConfirmationModal extends Component {

    constructor() {
        super()
    }

    UNSAFE_componentWillMount() {
        console.log('entered')
    }

    render() {
        const {
            showModal,
            title,
            text,
            cancelButton,
            successButton,
            handleConfirmationModal,
            handleModalResponse
        } = this.props

        return (
            <Modal
                size="md"
                show={showModal}
                class="modal fade modal-dialog"
                onHide={handleConfirmationModal}
                centered
            >
                <Modal.Header style={{ backgroundColor: '#F8F9F9' }} closeButton>
                    <div className="container">
                        <div className="row justify-content-center">
                            <h5 style={{ fontWeight: 'normal' }}>{title}</h5>
                        </div>
                    </div>
                </Modal.Header>

                <Modal.Body>
                    <div className="container">
                        <div className="row justify-content-center">
                            <h6 style={{ fontWeight: 'normal' }}>{text}</h6>
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <div className="container">
                        <div className="row justify-content-center">
                            <button
                                style={{ backgroundColor: '#E74C3C' }}
                                onClick={() => handleModalResponse(0)}
                                className="btn col-2 mr-2"
                                id='cancelButton'
                            >
                                {cancelButton}
                                </button>
                            <button
                                style={{ backgroundColor: '#2ECC71' }}
                                onClick={() => handleModalResponse(1)}
                                className="btn col-2"
                                id='successButton'
                            >
                                {successButton}
                            </button>
                        </div>
                    </div>
                </Modal.Footer>
            </Modal>
        )

    }
}

