import React, { Component } from 'react'
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'
import { BrowserRouter as Router, Link } from 'react-router-dom';
import './modalRespondComment.css';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import axios from 'axios';
import Loading from '../Loading'
import api from '../../Services/api';
import {getHeaderWithToken} from "../../helpers/authHelper";

export default class ModalRespondComment extends Component {

    constructor(props) {
        super(props)
        this.state = {
            contacto: '',
            resposta: '',
            loading: false
        }

        this.onRespostaChange = this.onRespostaChange.bind(this);
    }

    onRespostaChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }

    async onSubmitAnswer() {
        if (this.state.resposta === '') {
            toast.warn('O campo de resposta está vazio!')
        } else {
            this.setState({ loading: true })
            this.props.handleClose(false)

            //commentId, answerText

            api.put('publicationComments/replyComment',
                {
                    commentId: this.props.idComentarioRespondido,
                    answerText: this.state.resposta
                }, { headers: getHeaderWithToken() }).then(response => {
                    this.setState({ loading: false })
                    toast.success('Sua resposta foi Enviada.')
                    this.props.marcarComentarioRespondido(this.props.idComentarioRespondido)
                })
                .catch((err) => {
                    this.setState({ loading: false })
                    toast.error('❌ Não foi possível responder comentário. Tente novamente ou verifique sua conexão')
                })
        }
    }

    render() {
        return (
            <div id="modalAnswer">
                {(this.state.loading === true)
                    ? <Loading />
                    : null
                }
                <ToastContainer />
                <Modal show={this.props.show} onHide={() => this.props.handleClose(false)} style={{ marginTop: "8%" }}>
                    <Modal.Body>
                        <form>
                            <label>To:<input className="form-control form-control-sm inputs" type="text" name='contacto'
                                defaultValue={this.props.contacto} readOnly /></label>

                            <textarea className="form-control inputs" id="exampleFormControlTextarea1" name='resposta'
                                rows="5"
                                placeholder="Escreva aqui a resposta"
                                onChange={this.onRespostaChange}></textarea>
                        </form>

                    </Modal.Body>
                    <Modal.Footer >
                        <Button variant="secondary" onClick={() => this.props.handleClose(false)}>Cancelar</Button>
                        <Button variant="primary" onClick={() => this.onSubmitAnswer()}>Enviar</Button>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}
