import React, { useEffect, useState} from "react";
import {FaRegBell} from 'react-icons/fa';
import {FiUser, FiRefreshCw, FiLogOut, FiMessageSquare} from 'react-icons/fi'
import UserAvatar from 'react-user-avatar';
import './User.css';
import Badge from '@material-ui/core/Badge';

import { Link, useHistory} from 'react-router-dom';

import api from "../../Services/api";
import {API_URL} from "../../helpers/api";
import {getHeaderWithToken, getUserPermissions} from "../../helpers/authHelper";

function UserHeader({messages, userObject, logout}) {
    const {push} = useHistory()

    const [divClass, setDivClass] = useState('hide')
    const [comments, setComments] = useState([])
    const [notificationsQuantity, setNotificationsQuantity] = useState(0)

    useEffect(() => {

        api.get('publicationComments', {headers: getHeaderWithToken()}).then(response => {
            setComments(response.data)
        })
    }, [])

    useEffect(() => {
        sessionStorage.setItem('comentarios', JSON.stringify(comments));
        let quantity = 0;

        comments.forEach(comment => {
            if (!comment.seen) {
                quantity++;
            }
        })

        setNotificationsQuantity(quantity)
    }, [comments])

    function changeStatus() {
        let status;
        if (divClass === 'ver') {
            status = "hide";
        } else {
            status = "ver";
        }
        setDivClass(status)
    }

    function openNotificationHandler() {
        push("/dashboard/Notificacoes")
        setNotificationsQuantity(0)
    }

    return (
        <div>
            <nav id="navItems">
                <ul className="navbar-nav">

                    {
                        getUserPermissions().includes('P_PUBLISH_CONTENT')
                        &&
                        <li
                            onClick={() => push("/dashboard/caixa-de-emails")}
                            style={{cursor: "pointer"}}
                            data-toggle="tooltip"
                            data-placement="top"
                            title="Emails"
                        >
                            <Badge badgeContent={messages?.unread} color="secondary">
                                <FiMessageSquare size={25} color="#6c757d"/>
                            </Badge>
                        </li>
                    }

                    {
                        getUserPermissions().includes('P_MANAGE_NOTIFICATIONS')
                        &&
                        <li
                            onClick={openNotificationHandler}
                            style={{cursor: "pointer"}}
                            data-toggle="tooltip"
                            data-placement="top"
                            title="Notificações"
                        >
                            <Badge badgeContent={notificationsQuantity} color="secondary">
                                <FaRegBell size={25} color="#6c757d"/>
                            </Badge>
                        </li>
                    }
                    {
                        (userObject.user.profilePicture === undefined || userObject.user.profilePicture === null || userObject.user.profilePicture === '')
                            ? <li className="font-weight-bold text-capitalize" style={{cursor: "pointer"}}
                                  onMouseOverCapture={changeStatus} onMouseOut={changeStatus}><UserAvatar size="30"
                                                                                                          color="#00BFFF"
                                                                                                          name={`${userObject.user.name} ${userObject.user.lastName}`}/>
                            </li>
                            : <li className="font-weight-bold text-capitalize" style={{cursor: "pointer"}}
                                  onMouseOverCapture={changeStatus} onMouseOut={changeStatus}><UserAvatar size="30"
                                                                                                          color="#00BFFF"
                                                                                                          name={`${userObject.user.name} ${userObject.user.lastName}`}
                                                                                                          src={`${API_URL}/files/${userObject.user.profilePicture}`}/>
                            </li>
                    }

                    <li
                        className="font-weight-bold text-capitalize"
                        id="username"
                        style={{cursor: "pointer"}}
                    >
                        {`${userObject.user.name} ${userObject.user.lastName}`}
                    </li>
                </ul>
            </nav>

            <div id="menuUser" className={divClass} onMouseOverCapture={changeStatus} onMouseOut={changeStatus}>
                <ul>
                    <li>
                        <Link to={"/dashboard/perfil"}>
                            <FiUser className="sideIcons"/>Meu Perfil
                        </Link>
                    </li>
                    <li>
                        <Link to={"/dashboard/perfil"}>
                            <FiRefreshCw className="sideIcons"/>Alterar Senha
                        </Link>
                    </li>
                    <li onClick={logout}><Link><FiLogOut className="sideIcons"/>Sair do Sistema</Link></li>
                </ul>
            </div>
        </div>
    );
}

export default UserHeader;


// export default class Header extends Component {

//     constructor(props) {
//         super(props);
//         this.state = {
//             divClass: 'hide',
//             comentarios: [],
//             notificationsQuantity: 0
//         }
//         console.log(this.props.userObject)
//     }

//     async UNSAFE_componentWillMount() {
//         console.log(this.props.userObject)
//         await fetch('http://localhost:8100/VerComentario', {
//             method: 'GET',
//             headers: { 'Content-Type': 'application/json' },
//         })
//             .then(response => response.json())
//             .then(data => {
//                 if (data) {
//                     this.setState({ comentarios: data });
//                     sessionStorage.setItem('comentarios', JSON.stringify(this.state.comentarios));
//                 }
//             })

//         var quant = 0;
//         console.log(this.state.comentarios)
//         this.state.comentarios.map((com, i) => {
//             if (com.seen === false) {
//                 quant = quant + 1;
//             }
//         })

//         this.setState({ notificationsQuantity: quant });
//     }

//     onNotificationsOpen() {
//         this.setState({ notificationsQuantity: 0 });
//     }

//     alterarEstado() {
//         var estado;
//         if (this.state.divClass === 'ver') {
//             estado = "hide";
//         } else {
//             estado = "ver";
//         }
//         this.setState({
//             divClass: estado
//         })
//     }

//     render(props) {
//         const { userObject } = this.props;
//         const { notificationsQuantity } = this.state;
//         return (
//             <div>
//                 <nav id="navItems">
//                     <ul className="navbar-nav">

//                         <li
//                             style={{ cursor: "pointer" }}
//                             data-toggle="tooltip"
//                             data-placement="top"
//                             title="Emails"
//                         >
//                             <Badge badgeContent={4} color="secondary">
//                                 <FiMessageSquare size={25} color="#6c757d" />
//                             </Badge>
//                         </li>

//                         <li
//                             onClick={this.onNotificationsOpen.bind(this)}
//                             style={{ cursor: "pointer" }}
//                             data-toggle="tooltip"
//                             data-placement="top"
//                             title="Notificações"
//                         >
//                             <Link to={"/dashboard/Notificacoes"}>
//                                 <Badge badgeContent={notificationsQuantity} color="secondary">
//                                     <FaRegBell size={25} color="#6c757d" />
//                                 </Badge>
//                             </Link>
//                         </li>
//                         {
//                             (userObject.profilePicture === undefined || userObject.profilePicture === null || userObject.profilePicture === '')
//                                 ? <li className="font-weight-bold text-capitalize" style={{ cursor: "pointer" }} onMouseOverCapture={changeStatus} onMouseOut={changeStatus}><UserAvatar size="30" color="#00BFFF" name={`${this.props.userObject.user.name} ${this.props.userObject.user.lastName}`} /> </li>
//                                 : <li className="font-weight-bold text-capitalize" style={{ cursor: "pointer" }} onMouseOverCapture={changeStatus} onMouseOut={changeStatus}><UserAvatar size="30" color="#00BFFF" name={`${this.props.userObject.user.name} ${this.props.userObject.user.lastName}`} src={`http://localhost:8100/profile/${this.props.userObject.profilePicture}`} /> </li>
//                         }

//                         <li className="font-weight-bold text-capitalize" id="username" style={{ cursor: "pointer" }}> {`${this.props.userObject.user.name} ${this.props.userObject.user.lastName}`} </li>
//                     </ul>
//                 </nav>

//                 <div id="menuUser" className={`${this.state.divClass}`} onMouseOverCapture={changeStatus} onMouseOut={changeStatus}>
//                     <ul>
//                         <li><Link to={"/dashboard/perfil"}><FiUser className="sideIcons" />Meu Perfil</Link></li>
//                         <li><Link to={"/dashboard/perfil"}><FiRefreshCw className="sideIcons" />Alterar Senha</Link></li>
//                         <li onClick={this.props.logout}><Link><FiLogOut className="sideIcons" />Sair do Sistema</Link></li>
//                     </ul>
//                 </div>
//             </div>
//         );
//     }
// }
