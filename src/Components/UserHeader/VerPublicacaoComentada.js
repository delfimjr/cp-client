import React, { Component } from "react";
import '../Publicacoes/publicacoes.css';
import history from '../../history'
import { withRouter } from 'react-router-dom';
import api from "../../Services/api";
import PostCardContainer from "../Publicacoes/PostCardContainer";
import {getHeaderWithToken} from "../../helpers/authHelper";


class VerPublicacaoComentada extends Component {

    constructor(props) {
        super(props)
        this.state = {
            post: null,
            show: false,
            loading: false
        }

        this.handleShow = this.handleShow.bind(this);
        this.handleClose = this.handleClose.bind(this);
    }


    async handleShow(estado) {
        await this.setState({ show: estado })
    }

    handleClose(estado) {
        this.setState({ show: estado })
    }

    UNSAFE_componentWillMount() {
        this.setState({ loading: true })
        this.fetchPublications();
    }


    // fetching posts from database
    fetchPublications = async () => {
        await api.get(`publications/${this.props.location.state.publicationId}`, {headers: getHeaderWithToken()}).then(response => {
            this.setState({ post: response.data })
        })
    }

    goBack() {
        history.goBack();
    }


    render() {
        return (
            this.post?.map(p => {
                let data = p.created_at.split('T');
                let hora = data.pop();
                hora = hora.split(':');
                hora.pop();

                return (
                    < PostCardContainer
                        cardId='card'
                        pub={p}
                        fromNotifications
                    />
                )
            })
        )

    }
}

export default withRouter(VerPublicacaoComentada);
