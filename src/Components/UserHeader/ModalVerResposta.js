import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'

import Modal from 'react-bootstrap/Modal'
import { Button } from 'react-bootstrap';
import { FaRegCalendarAlt } from 'react-icons/fa';

import history from '../../history'
import './notificacoes.css';
import axios from 'axios';
import api from '../../Services/api';
import dateOrganizeHelper from '../../helpers/dateOrganizeHelper'
import {getHeaderWithToken} from "../../helpers/authHelper";

class ModalVerResposta extends Component {

    constructor(props) {
        super(props)
        this.state = {
            show: true,
            answer: {},
            loading: true
        }
    }

    async UNSAFE_componentWillMount() {
        //a.slice(a.lastIndexOf('/')+1,a.length)
        api.get(`publicationComments/responses/${this.props.location.pathname.slice(this.props.location.pathname.lastIndexOf('/') + 1, this.props.location.pathname.length)}`, {headers: getHeaderWithToken()})
            .then(response => {
                console.log(response.data)
                this.setState({ answer: response.data })
                console.log(this.state.answer)
                this.setState({ loading: false });
            })
    }

    handleCloseModal = () => {
        this.setState({ show: false })
        history.push('/dashboard/Notificacoes/')
    }

    render() {
        const { answer } = this.state;
        return (
            <div>
                <Modal
                    size="md"
                    show={this.state.show}
                    class="modal fade modal-dialog"
                    animation
                    onHide={this.handleCloseModal}
                >
                    <Modal.Body>
                        {
                            (this.state.loading === true)
                                ?
                                <div className="d-flex justify-content-center">
                                    <div class="spinner-border text-primary" role="status">
                                        <span class="sr-only">Loading...</span>
                                    </div>
                                </div>
                                :
                                <>
                                    <label className="labels mr-4"><FaRegCalendarAlt size={15} className="mr-2" />{answer && dateOrganizeHelper(answer.answeredAt)}</label>
                                    <label className="labels">Por: {answer.answeredBy}</label>
                                    <h6 style={{ marginBottom: '1px' }}>Resposta</h6>
                                    <p>{answer.answerText}</p>
                                </>
                        }
                    </Modal.Body>

                    <Modal.Footer>
                        <Button variant="secondary" className="mr-2" onClick={this.handleCloseModal}>Fechar</Button>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}

export default withRouter(ModalVerResposta);
