import React, { useState, useEffect, useRef } from "react";
import { IoMdInformationCircleOutline } from 'react-icons/io'
import ExclamationMark from '../../Images/ExclamationMark.svg'
import { IoIosClose } from 'react-icons/io';
import ModalRespondComment from './ModalRespondComment';
import './notificacoes.css';
import { Badge } from 'reactstrap';
import { Route } from 'react-router-dom'
import api from '../../Services/api'
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import ModalVerResposta from './ModalVerResposta'
import history from "../../history";
import { FiEye } from "react-icons/fi";
import {getHeaderWithToken} from "../../helpers/authHelper";


const LIMIT = 2
let data;
let hora;
let bgcolor;

const Notificacoes = (props) => {

    const [comentarios, setComentarios] = useState([])
    const [loading, setLoading] = useState(true)
    const [contacto, setContacto] = useState('')
    const [idComentarioRespondido, setIdComentarioRespondido] = useState('')
    const [show, setShow] = useState(false)
    const [total, setTotal] = useState(0)
    const [currentPage, setCurrentPage] = useState(1)
    const [auxLoading, setAuxLoading] = useState(false)


    function handleShow(flag) {
        setShow(flag)
    }

    function handleClose(flag) {
        setShow(flag)
    }

    useEffect(() => {
        fetchNotifications();
    }, [])


    useEffect(() => {
        fetchNotifications()
    }, [currentPage])


    useEffect(() => {
        window.addEventListener("scroll", handleScroll)
        return () => window.removeEventListener("scroll", handleScroll)
    }, [handleScroll])


    function handleScroll() {
        const scrollable = document.documentElement.scrollHeight - window.innerHeight
        const scrolled = window.scrollY

        if (Math.ceil(scrolled) === scrollable && currentPage < total) {
            setAuxLoading(true)
            setCurrentPage(currentPage + 1)
        }
    }

    async function fetchNotifications() {
        api.get(`publicationComments/indexed?page=${currentPage}&limit=${LIMIT}`, {headers: getHeaderWithToken()}).then(response => {
            setLoading(false)
            setAuxLoading(false)
            setComentarios([...comentarios, ...response.data.data])

            const totalPages = Math.ceil((response.data.count / LIMIT))
            setTotal(totalPages)
        })
            .catch(() => toast.error("Sorry!!! It was not possible to retrieve notifications! Verify your connection, please!"))
    }


    useEffect(() => {

        const unseenComments = comentarios.filter(comment => !comment.seen)

        if (comentarios?.length > 0 && unseenComments?.length > 0) {

            api.put('publicationComments/commentOpen', {}, {headers: getHeaderWithToken()})

            setTimeout(() => {
                const newComments = comentarios.map(comment => {
                    return { ...comment, seen: true }
                })
                setComentarios(newComments)
            }, 5000)
        }
    }, [comentarios])


    async function marcarComentarioRespondido(idComentario) {

        const newComments = comentarios.map(comment => {
            return comment.id === idComentario ? { ...comment, answered: true } : comment
        })

        sessionStorage.setItem('comentarios', JSON.stringify(newComments));
        setComentarios(newComments)
        await setComentarios(JSON.parse(sessionStorage.getItem('comentarios')))
    }

    function answerToComment(contact, commentId) {
        setContacto(contact)
        setIdComentarioRespondido(commentId)
        handleShow(true)
    }

    useEffect(() => {
        /*fetchNotifications()*/
    }, [comentarios]);

    function seeAnswer(id) {
        history.push(`/dashboard/Notificacoes/respostas/${id}`, { id })
    }


    return (
        <div>
            <div id="divNotif">
                {
                    (loading === true)
                        ? <div className="d-flex justify-content-center">
                            <div class="spinner-border text-primary" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </div>
                        : (comentarios.length === 0
                            ? <div style={{ textAlign: "center" }}>
                                <img src={ExclamationMark} width="260px" />
                                <h3 className="mt-4 text-secondary" style={{ textAlign: "center" }}>Sem Notificações</h3>
                            </div>

                            : comentarios.map((comment, i) => {
                                data = comment.commented_at.split('T');
                                hora = data.pop();
                                hora = hora.split(':');
                                hora.pop();

                                if (comment.seen === false) {
                                    bgcolor = "#80c3d8";
                                } else {
                                    if (comment.seen === true) {
                                        bgcolor = "#daedf4";
                                    }
                                }

                                return (
                                    <div className='container p-3 mb-3 rounded' style={{ backgroundColor: `${bgcolor}`, border: "solid 1px #b2dbe7" }}>
                                        <div id="notifColum1">
                                            <label id="info">
                                                <IoMdInformationCircleOutline size={22} />
                                                <label className="labels ml-2 mr-3">{data + ', ' + hora.join(':')}</label>
                                                <h6 class="text-info">Publicação comentada</h6>
                                                <div class="column">
                                                    <div style={{ marginTop: 0, display: 'flex', flexDirection: 'row' }}>
                                                        <p style={{ marginRight: '1%', fontWeight: 'bold' }}>Assunto:</p>
                                                        <p>{comment.publicationSubject}</p>
                                                    </div>
                                                    <div style={{ marginTop: 0, display: 'flex', flexDirection: 'row' }}>
                                                        <p style={{ marginRight: '1%', fontWeight: 'bold' }}>Corpo:</p>
                                                        <p>{comment.publicationBody}</p>
                                                    </div>
                                                </div>
                                                <hr style={{ background: "#5DADE2" }} />
                                            </label>

                                            <label id="notifColum2">
                                                {
                                                    (comment.answered === true)
                                                        ?
                                                        <span onClick={() => seeAnswer(comment.id)}>
                                                            <Badge
                                                                className="mr-4"
                                                                style={{ cursor: "pointer" }}
                                                                data-toggle="tooltip"
                                                                data-placement="top"
                                                                title="Ver resposta"
                                                            >
                                                                Ver Resposta <FiEye />
                                                            </Badge>
                                                        </span>
                                                        : null
                                                }
                                                <label onClick={() => { }}><IoIosClose size={20} style={{ cursor: "pointer" }} data-toggle="tooltip" data-placement="top" title="Remover" /></label>
                                            </label>
                                        </div>

                                        {(comment.authorName !== '')
                                            ? <p className="paragrafo">{comment.authorName} comentou:</p>
                                            : <p className="paragrafo">Alguém comentou:</p>
                                        }

                                        <p className="paragrafo">"{comment.commentBody}"</p>

                                        <div className='pl-2 p-1 rounded' id="botaoResponder" onClick={() => answerToComment(comment.authorContact, comment.id)}>Responder</div>
                                    </div>
                                );
                            })
                        )
                }
            </div>

            <ModalRespondComment
                handleShow={() => handleShow()}
                handleClose={() => handleClose()}
                contacto={contacto}
                show={show}
                marcarComentarioRespondido={marcarComentarioRespondido}
                idComentarioRespondido={idComentarioRespondido}
                userObject={props.userObject}
            />

            <Route
                path="/dashboard/Notificacoes/respostas/:id"
                render={({ }) => {
                    return (
                        <ModalVerResposta userObject={props.userObject} />
                    );
                }}
            />
        </div>
    );
}

export default Notificacoes;
