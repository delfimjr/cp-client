import React, { Component } from 'react'
import { FaFilter, FaSearch, FaSave } from "react-icons/fa";
import Modal from 'react-bootstrap/Modal'
import { useAlert } from 'react-alert'

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import './styles.css'

import api from '../../Services/api'
import {getHeaderWithToken} from "../../helpers/authHelper";

//toast.configure()

export default class CreateGroupModal extends Component {

    constructor(props) {
        super(props)
        this.state = {
            groupName: '',
            members: [],
            searchInput: '',
            companySelect: '',
            genderSelect: '',
            isEmptyGroupName: '',
            showModal: false,
            contacts: [],
            companies: [],
            markButtonValue: "Marcar Todos",
            allRowsMarked: false,

            formInput: {
                groupName: '',
                description: 'Sem Descrição'
            },
            loading: true
        }
    }

    async componentWillMount() {

        await api.get('/contacts', {headers: getHeaderWithToken()}).then(async response => {
            console.log("contacts", response)
            const auxContacts = response.data.map(contact => {
                if (contact.department === null) {
                    contact.department = ""
                }
                if (contact.company === null) {
                    contact.company = `MPDC - ${contact.department}`
                }
                if (contact.designation === null) {
                    contact.designation = ""
                }

                return { ...contact, marked: false }
            })
            await this.setState({ contacts: auxContacts })

            const auxCompanies = this.state.contacts.map(contact => {
                return contact.company
            })
            await this.setState({ companies: [...new Set(auxCompanies)] })
            this.setState({ loading: false })
        })
        .catch(err => console.log(err))
    }


    handleOnChangeInput = async (event) => {
        const { name, value } = event.target

        await this.setState({ formInput: { ...this.state.formInput, [name]: value } })
        console.log(this.state.formInput)
    }

    onSearchChange = (event) => {
        this.setState({ searchInput: event.target.value })
    }

    onCompanyChange = (event) => {
        this.setState({ companySelect: event.target.value })
    }

    onGenderChange = (event) => {
        this.setState({ genderSelect: event.target.value })
    }

    handleGroupSave = async () => {
        const { handleCreateGroupModal, updateGroupState } = this.props
        const { members } = this.state;
        const { groupName, description } = this.state.formInput

        if (groupName && members.length >= 2) {


            const data = {
                groupName: groupName,
                description: description,
                contactsIds: members
            }

            await api.post('groups', data,{headers: getHeaderWithToken()})
                .then(response => {
                        toast.success(`Grupo ${groupName} Criado!` )
                })
                .catch(err => {
                    if(err?.response?.data?.error){
                        toast.warning(err?.response?.data?.error)
                    }
                    else {
                        toast.error('❌ Não foi possível criar o grupo. Tente novamente ou verifique a conexão')
                    }
                })
            //Fetching to obtain updated groups
            updateGroupState()
            //closing Modal
            handleCreateGroupModal(false)
        }
        else {
            if (groupName === '' && members) {
                this.setState({ isEmptyGroupName: 'border border-danger' })
                alert('Escolha um nome para o grupo.')
            }
            else {
                toast.warning('Um grupo deve ter no minimo 2 elementos. Por favor, adicione membros ao grupo...')
            }
        }

    }

    markAll = async () => {
        const { allRowsMarked } = this.state

        await this.setState({ allRowsMarked: !allRowsMarked })

        let input = document.getElementsByTagName('input');

        if (this.state.allRowsMarked === true) {
            for (let i = 0; i < input.length; i++) {
                if (input[i].type === 'checkbox') {
                    if (input[i].checked === false) {
                        input[i].checked = true;
                        this.handleAddMembers(Number(input[i].id))
                    }

                }
            }
            await this.setState({ markButtonValue: 'Desmarcar Todos' });
        }
        else {
            for (let i = 0; i < input.length; i++) {
                if (input[i].type === 'checkbox') {
                    if (input[i].checked === true) {
                        input[i].checked = false;
                        this.handleAddMembers(Number(input[i].id))
                    }

                }
            }
            await this.setState({ markButtonValue: 'Marcar Todos' });
        }
    }

    //This method governs the contacts table by the Selects and Search input content!
    filterContacts = () => {
        const { contacts, searchInput, companySelect, genderSelect } = this.state;
        let filteredContacts;

        if (companySelect === '' || companySelect === 'Companhia') {
            if (genderSelect === '' || genderSelect === 'gender') {
                if (searchInput === '') {
                    filteredContacts = contacts;
                }
                else {
                    filteredContacts = contacts.filter(contact => {
                        return contact?.email?.toLowerCase().includes(searchInput?.toLowerCase());
                    })
                }
            }
            else {
                if (searchInput === '') {
                    filteredContacts = contacts.filter(contact => {
                        return contact?.gender?.toLowerCase().includes(genderSelect?.toLowerCase());
                    })
                }
                else {
                    const auxFilter = contacts.filter(contact => {
                        return contact?.gender?.toLowerCase().includes(genderSelect?.toLowerCase());
                    })

                    filteredContacts = auxFilter.filter(contact => {
                        return contact?.email?.toLowerCase().includes(searchInput?.toLowerCase());
                    })
                }
            }

        }
        else {
            if (genderSelect === '' || genderSelect === 'gender') {
                if (searchInput === '') {
                    filteredContacts = contacts.filter(contact => {
                        return contact?.company?.toLowerCase().includes(companySelect?.toLowerCase());
                    })
                }
                else {
                    const auxFilter = contacts.filter(contact => {
                        return contact?.company?.toLowerCase().includes(companySelect?.toLowerCase());
                    })

                    filteredContacts = auxFilter.filter(contact => {
                        return contact?.email?.toLowerCase().includes(searchInput?.toLowerCase());
                    })
                }
            }
            else {
                if (searchInput === '') {
                    const auxFilter = contacts.filter(contact => {
                        return contact?.company?.toLowerCase().includes(companySelect?.toLowerCase());
                    })

                    filteredContacts = auxFilter.filter(contact => {
                        return contact?.gender?.toLowerCase().includes(genderSelect?.toLowerCase());
                    })
                }
                else {
                    const auxFilter1 = contacts.filter(contact => {
                        return contact?.company?.toLowerCase().includes(companySelect?.toLowerCase());
                    })

                    const auxFilter2 = auxFilter1.filter(contact => {
                        return contact?.gender?.toLowerCase().includes(genderSelect?.toLowerCase());
                    })

                    filteredContacts = auxFilter2.filter(contact => {
                        return contact?.email?.toLowerCase().includes(searchInput?.toLowerCase());
                    })

                }
            }
        }
        return filteredContacts;
    }

    handleAddMembers = async id => {
        const { members, contacts } = this.state

        const index = members.indexOf(id)
        console.log(index)
        if (index === -1) {
            members.push(id)
        }
        else {
            members.splice(index, 1)
        }

        const auxContacts = contacts.map(contact => {
            if (contact.id === id) {
                if (contact.marked === false) {
                    contact.marked = true
                }
                else {
                    contact.marked = false
                }
            }
            return contact
        })
        console.log(auxContacts)
        this.setState({ contacts: auxContacts })
    }

    render(props) {
        let filteredContacts = this.filterContacts();
        const { contacts, companies, loading } = this.state
        return (
            <>
            <ToastContainer />
            <Modal
                size="lg"
                show={this.props.showCreateGroupModal}
                class="modal fade modal-dialog"
                aria-labelledby="example-modal-sizes-title-lg"
                onHide={() => this.props.handleCreateGroupModal(false)}
            >
                <Modal.Header>
                    <div className="container">
                        <div className="row justify-content-between">
                            <h4>Criação de Grupos de Destinatário</h4>
                            <button
                                onClick={this.handleGroupSave}
                                className="btn btn-info"
                                type="button"
                            >

                                Guardar &nbsp;
                                <i><FaSave /></i>
                            </button>
                        </div>
                    </div>

                </Modal.Header>
                <Modal.Body>
                    <div className="container">
                        <div className='row'>
                            <div className='col-sm-4'>
                                <input
                                    onChange={this.handleOnChangeInput}
                                    name='groupName'
                                    className={`form-control color ${this.state.isEmptyGroupName}`}
                                    type='text'
                                    placeholder='Nome do Grupo' />
                            </div>

                            <div className='col-sm-4'>
                                <input
                                    onChange={this.handleOnChangeInput}
                                    name='description'
                                    className={`form-control color ${this.state.isEmptyGroupName}`}
                                    type='text'
                                    placeholder='Descrição (Opcional)' />
                            </div>
                        </div>

                        <div className="row mt-4">
                            <div className='col-sm-4'>
                                <input
                                    className='form-control'
                                    type='search'
                                    placeholder='procurar...'
                                    onChange={this.onSearchChange}
                                />
                            </div>
                            <div className='col-sm-4'>

                                <select onChange={this.onCompanyChange} className='form-control'>
                                    <option>Companhia</option>
                                    {
                                        companies.map(company => {
                                            return <option>{company}</option>
                                        })
                                    }
                                </select>
                            </div>

                            <div className='col-sm-4'>
                                <select onChange={this.onGenderChange} className='form-control'>
                                    <option value='gender'>Género</option>
                                    <option value='F'>Femenino</option>
                                    <option value='M'>Masculino</option>
                                </select>
                            </div>
                        </div>

                        {
                            (loading)
                                ?
                                <div class="d-flex justify-content-center">
                                    <div class="spinner-border text-primary" role="status">
                                        <span class="sr-only">Loading...</span>
                                    </div>
                                </div>
                                :
                                (filteredContacts.length)
                                    ?
                                    <div className=" row justify-content-start overflow-auto mt-1" style={{ height: '60vh' }}>
                                        <table class="table table-borderless" style={{ cursor: 'pointer' }}>
                                            <thead className="">
                                                <tr>
                                                    <th scope="col">#</th>
                                                    <th scope="col">Email</th>
                                                    <th scope="col">Companhia</th>
                                                    <th scope="col">Função/Cargo</th>
                                                    <th
                                                        className='btn'
                                                        data-toggle="tooltip"
                                                        data-placement="top"
                                                        title="Macar todos"
                                                        style={{ backgroundColor: '#CCD1D1 ', color: '' }}
                                                        scope='col' className='dim'
                                                        onClick={this.markAll}>{this.state.markButtonValue}</th>
                                                </tr>
                                            </thead>
                                            <tbody >
                                                {
                                                    filteredContacts.map(
                                                        (contact, i) =>
                                                            <tr key={contact.id}>
                                                                <th scope="row">{i + 1}</th>
                                                                <td>{contact.email}</td>
                                                                <td>{contact.company}</td>
                                                                <td>{contact.designation}</td>
                                                                <td className='text-center'>
                                                                    {
                                                                        (contact.marked)
                                                                            ?
                                                                            <input
                                                                                onChange={() => this.handleAddMembers(contact.id)}
                                                                                className='check'
                                                                                id={contact.id}
                                                                                type='checkbox'
                                                                                checked
                                                                            />
                                                                            :
                                                                            <input
                                                                                onChange={() => this.handleAddMembers(contact.id)}
                                                                                className='check'
                                                                                id={contact.id}
                                                                                type='checkbox'
                                                                            />
                                                                    }
                                                                </td>
                                                            </tr>
                                                    )
                                                }
                                            </tbody>
                                        </table>
                                    </div>
                                    :
                                    (
                                        (contacts !== 0)
                                            ?
                                            <div className=" row justify-content-center overflow-auto mt-4" style={{ height: '55vh' }}>
                                                <h3 className='row justify-content-center text-secondary' >Nenhum contacto foi encontrado</h3>
                                            </div>
                                            :
                                            <div className=" row justify-content-center overflow-auto mt-4" style={{ height: '55vh' }}>
                                                <h3 className='row justify-content-center text-secondary' >
                                                    Ainda nao registou contactos. Por favor, registe contactos para criar grupos
                                                </h3>
                                            </div>
                                    )

                        }
                    </div>

                </Modal.Body>
            </Modal >
            </>
        )
    }
}
