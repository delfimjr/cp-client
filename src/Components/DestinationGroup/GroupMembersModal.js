import React, {Component} from 'react'
import {withRouter} from 'react-router-dom'
import Modal from 'react-bootstrap/Modal'
import {Table} from "reactstrap";
import history from '../../history'

import {FaPlus, FaRegTrashAlt, FaSave} from 'react-icons/fa'
import {TiArrowLeftThick} from 'react-icons/ti'

import api from '../../Services/api'
import {toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {getHeaderWithToken} from "../../helpers/authHelper";


//toast.configure()


class GroupMembersModal extends Component {

    constructor(props) {
        super(props)
        this.state = {
            show: '',
            members: [],
            newMembers: [],
            groupId: '',
            searchField: '',
            newMembersList: false,
            loading: true
        }
    }

    async UNSAFE_componentWillMount() {
        this.defaultFetch()
    }

    defaultFetch = () => {
        const groupId = this.props.match.params.id
        this.setState({groupId})
        this.setState({show: true})


        api.get(`groupContacts/ByGroup/${groupId}`, {headers: getHeaderWithToken()}).then(async response => {
            console.log("RESPONSE", response)
            await this.setState({members: response.data})
            this.setState({loading: false})

        })
    }

    handleDeleteGroup = async groupId => {
        console.log("Ya")
        await api.delete(`groups/${groupId}`, {headers: getHeaderWithToken()})
            .then(() => toast.success(`${this.props.location.state.groupName} apagado!`))
            .catch(() => alert('Falha na remocao...'))

        //Updating the state in GroupCreation.js Group state to perform an updated render
        this.props.updateGroupState();

        history.push('/dashboard/configuracoes/grupos')
    }


    handleBack = () => {
        if (this.state.newMembersList) {
            this.defaultFetch()
            this.setState({newMembersList: false})
        } else {
            history.push('/dashboard/configuracoes/grupos')
        }

    }

    handleRemoveMember = async contactId => {
        const {members} = this.state
        const {groupId} = this.state

        const data = {
            contactId,
            groupId
        }

        if (members.length > 2) {
            api.delete('groupContacts', {headers: getHeaderWithToken(), data})
                .then(() => {
                    const auxMember = members.filter((member, i) => {
                        return member.id !== contactId
                    })
                    this.setState({members: auxMember})
                })
                .catch(() => alert('An error ocurred!'))
        } else {
            toast.warning(`Um grupo deve ter no minimo 2 membros. Se apagar este o grupo ficará apenas com 1 membro.`)
        }

    }

    handleAddMembers = groupId => {
        this.setState({loading: true})
        api.get(`groupContacts/freeContacts/${groupId}`).then((response) => {

            const auxMembers = response.data.map(contact => {
                return {...contact, marked: false}
            })
            this.setState({members: auxMembers})
            this.setState({newMembersList: true})
            this.setState({loading: false})

        })
    }

    onSearchField = async (event) => {
        await this.setState({searchField: event.target.value})
    }

    handleNewMember = async id => {
        const {members, newMembers} = this.state

        const index = newMembers.indexOf(id)
        console.log(index)
        if (index === -1) {
            newMembers.push(id)
        } else {
            newMembers.splice(index, 1)
        }

        const auxMembers = members.map(member => {
            if (member.id === id) {
                if (member.marked === false) {
                    member.marked = true
                } else {
                    member.marked = false
                }
            }
            return member
        })

        this.setState({members: auxMembers})
    }

    handleStoreMembers = groupId => {
        const {newMembers} = this.state
        if (newMembers.length !== 0) {

            //this.setState({ loading: true })
            api.post('groupContacts', {
                groupId,
                contactsIds: newMembers
            }, {headers: getHeaderWithToken()}).then((response) => {
                //this.setState({ loading: false })
                this.defaultFetch()
                this.setState({newMembersList: false})

                toast.info(`Grupo ${this.props.location.state.groupName} actualizado`)
            })
                .catch(err => {
                    console.log("err")
                })
        } else {
            toast.warning(`Nenhum contacto foi escolhido.`)
        }
    }

    render() {

        const {groupId, members, searchField, newMembersList, loading} = this.state

        const filteredMembers = members.filter(member => {
            return (member?.name + member.lastName).toLowerCase().includes(searchField?.toLowerCase())
        })

        return (
            <>
                <Modal
                    centered
                    size="lg"
                    show={this.state.show}
                    onHide={this.handleBack}
                >
                    <Modal.Header style={{backgroundColor: '#F8F9F9'}}>
                        <div className="container">
                            <div className="row justify-content-between">
                                <h4 className=''>{this.props.location.state.groupName}</h4>
                                <button
                                    onClick={() => this.handleDeleteGroup(groupId)}
                                    className="btn btn-danger"
                                >
                                    Apagar Grupo
                                </button>
                            </div>
                        </div>

                    </Modal.Header>

                    <Modal.Body>
                        <div className="container">
                            <header className="row justify-content-center">
                                <h4>
                                    {
                                        (!newMembersList)
                                            ?
                                            <>Membros do Grupo</>
                                            :
                                            <>Adicione novos membros</>
                                    }

                                </h4>
                            </header>

                            <div className="row justify-content-between">
                                <input
                                    onChange={this.onSearchField}
                                    class="form-control col-sm-3"
                                    type="text"
                                    name=""
                                    placeholder="Procurar..."
                                />
                                {
                                    (!newMembersList)
                                        ?
                                        <button
                                            onClick={() => this.handleAddMembers(groupId)}
                                            data-toggle="tooltip"
                                            data-placement="top"
                                            title="Adicionar membro"
                                            className="btn"
                                            style={{backgroundColor: '#27AE60', color: 'white'}}
                                        >
                                            Novos &nbsp;
                                            <FaPlus/>
                                        </button>
                                        :
                                        <button
                                            onClick={() => this.handleStoreMembers(groupId)}
                                            data-toggle="tooltip"
                                            data-placement="top"
                                            title="Guardar os contactos marcados"
                                            className="btn btn-info"
                                            style={{color: 'white'}}
                                        >
                                            Guardar
                                            &nbsp;
                                            <i><FaSave/></i>
                                        </button>
                                }

                            </div>
                            {
                                (loading)
                                    ?
                                    (
                                        <div class="d-flex justify-content-center">
                                            <div class="spinner-border text-primary" role="status">
                                                <span class="sr-only">Loading...</span>
                                            </div>
                                        </div>
                                    )
                                    :
                                    (
                                        (filteredMembers.length !== 0)
                                            ?
                                            <div className=" row justify-content-start overflow-auto mt-4"
                                                 style={{height: '55vh'}}>
                                                <Table className='table-hover' style={{cursor: 'pointer'}} responsive>
                                                    <thead className="text-info">
                                                    <tr>
                                                        <th scope="col">#</th>
                                                        <th scope="col">Nome</th>
                                                        <th scope="col">Apelido</th>
                                                        <th scope="col">Email</th>
                                                        {
                                                            (!newMembersList)
                                                                ?
                                                                <th
                                                                    data-toggle="tooltip"
                                                                    data-placement="top"
                                                                    title="Clicar na Lixeira para remover"
                                                                    className="text-center"
                                                                >
                                                                    Remover
                                                                </th>
                                                                :
                                                                <th
                                                                    data-toggle="tooltip"
                                                                    data-placement="top"
                                                                    title="Clicar na Lixeira para remover"
                                                                    className="text-center"
                                                                >
                                                                    Adicionar
                                                                </th>

                                                        }

                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    {
                                                        filteredMembers.map(
                                                            (member, i) =>
                                                                <tr id={member.id}>
                                                                    <th scope="row">{i + 1}</th>
                                                                    <td>{member.name}</td>
                                                                    <td>{member.lastName}</td>
                                                                    <td>{member.email}</td>
                                                                    <td className='text-center'>

                                                                        {
                                                                            (newMembersList)
                                                                                ?

                                                                                (member.marked)
                                                                                    ?
                                                                                    <input
                                                                                        onChange={() => this.handleNewMember(member.id)}
                                                                                        className='check text-center'
                                                                                        id={member.id}
                                                                                        type='checkbox'
                                                                                        checked
                                                                                    />
                                                                                    :
                                                                                    <input
                                                                                        onChange={() => this.handleNewMember(member.id)}
                                                                                        className='check tex-center'
                                                                                        id={member.id}
                                                                                        type='checkbox'
                                                                                    />
                                                                                :
                                                                                <FaRegTrashAlt
                                                                                    data-toggle="tooltip"
                                                                                    data-placement="top"
                                                                                    title="Excluir membro"
                                                                                    onClick={() => this.handleRemoveMember(member.id)}
                                                                                    className='trashStyle'
                                                                                    style={{color: '#C0392B'}}
                                                                                />

                                                                        }
                                                                    </td>
                                                                </tr>
                                                        )
                                                    }
                                                    </tbody>
                                                </Table>
                                            </div>
                                            :
                                            <div className=" row justify-content-center overflow-auto mt-4"
                                                 style={{height: '60vh'}}>
                                                <h3 className='row justify-content-center text-secondary'>Não encontrou
                                                    nenhum membro novo</h3>
                                            </div>
                                    )

                            }
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <div className="container">
                            <div className="row justify-content-end">
                                <button
                                    onClick={this.handleBack}
                                    style={{backgroundColor: '#BDC3C7'}}
                                    className='btn'
                                >
                                    <TiArrowLeftThick/>
                                    voltar
                                </button>
                            </div>

                        </div>

                    </Modal.Footer>
                </Modal>
                {/*<EditGroupModal employers = {this.state.employers} descricao = {this.props.descricao} group = {groupId} setLgShow = {() => this.setLgShow()} shw = {this.state.show}/>*/}
            </>
        )
    }
}

export default withRouter(GroupMembersModal)
