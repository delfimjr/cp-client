import React from 'react';

const Scroll = (props) => {
  return (
    <div className = 'bg-white' 
      style={{ overflow: 'scroll', height: '400px', width: ''}}>
      {props.children}
    </div>
  );
};

export default Scroll;