import React, { Component } from 'react'
import { FiUsers, FiEdit } from "react-icons/fi"
import { IoMdContacts } from "react-icons/io";
import { Row, Col, CardBody, Card, CardTitle } from "reactstrap"
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import GroupMembersModal from './GroupMembersModal'
import CreateGroupModal from './CreateGroupModal'

import api from '../../Services/api'

import './styles.css'
import { Route } from 'react-router-dom';
import history from '../../history';
import {getHeaderWithToken} from "../../helpers/authHelper";

export default class GroupCreation extends Component {
    constructor() {
        super()
        this.state = {
            groups: [],
            searchfield: '',
            cardsNumber: 0,
            loading: true,
            showCreateGroupModal: false
        }
    }

    async componentWillMount() {
        this.props.handlePageTitleChange('Grupos Personalizados')
        let i = 0;
        await api.get('groups', {headers: getHeaderWithToken()}).then(response => {
            this.setState({ groups: response.data })
            this.setState({ loading: false })

            response.data.map(() => {
                i++
            })
        })
            .catch(() => this.setState({ loading: false }))
    }

    updateGroupState = async () => {
        let i = 0;
        await api.get('groups', {headers: getHeaderWithToken()}).then(response => {
            this.setState({ groups: response.data })
            this.setState({ loading: false })

            response.data.map(() => {
                i++
            })
        })
            .catch(() => this.setState({ loading: false }))
    }

    onSearchChange = (event) => {
        this.setState({ searchfield: event.target.value })
    }

    handleCreateGroupModal = flag => {
        this.setState({ showCreateGroupModal: flag })
    }

    render() {
        const { groups, searchfield, showCreateGroupModal } = this.state;

        const filteredGroups = groups.filter(group => {
            return group?.name?.toLowerCase().includes(searchfield?.toLowerCase());
        })
        return (
            <>
                <ToastContainer />
                <Row>
                    <Col md="12">
                        <Card className="shadow-sm p-3 cardHeight-inbox">
                            <CardTitle>
                                <span style={{ display: "flex", justifyContent: "space-between" }}>
                                    <div>
                                        <h4 className="title-register">
                                            Meus Grupos
                                            &nbsp;
                                        <FiUsers size={26} style={{ color: "#B2BABB " }} />
                                        </h4>
                                    </div>
                                    <div>
                                        <button
                                            onClick={() => this.handleCreateGroupModal(true)}
                                            className=''
                                            style={{ color: 'white' }}
                                            class="btn add"
                                        >
                                            Novo Grupo
                                            &nbsp;
                                            <FiEdit />
                                        </button>
                                    </div>

                                </span>
                            </CardTitle>
                            <CardBody>
                                {
                                    !this.state.groups.length === 0 
                                    &&
                                    <div className="row justify-content-center">
                                        <input
                                            className="form-control col-sm-4 searchInput-groups"
                                            type="text"
                                            name=""
                                            placeholder="Procurar..."
                                            onChange={this.onSearchChange}
                                        />
                                    </div>
                                }

                                {
                                    (this.state.loading)
                                        ? (
                                            <div class="d-flex justify-content-center">
                                                <div class="spinner-border text-primary" role="status">
                                                    <span class="sr-only">Loading...</span>
                                                </div>
                                            </div>
                                        )
                                        : (
                                            (filteredGroups.length !== 0)
                                                ? (
                                                    <>

                                                        <div className="row mt-2">
                                                            {
                                                                filteredGroups.map(group => {
                                                                    return (
                                                                        <div
                                                                            onClick={() => history.push(
                                                                                {
                                                                                    pathname: `/dashboard/configuracoes/grupos/${group.id}`,
                                                                                    state: { groupName: group.name }
                                                                                }

                                                                            )}
                                                                            className='text-center col-sm-2 rounded shadow mr-2 hover'
                                                                            style={{ backgroundColor: '#85C1E9', cursor: 'pointer', marginBottom: '1%' }}>
                                                                            < IoMdContacts />
                                                                            <div>
                                                                                <h4>{group.name?.length > 27 ? `${group.name.substr(0,25)}...` : group.name}</h4>
                                                                                <p>{group.description}</p>
                                                                            </div>
                                                                        </div>
                                                                    )
                                                                })
                                                            }

                                                        </div>
                                                    </>
                                                )

                                                : (
                                                    (this.state.groups.length === 0)
                                                        ?
                                                        <div className='container'>
                                                            <h3 className='row justify-content-center text-secondary' >Nenhum Grupo foi criado!</h3>
                                                            <h6 className='row justify-content-center'>
                                                                (Por favor, clica o  botão "Novo Grupo" para criar o primeiro &nbsp; <FiUsers size={18} />)
                                                            </h6>
                                                        </div>
                                                        :
                                                        <div className='container'>
                                                            <h3 className='row justify-content-center text-secondary' >Não encontrou nenhum grupo</h3>
                                                        </div>
                                                )
                                        )
                                }
                                <Route
                                    exact
                                    path="/dashboard/configuracoes/grupos/:id"
                                    render={() => {
                                        return (
                                            <GroupMembersModal updateGroupState={this.updateGroupState} />
                                        );
                                    }}
                                />
                                {
                                    (showCreateGroupModal)
                                        ?
                                        <CreateGroupModal
                                            showCreateGroupModal={this.state.showCreateGroupModal}
                                            handleCreateGroupModal={this.handleCreateGroupModal}
                                            updateGroupState={this.updateGroupState}
                                        />
                                        :
                                        null
                                }
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </>
        )
    }
}
