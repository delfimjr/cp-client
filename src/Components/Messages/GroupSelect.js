import React, { Component } from 'react'
import Select from 'react-select';
import api from '../../Services/api';
import {getHeaderWithToken} from "../../helpers/authHelper";

export default class GroupSelect extends Component {
    constructor() {
        super()
        this.state = {
            groups: [],
            selectedOption: null
        }
    }

    componentWillMount() {
        api.get('groups',{headers: getHeaderWithToken()})
            .then(response => {
                if (response.data) {
                    this.setState({ groups: response.data });
                }
            })
    }

    //This Method prints the Selectd Options on Select
    handleChange = selectedOption => {
        let {changeGroup} = this.props
        this.setState(
            { selectedOption },
            () => changeGroup(this.state.selectedOption)
        )
    }

    render() {
        let options = this.state.groups.map((opt,i) => ({ label: opt.name, value: opt.id }));
        const { selectedOption } = this.state

        return (
            <div className="app">
                <div className="container">
                    <Select
                        isMulti
                        value = { selectedOption }
                        options = {options}
                        onChange = {this.handleChange}
                    />
                </div>
            </div>
        )
    }
}
