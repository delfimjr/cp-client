import React, { Component } from 'react'

import history from '../../history'
import Modal from 'react-bootstrap/Modal'
import { AiOutlineAlert } from 'react-icons/ai'
import { TiChevronRightOutline, TiChevronLeftOutline, TiTickOutline } from 'react-icons/ti'
import { FcIdea } from 'react-icons/fc'

import api from '../../Services/api'

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { MdInfoOutline } from 'react-icons/md'

//Constants
const NOME_DO_ALERTA = 'Nome do Alerta'
const TURN_INTO_DEFAULT = 'default'
const JUST_ONCE = 'once'

export default class AlertModal extends Component {

    constructor() {
        super()

        this.state = {
            newAlert: false,
            alert_messages: [],
            name: '',
            text: '',
            showModal: false,
            warning: '',
            noNameWarning: '',
            notextWarning: '',
            isSaving: false
        }
    }

    UNSAFE_componentWillMount() {
        this.setState({ showModal: true })

        api.get('alertMessages')
            .then(response => {
                this.setState({ alert_messages: response.data })
            })
            .catch(() => alert('Nao foi possivel carregar os alertas!'))
    }

    onSelectChange = event => {
        const { alert_messages, name } = this.state
        const { value } = event.target

        let currentText = []

        if (this.state.warning !== '') {
            this.setState({ warning: '' })
        }
        if (value !== NOME_DO_ALERTA && value !== 'Sem Alertas') {
            if (value.includes('(padrão)')) {
                this.setState({ name: value.replace('(padrão)', " ").trim() })

                currentText = alert_messages.filter(alert => {
                    return alert.name == value.replace('(padrão)', " ").trim()
                })
            }
            else {
                this.setState({ name: value })

                currentText = alert_messages.filter(alert => {
                    return alert.name == value
                })
            }
            this.setState({ text: currentText[0].body })
        }
        else {
            this.setState({ name: value })
            this.setState({ text: '' })
        }

    }

    handleNameChange = (event) => {
        const { value } = event.target
        if (this.state.noNameWarning !== '') {
            this.setState({ noNameWarning: '' })
        }

        this.setState({ name: value })
    }

    handleTextChange = (event) => {
        const { value } = event.target
        if (this.state.notextWarning !== '') {
            this.setState({ notextWarning: '' })
        }
        this.setState({ text: value })
    }

    handleNewAlert = () => {
        this.setState({ newAlert: !this.state.newAlert })
        this.setState({ name: '' })
        this.setState({ text: '' })
    }

    handleCloseModal = () => {
        this.setState({ show: false })
    }

    handleStore = (defaultFlag) => {
        const { newAlert, name, text, alert_messages } = this.state
        const { updateAlertMessage } = this.props

        if (name === '' || text === '') {
            toast.warn('Defina um nome amigável e o respectivo texto do alerta')

            return;
        }

        if (this.state.newAlert) {
            api.post('alertMessages', {
                name,
                body: text,
                defaultMessage: defaultFlag
            }).then(response => {
                if(response.data.default){
                    toast.success(`Alerta ${response.data.name.toUpperCase()} guardado e padronizado`)
                }
                else{
                    toast.success(`Alerta ${response.data.name.toUpperCase()} guardado`)
                }
                this.handleModal()
                this.props.updateAlertMessage(response.data.body)
            })
                .catch((err) => {
                    console.log(err)
                    toast.error('❌ Não foi possível guardar o alerta.')
                })
        }
        else {
            if (defaultFlag) {
                const idOfChosenAlert = this.state.alert_messages.filter(alert => alert.name === this.state.name)

                if (idOfChosenAlert[0].default) {
                    toast.info("O alerta escolhido ja e' o padrão.")
                }
                else {
                    api.put(`alertMessages/${idOfChosenAlert[0].id}`, {
                        name,
                        body: text,
                        defaultMessage: defaultFlag
                    }).then(response => {
                        toast.success(`✅ Alerta ${response.data.name.toUpperCase()} guardado ${response.data.default && "e padronizado"}`)
                        this.handleModal();
                        this.props.updateAlertMessage(response.data.body)
                    })
                        .catch((err) => {
                            console.log(err)
                            toast.error('❌ Não foi possível guardar o alerta.')
                        })
                }

                return
            }
            this.handleModal()
            this.props.updateAlertMessage(this.state.text)
        }
    }

    handleModal = () => {
        history.push("/dashboard/enviar-mensagem")
    }

    render() {
        const { name, text, alert_messages, warning, noNameWarning, notextWarning } = this.state
        return (
            <>
                <Modal
                    size="md"
                    show={this.state.showModal}
                    class="modal fade modal-dialog"
                    onHide={this.handleModal}
                    centered
                >
                    <Modal.Header style={{ backgroundColor: '#F8F9F9' }} closeButton>
                        <div className="container">
                            <div className="row justify-content-center">
                                <h5 className=''>
                                    As minhas mensagens de alerta
                                <AiOutlineAlert size='2em' style={{ color: '#F4D03F' }} />
                                </h5>
                            </div>
                        </div>
                    </Modal.Header>

                    <Modal.Body>

                        <div className="container">
                            <div className="row">
                                <div className="col-sm-4">
                                    {
                                        (this.state.newAlert === false)
                                            ?
                                            <button onClick={this.handleNewAlert} className="btn addAlert">
                                                Criar novo<TiChevronRightOutline />
                                            </button>
                                            :
                                            <button onClick={this.handleNewAlert} className="btn addAlert">
                                                Voltar<TiChevronLeftOutline />
                                            </button>
                                    }
                                </div>
                                <div className="col-sm-8">
                                    {
                                        (this.state.newAlert)
                                            ?
                                            <input
                                                onChange={this.handleNameChange}
                                                className={`form-control ${noNameWarning}`}
                                                type="text"
                                                placeholder='Escolha um nome amigável'
                                            />
                                            :
                                            null
                                    }

                                </div>
                            </div>
                            {
                                (!this.state.newAlert)
                                    ?
                                    <div className="row mt-2">
                                        <h6 className='col text-info' style={{ fontWeight: 'normal', fontSize: '10pt' }}>
                                            abaixo, seleccione o alerta desejado
                                        </h6>
                                    </div>
                                    :
                                    null
                            }

                            {
                                (!this.state.newAlert)
                                    ?
                                    <div className="row">
                                        <div className="col-sm-6">
                                            <select onChange={this.onSelectChange} className={`form-control ${warning}`} name="names" id="">
                                                <option>{NOME_DO_ALERTA}</option>
                                                {
                                                    (alert_messages.length > 0)
                                                        ?
                                                        (
                                                            alert_messages.map(alert => {
                                                                return alert.default ? <option id='default'>{alert.name} &nbsp; (padrão)</option> : <option>{alert.name}</option>
                                                            })
                                                        )
                                                        :
                                                        <option>Sem Alertas</option>

                                                }
                                            </select>
                                        </div>
                                        {
                                            warning !== ''
                                                ?
                                                <span className='text-info' style={{ fontWeight: 'normal' }}>
                                                    <MdInfoOutline />&nbsp;
                                                Seleccione o alerta
                                            </span>
                                                :
                                                null
                                        }

                                    </div>
                                    :
                                    null
                            }
                            <div className="row mt-2">
                                <div className="col">
                                    {
                                        (!this.state.newAlert && (name === NOME_DO_ALERTA || name === ''))
                                            ?
                                            <textarea
                                                className='form-control'
                                                id="inputMessage"
                                                rows="5"
                                                disabled
                                                value={text}
                                            />
                                            :
                                            (
                                                (!this.state.newAlert && (name !== NOME_DO_ALERTA && name !== ''))
                                                    ?
                                                    <textarea
                                                        className='form-control'
                                                        rows="5"
                                                        value={text}
                                                        placeholder={"Escreva a mensagem de alerta"}
                                                    />
                                                    :
                                                    <textarea
                                                        className={`form-control ${notextWarning}`}
                                                        rows="5"
                                                        onChange={this.handleTextChange}
                                                        placeholder={"Escreva a mensagem de alerta"}
                                                    />
                                            )
                                    }
                                </div>


                            </div>

                            {
                                (this.state.newAlert)
                                    ?
                                    <div className="justify-content-start mt-2">
                                        <span className='text-info ' style={{ fontWeight: 'normal', fontSize: '10pt' }}>
                                            <FcIdea />&nbsp;
                                        sugestão: assinatura no fim do alerta (ex: Port Communications)
                                    </span>
                                    </div>
                                    :
                                    null
                            }
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <div className="container">
                            <div className="row justify-content-end">
                                <button
                                    onClick={() => this.handleStore(false)}
                                    className="btn btn-secondary col-4 mr-2"
                                >
                                    Guardar
                                </button>
                                <button
                                    onClick={() => this.handleStore(true)}
                                    className="btn btn-primary col-4"
                                >
                                    Padronizar
                                </button>
                            </div>
                        </div>
                    </Modal.Footer>
                </Modal>
            </>
        )

    }
}

