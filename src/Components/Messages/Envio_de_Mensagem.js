import React, {Component} from "react";
import {Route} from 'react-router-dom'

import history from '../../history'
import 'bootstrap/dist/css/bootstrap.min.css';


import Mensagem from './Mensagem';
import Destinatario from './Destinatario';
import EscolherMeio from './Meio_de_Envio';
import AlertModal from './AlertModal'
import ConfirmationModal from '../ConfirmationModal/index'
import Loading from '../Loading/index'

import {AiOutlineAlert} from 'react-icons/ai'

import './alertButtonStyle.css'

import api from "../../Services/api";

import {ToastContainer, toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {getHeaderWithToken} from "../../helpers/authHelper";
import dateOrganizeHelper from "../../helpers/dateOrganizeHelper";

//Constants
const ALL = '*'
const EMAIL = 'email'
const EMAIL_SMS = 'email&sms'
const EMAIL_WHATSAPP = 'email&whatsapp'
const SMS_WHATSAPP = 'sms&whatsapp'
const SMS = 'sms'
const WHATSAPP = 'whatsapp'


export default class Envio_de_Mensagem extends Component {
    constructor(props) {
        super(props)

        this.state = {
            subject: '',
            body: '',
            selectedFile: null,
            selectedIndividualOption: [],
            selectedGroupOption: [],
            contacts: '',
            showModal: false,
            alertText: '',
            //CONFIRMATION MODAL PROPERTIES
            confirmationModalTitle: '',
            confirmationModalText: '',
            showConfirmationModal: false,
            successButton: '',
            cancelButton: '',
            searializedData: {},
            loading: true,
            emptynessSubjectBorder: false,
            emptynessBodyBorder: false,
            isSaving: false,
            formInput: {
                subject: '',
                body: ''
            },
            cancel: false,
            auxDefaultAlert: '',
            isToSchedule: false,
        }
    }

    async UNSAFE_componentWillMount() {
        this.props.handlePageTitleChange('Envio de Mensagem')
        if (sessionStorage.getItem('message')) {
            this.setState({
                formInput: {
                    ...this.state.formInput,
                    subject: JSON.parse(sessionStorage.getItem('message')).subject,
                    body: JSON.parse(sessionStorage.getItem('message')).body,
                }
            })
        }

        await api.get('contacts', {headers: getHeaderWithToken()}).then(response => {
            this.setState({loading: false})
            this.setState({contacts: response.data})
        })
            .catch(() => {
                this.setState({loading: false})
            })

        api.get('alertMessages', {headers: getHeaderWithToken()})
            .then(async response => {
                this.setState({alert_messages: response.data})

                const defaultAlert = response.data.filter(alert => alert.default)
                if (defaultAlert?.length > 0) {
                    this.setState({auxDefaultAlert: defaultAlert})
                    this.setState({alertText: defaultAlert[0].body})
                }

            })
            .catch(() => alert('Nao foi possivel carregar os alertas!'))
    }

    //Definir session storage para assunto e texto...
    componentWillUnmount() {
        if (!this.state.formInput.body && !this.state.formInput.subject) {
            sessionStorage.setItem("message", '')
        } else {
            sessionStorage.setItem("message", JSON.stringify({
                subject: this.state.formInput.subject,
                body: this.state.formInput.body
            }))
        }
    }

    handleOnChangeInput = (event, isBody) => {
        const {formInput} = this.state;

        /* if (isBody) {
             // the used editor for the body has an input containing paragraph tag
             //so, we must remove it because we also want to send sms
             const value = event
                 .replace('<p>', '')
                 .replace('</p>', '');

             this.setState({formInput: {...formInput, body: value}})
         } else {

         }*/
        const {name, value} = event.target
        this.setState({formInput: {...formInput, [name]: value}})
        this.handleSubjectInputColor(false)
        this.handleBodyInputColor(false)
    }

    onChangeHandler = event => {
        if (this.checkMimeType(event)) {
            this.setState({selectedFile: event.target.files})
        }
    }

    handleCancel = () => {
        this.setState({
            formInput: {
                ...this.state.formInput,
                subject: '',
                body: '',
            }
        })
        this.setState({loading: true})
        setTimeout(() => {
            history.push('/dashboard/mensagens-enviadas')
        }, 1000)

    }

    checkMimeType = (event) => {
        //getting file object
        let files = event.target.files
        //define message container
        let err = ''
        let format;
        // list allow mime type
        const types = [
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'application/msword',
            'text/plain',
            'application/pdf',
            'application/vnd.ms-excel',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'image/jpeg',
            'image/png',
            'image/pjpeg',
            'image/jpeg',
            'image/gif',
            'image/svg',
            'application/vnd.openxmlformats-officedocument.presentationml.presentation',
        ];
        // loop access array
        for (var x = 0; x < files.length; x++) {
            if (types.every(type => files[x].type !== type)) {
                format = files[x].type.split('/');
                format = format.pop();
                err += 'Ficheiros do formato ' + format + ' não são permitidos!\n Se se tratar de um documento, por favor, converta para o formato PDF.';
            }
        }

        if (err !== '') {
            event.target.value = null
            toast.error(err)
            return false;
        }

        return true;
    }

    validateShippingMethod = (email, sms, whatsapp) => {
        let shipping_method = ''

        if (email) {
            if (sms) {
                if (whatsapp) {
                    shipping_method = ALL
                } else {
                    shipping_method = EMAIL_SMS
                }
            } else {
                if (whatsapp) {
                    shipping_method = EMAIL_WHATSAPP
                } else {
                    shipping_method = EMAIL
                }
            }
        } else {
            if (sms) {
                if (whatsapp) {
                    shipping_method = SMS_WHATSAPP
                } else {
                    shipping_method = SMS
                }
            } else {
                if (whatsapp) {
                    shipping_method = WHATSAPP
                }
            }
        }

        return shipping_method

    }

    //For while we just go to sent messages page after sending a message...
    onSubmitMessage = (scheduledTo) => {
        const {userObject} = this.props
        const {selectedIndividualOption, selectedGroupOption, formInput} = this.state

        const {subject, body} = formInput;

        const inputCheckbox = document.getElementsByClassName('shipping_method');

        let singleDestination = selectedIndividualOption.map(option => option.value)

        let groups = selectedGroupOption.map(option => option.value)

        const email = inputCheckbox[0].checked
        const sms = inputCheckbox[1].checked
        const whatsapp = false
        this.setState({isToSchedule: scheduledTo !== null})


        if (subject && body && (singleDestination.length > 0 || groups.length > 0) && (email || sms)) {
            const shipping_method = this.validateShippingMethod(email, sms, whatsapp)
            let data = {
                communicationId: userObject.user.id,
                contactsIds: singleDestination,
                groupsIds: groups,
                body: body,
                idComunicacoes: userObject.id,
                subject: subject,
                sms: shipping_method.includes('sms'),
                mail: shipping_method.includes('mail'),
                alertText: '',
                scheduledTo
            }

            if (shipping_method.includes('sms') || shipping_method === '*') {
                this.setState({isSaving: true})

                const dataToSave = new FormData();
                if (this.state.selectedFile != null) {
                    for (let index = 0; index < this.state.selectedFile.length; index++) {
                        dataToSave.append('file', this.state.selectedFile[index])
                    }
                }

                dataToSave.append('content', JSON.stringify(data))

                api.post(scheduledTo === null ? 'messages' : 'messages/schedule', dataToSave, {headers: getHeaderWithToken()}).then(() => {

                    this.setState({isSaving: false})
                    toast.success(
                        scheduledTo !== null ?
                            `A mensagem foi agendada para ${dateOrganizeHelper(data.scheduledTo.toISOString())}` :
                            'A Mensagem foi enviada!');

                    setTimeout(() => {
                        this.handleCancel()
                    }, 2000)

                })
                    .catch((err) => {
                        console.log(err)
                        this.setState({isSaving: false})
                        toast.error('❌ Não foi possível enviar a mensagem. Tente novamente ou verifique a conexão')
                    })

            } else {

                this.setState({showConfirmationModal: true})
                this.setState({confirmationModalTitle: 'Alerta SMS'})
                this.setState({confirmationModalText: 'Deseja enviar uma mensagem de alerta SMS?'})
                this.setState({cancelButton: 'Não'})
                this.setState({successButton: 'Sim'})

                data = {
                    communicationId: userObject.user.id,
                    contactsIds: singleDestination,
                    groupsIds: groups,
                    body: body,
                    idComunicacoes: userObject.id,
                    subject: subject,
                    sms: shipping_method.includes('sms'),
                    mail: shipping_method.includes('mail'),
                    alertText: '',
                    scheduledTo
                }

                this.setState({searializedData: data})
            }

        } else {
            if (subject === '' || subject === undefined) {
                this.handleSubjectInputColor(true)
            }

            if (body === '' || body === undefined) {
                this.handleBodyInputColor(true)
            } else {
                if (singleDestination.length <= 0) {
                    toast.warn('Por favor, indique o destinatário')
                } else {
                    toast.warn('Por favor, seleccione o método de envio')
                }
            }
        }
    }

    handleSubjectInputColor = (flag) => {
        this.setState({emptynessSubjectBorder: flag})
    }

    handleBodyInputColor = (flag) => {
        this.setState({emptynessBodyBorder: flag})
    }

    //This function just bring all selected ID and destin names from DestinSelect class
    changeDestin = destin => {
        console.log(destin)
        this.setState({selectedIndividualOption: destin})
    }

    //This function just bring all selected Groups from GroupSelect class
    changeGroup = destinationGroup => {
        console.log(destinationGroup)
        this.setState({selectedGroupOption: destinationGroup})

    }

    handleModal = () => {
        history.push("/dashboard/enviar-mensagem/alert-message")
    }

    updateAlertText = (text) => {
        this.setState({alertText: text})
    }

    handleConfirmationModal = () => {
        this.setState({showConfirmationModal: !this.state.showConfirmationModal})
    }

    handleModalResponse = async flag => {
        let index;
        const {searializedData, alertText, isToSchedule} = this.state

        if (flag === 1) {
            if (alertText !== '') {
                const data = {...searializedData, alertText: alertText}

                this.setState({isSaving: true})
                this.setState({showConfirmationModal: false})

                const dataToSave = new FormData();
                if (this.state.selectedFile != null) {
                    for (index = 0; index < this.state.selectedFile.length; index++) {
                        dataToSave.append('file', this.state.selectedFile[index])
                    }
                }

                dataToSave.append('content', JSON.stringify(data))

                await api.post(!isToSchedule ? 'messages' : 'messages/schedule', dataToSave, {headers: getHeaderWithToken()}).then(() => {
                    //alert('Mensagem Enviada')
                    toast.success(
                        isToSchedule ?
                            `A mensagem foi agendada para ${dateOrganizeHelper(data.scheduledTo.toISOString())}` :
                            'A Mensagem foi enviada!');
                    this.setState({isSaving: false})

                    setTimeout(() => {
                        this.handleCancel()
                    }, 2000)

                })
                    .catch(() => {
                        this.setState({isSaving: false})
                        toast.error(`❌ Não foi possível ${isToSchedule ? 'agendar' : 'enviar'} a mensagem. Tente novamente ou verifique a conexão`)
                    })

            } else {
                this.setState({showConfirmationModal: false})
                toast.warn('Nenhuma mensagem de alerta foi definida! Por, favor, defina uma mensagem.')
            }
        } else {
            const data = {...searializedData, alertText: ''}

            this.setState({isSaving: true})
            this.setState({showConfirmationModal: false})

            const dataToSave = new FormData();
            if (this.state.selectedFile != null) {
                for (index = 0; index < this.state.selectedFile.length; index++) {
                    dataToSave.append('file', this.state.selectedFile[index])
                }
            }

            dataToSave.append('content', JSON.stringify(data))

            await api.post(!isToSchedule ? 'messages' : 'messages/schedule', dataToSave, {headers: getHeaderWithToken()}).then(() => {

                this.setState({isSaving: false})
                toast.success(
                    isToSchedule ?
                        `A mensagem foi agendada para ${dateOrganizeHelper(data.scheduledTo.toISOString())}` :
                        'A Mensagem foi enviada!');

                setTimeout(() => {
                    this.handleCancel()
                }, 2000)

            })
                .catch((err) => {
                    console.log(err)
                    this.setState({isSaving: false})
                    toast.error(`❌ Não foi possível ${isToSchedule ? 'agendar' : 'enviar'} a mensagem. Tente novamente ou verifique a conexão`)
                })
        }
    }

    render(props) {
        //CSS PARA LINHA VERTICAL
        var verticalLine = {
            borderLeft: "1px solid black",
            position: "relative",
            width: 1,
            height: "100%"
        }
        const {
            showConfirmationModal,
            confirmationModalText,
            confirmationModalTitle,
            cancelButton,
            successButton,
            loading
        } = this.state

        if (loading) {
            return (
                <>
                    <div className="d-flex justify-content-center">
                        <div className="spinner-border text-primary" role="status">
                            <span className="sr-only">Loading...</span>
                        </div>
                    </div>
                </>
            )
        } else
            return (
                <>
                    <ToastContainer/>
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-sm-8">
                                <Mensagem
                                    handleSubjectInputColor={this.handleSubjectInputColor}
                                    handleBodyInputColor={this.handleBodyInputColor}
                                    emptynessSubjectBorder={this.state.emptynessSubjectBorder}
                                    emptynessBodyBorder={this.state.emptynessBodyBorder}
                                    onSubmitMessage={this.onSubmitMessage}
                                    handleOnChangeInput={this.handleOnChangeInput}
                                    onChangeHandler={this.onChangeHandler}
                                    formInput={this.state.formInput}
                                    handleCancel={this.handleCancel}
                                />
                            </div>
                            <div>
                                <div style={verticalLine}/>
                            </div>
                            <div className="col-sm">
                                <div className="container">
                                    {
                                        this.state.contacts?.length && !this.state.loading
                                            ?
                                            <Destinatario
                                                contacts={this.state.contacts}
                                                changeDestin={this.changeDestin}
                                                changeGroup={this.changeGroup}
                                            />
                                            :
                                            <p style={{color: 'red'}}>Ainda nao possui contactos na plataforma</p>
                                    }

                                    <EscolherMeio/>
                                    <div className="ml-2 mt-2">
                                        <button
                                            onClick={this.handleModal}
                                            data-toggle="tooltip{"
                                            data-placement="right"
                                            title="Clicar para modificar a mensagem de alerta"
                                            className='btn alert'
                                        >
                                            Personalizar alerta
                                            <AiOutlineAlert size='2em' className='alert_icon'/>
                                        </button>
                                        {
                                            this.state.alertText
                                            &&
                                            <div style={{display: 'flex', flexDirection: 'column', fontSize: '2vh'}}>
                                                <p style={{fontWeight: 'bold', marginRight: '1vh'}}>Alerta Definido:</p>
                                                <p className='text-info'>
                                                    {
                                                        this.state.auxDefaultAlert?.body?.replace(/\s/g, '') === this.state.alertText?.replace(/\s/g, '')
                                                            ? `${this.state.alertText}(padrão)`
                                                            : this.state.alertText
                                                    }
                                                </p>
                                            </div>
                                        }
                                    </div>

                                </div>
                            </div>
                        </div>
                        <Route
                            path="/dashboard/enviar-mensagem/alert-message"
                            render={({}) => {
                                return (
                                    <AlertModal updateAlertMessage={this.updateAlertText}/>
                                );
                            }}
                        />


                        <ConfirmationModal
                            showModal={showConfirmationModal}
                            handleConfirmationModal={this.handleConfirmationModal}
                            title={confirmationModalTitle}
                            text={confirmationModalText}
                            cancelButton={cancelButton}
                            successButton={successButton}
                            handleModalResponse={this.handleModalResponse}
                        />
                        {
                            (this.state.isSaving)
                                ?
                                <Loading/>
                                :
                                null
                        }

                    </div>
                </>
            );
    }
}
