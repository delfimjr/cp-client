import React, {Component, useState, useEffect, useRef} from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import {FaUpload} from 'react-icons/fa';
import {DateTimePickerComponent} from '@syncfusion/ej2-react-calendars';
import SunEditor from "suneditor-react";
import 'suneditor/dist/css/suneditor.min.css';

export default class Mensagem extends Component {
    constructor(props) {
        super(props);
        this.editor = React.createRef();
        this.state = {
            scheduledDate: new Date(Date.now()),
            dateInputShown: false,
        }
    }

    handleDateInputVisibility = () => {
        this.setState({dateInputShown: !this.state.dateInputShown})
    }

    scheduledDateHandler(date) {
        this.setState({scheduledDate: date.value});
    }

    getSunEditorInstance = (sunEditor) => {
        this.editor.current = sunEditor;
    };

    render(props) {

        const {emptynessSubjectBorder, emptynessBodyBorder} = this.props

        return (

            <div>
                <form>
                    <div class="form-group col-sm-12">
                        <label for="inputSubject">Assunto</label>
                        <input
                            type="text"
                            className={emptynessSubjectBorder ? 'form-control border border-danger' : 'form-control'}
                            name='subject'
                            id="inputSubject"
                            value={this.props.formInput.subject}
                            onChange={this.props.handleOnChangeInput}
                            required
                        />

                        <br/>

                        <label for="inputMessage">Sua mensagem</label>
                        <textarea
                            className={emptynessBodyBorder ? 'form-control border border-danger' : 'form-control'}
                            name='body'
                            id="inputMessage"
                            placeholder="Escreva aqui a sua mensagem"
                            rows="15"
                            value={this.props.formInput.body}
                            onChange={this.props.handleOnChangeInput}
                            required
                        />
                        {/*<SunEditor
                            className={emptynessBodyBorder ? 'form-control border border-danger' : 'form-control'}
                            enableToolbar={true}
                            name="body"
                            value={this.props.formInput.body}
                            onChange={value => this.props.handleOnChangeInput(value, true)}
                            setOptions={{
                                buttonList: [
                                    ['undo', 'redo'],
                                    ['font', 'fontSize', 'formatBlock'],
                                    ['bold', 'underline', 'italic', 'strike', 'subscript', 'superscript'],
                                    ['fontColor', 'hiliteColor', 'textStyle'],
                                    ['align', 'horizontalRule', 'list', 'lineHeight'],
                                    ['fullScreen'],
                                ],
                                height: 400,
                            }}/>*/}
                    </div>


                    <div className='control-pane' ref={this.dropContainerRef}>
                        <div className='control-section uploadpreview'>
                            <div className='col-lg-12'>
                                <label>Seleccione ficheiros</label>
                                <div style={{border: "solid 1px #d3d3d3", padding: "10px"}}>
                                    <form action="\CriarPublicacao" method="POST">
                                        <label for="fileInput"><FaUpload size={25} className="mr-3"
                                                                         style={{color: "#3b6186"}}/></label>
                                        <input multiple onChange={this.props.onChangeHandler} type="file" id="fileInput"
                                               name="file"/>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="container form-group col-sm-12 mt-3">
                        <div class="row">
                            <div class="col-sm-12" style={{textAlign: 'right'}}>
                                {
                                    !this.state.dateInputShown
                                    &&
                                    <button
                                        type="button"
                                        className="btn btn-danger mr-3"
                                        onClick={this.props.handleCancel}
                                    >
                                        Cancelar
                                    </button>
                                }
                                {
                                    this.state.dateInputShown
                                    &&
                                    <DateTimePickerComponent
                                        placeholder='escolha a data a agendar'
                                        value={this.state.scheduledDate}
                                        onChange={value => this.scheduledDateHandler(value)}
                                    >
                                    </DateTimePickerComponent>
                                }
                                {
                                    this.state.dateInputShown
                                    &&
                                    <button
                                        type="button"
                                        class="btn btn-danger mr-3"
                                        onClick={() => this.handleDateInputVisibility()}
                                    >
                                        Cancelar
                                    </button>
                                }
                                {
                                    !this.state.dateInputShown
                                    &&
                                    <button
                                        type="button"
                                        class="btn btn-info mr-3"
                                        onClick={() => this.handleDateInputVisibility()}
                                    >
                                        Agendar
                                    </button>
                                }
                                {
                                    this.state.dateInputShown
                                    &&
                                    <button
                                        type="button"
                                        class="btn btn-success mr-3"
                                        onClick={() => this.props.onSubmitMessage(this.state.scheduledDate)}
                                    >
                                        Okay
                                    </button>
                                }
                                {
                                    !this.state.dateInputShown
                                    &&
                                    <button
                                        type="button"
                                        class="btn btn-success"
                                        onClick={() => this.props.onSubmitMessage(null)}
                                    >
                                        Enviar
                                    </button>
                                }
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}
