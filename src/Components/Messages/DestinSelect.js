import React, { Component } from 'react'
import Select from 'react-select';

export default class GroupSelect extends Component {
    constructor() {
        super()
        this.state = {
            contacts: [],
            selectedOption: null
        }
    }

    componentWillMount() {
        this.setState({contacts: this.props.contacts})
    }

    //This Method prints the Selectd Options on Select
    handleChange = selectedOption => {
        let { changeDestin } = this.props
        this.setState(
            { selectedOption },
            () => changeDestin(this.state.selectedOption)
        )

    }

    render() {
        const  { contacts } = this.state
        let options = contacts.map(opt => ({ label: opt.name +' '+ opt.lastName, value: opt.id }));
        
        const { selectedOption } = this.state

        return (
            <div className="app">
                <div className="container">
                    <Select
                        isMulti
                        value = { selectedOption }
                        options = {options}
                        onChange = {this.handleChange}
                    />
                </div>
            </div>
        )
    }
}
