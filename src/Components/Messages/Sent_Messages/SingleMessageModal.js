import React, { useState} from 'react'
import Modal from 'react-bootstrap/Modal'
import {API_URL} from "../../../helpers/api";
import {
    FaRegFileAlt,
    FaRegFileArchive,
    FaRegFileExcel,
    FaRegFileImage,
    FaRegFilePdf,
    FaRegFilePowerpoint,
    FaRegFileWord
} from "react-icons/fa";
import Radium from "radium";
import {pulse} from "react-animations";
import {FiShare} from "react-icons/fi";
import QuestionModal from "../../ShareQuestionModal";

const styles = {
    pulse: {
        animation: 'x 0.7s',
        animationName: Radium.keyframes(pulse, 'pulse')
    },
}

const SingleMessageModal = ({message, close}) => {
    const [showQuestionModal, setShowQuestionModal] = useState(false)


    function handleShowQuestion() {
        setShowQuestionModal(!showQuestionModal)
    }

    return (
        <Modal
            centered
            size="xl"
            show={message !== ''}
            onHide={close}
        >
            <Modal.Header style={{backgroundColor: '#F8F9F9'}}>
                <div className="container">
                    <div className="row justify-content-between">
                        <h4 className=''>{message.subject}</h4>
                        <div style={{display: "flex", flexDirection: "row", alignItems: "center"}}>
                            <span style={{color: '#3498DB'}}>Partilhar</span>
                            <FiShare
                                onClick={handleShowQuestion}
                                className="share"
                                size={20}
                                c
                                style={{cursor: "pointer"}}
                            />
                        </div>
                    </div>
                    <div className="row">
                        <p>Por: {message.sender}</p>
                    </div>
                </div>
            </Modal.Header>

            <Modal.Body>
                <div style={{display: "flex", flexDirection: "row", alignItems: "center"}}>
                    <p style={{fontSize: "small"}}>Para: </p>
                    <p
                        style={{display: "flex", flexDirection: "row", cursor: "pointer"}}
                        data-toggle="tooltip"
                        data-placement="bottom"
                        title={message?.contactsFullNames?.map(contact => contact.fullName)}
                    >
                        {
                            message?.contactsFullNames?.map((contact, i) => (
                                i < 5 &&
                                <i style={{fontSize: "small"}}>{`${message?.contactsFullNames?.length - 1 > i ? contact.fullName + "," : contact.fullName}`}</i>
                            ))
                        }
                        {
                            message?.contactsFullNames?.length > 5
                            &&
                            <i style={{fontSize: "small"}}>etc</i>
                        }
                    </p>
                </div>
                <div>
                    {
                        message.attachments?.length > 0
                        &&
                        <div>
                            <p style={{fontWeight: 420}}>Anexos</p>
                            <div style={{marginLeft: '1rem'}}>
                                {
                                    message.attachments.map(att => {
                                        if (att.indexOf('.xlsx') !== -1) {
                                            return (
                                                <a className='row decoration' href={`${API_URL}/files/${att}`}
                                                   target="">
                                                    <button className="att-button">
                                                        <FaRegFileExcel
                                                            style={{color: '#2ECC71'}}/> {att.slice(14, att.length)}
                                                    </button>

                                                </a>
                                            )
                                        } else {
                                            if (att.indexOf('.pptx') !== -1) {
                                                return (
                                                    <a className='row decoration' href={`${API_URL}/files/${att}`}
                                                       target="">
                                                        <button className="att-button">
                                                            <FaRegFilePowerpoint
                                                                style={{color: '#D35400'}}/> {att.slice(14, att.length)}
                                                        </button>
                                                    </a>
                                                )
                                            } else {
                                                if (att.indexOf('.doc') !== -1) {
                                                    return (
                                                        <a className='row decoration' href={`${API_URL}/files/${att}`}
                                                           target="">
                                                            <button className="att-button">
                                                                <FaRegFileWord
                                                                    style={{color: '#3498DB'}}/> {att.slice(14, att.length)}
                                                            </button>
                                                        </a>
                                                    )
                                                } else {
                                                    if (att.indexOf('.pdf') !== -1) {
                                                        return (
                                                            <a className='row decoration'
                                                               href={`${API_URL}/files/${att}`} target="">
                                                                <button className="att-button">
                                                                    <FaRegFilePdf
                                                                        style={{color: '#C0392B'}}/> {att.slice(14, att.length)}
                                                                </button>
                                                            </a>
                                                        )
                                                    } else {
                                                        if (att.indexOf('.png') !== -1 || att.indexOf('.jpeg') !== -1 || att.indexOf('.pjpeg') !== -1) {
                                                            return (
                                                                <a className='row decoration'
                                                                   href={`${API_URL}/files/${att}`} target="">
                                                                    <button className="att-button">
                                                                        <FaRegFileImage
                                                                            style={{color: '#F9E79F'}}/> {att.slice(14, att.length)}
                                                                    </button>
                                                                </a>
                                                            )
                                                        } else {
                                                            if (att.indexOf('.rar') !== -1 || att.indexOf('.zip') !== -1) {
                                                                return (
                                                                    <a className='row decoration'
                                                                       href={`${API_URL}/files/${att}`} target="">
                                                                        <button className="att-button">
                                                                            <FaRegFileArchive
                                                                                style={{color: '#78281F'}}/> {att.slice(14, att.length)}
                                                                        </button>
                                                                    </a>
                                                                )
                                                            } else {
                                                                return (
                                                                    <a className='row decoration'
                                                                       href={`${API_URL}/files/${att}`} target="">
                                                                        <button className="att-button">
                                                                            <FaRegFileAlt
                                                                                style={{color: '#AEB6BF'}}/> {att.slice(14, att.length)}
                                                                        </button>
                                                                    </a>
                                                                )

                                                            }
                                                        }

                                                    }
                                                }
                                            }
                                        }
                                    })
                                }
                            </div>
                        </div>
                    }
                </div>
                <div style={{marginTop: '4%'}}>
                    <pre style={{...styles.pulse, background: "#EBF5FB"}} className='message-body'>
                        {message.body}
                    </pre>
                </div>
            </Modal.Body>
            {
                <QuestionModal
                    messageContent={message}
                    setShowModal={handleShowQuestion}
                    showQuestion={showQuestionModal}
                />
            }
        </Modal>
    )
}

export default SingleMessageModal;
