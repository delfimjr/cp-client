import React, {useState, useEffect} from 'react';
import '../../Inbox/inbox.css'
import Radium, {StyleRoot} from 'radium';

import {
    CardBody, Table
} from "reactstrap";

import CardComponent from '../../Inbox/cardComponent';
import api from "../../../Services/api";
import dateOrganizeHelper from "../../../helpers/dateOrganizeHelper";
import SingleMessageModal from "./SingleMessageModal";
import {fadeInDown} from "react-animations";
import {getHeaderWithToken} from "../../../helpers/authHelper";
import Loading from "../../Loading";
import ExclamationMark from "../../../Images/ExclamationMark.svg";

const LIMIT = 30

const styles = {
    fadeIn: {
        animation: 'x 0.7s',
        animationName: Radium.keyframes(fadeInDown, 'fadeInDown')
    },
}

const History = (props) => {

    const [searchField, setSearchField] = useState('')
    const [auxLoading, setAuxLoading] = useState(false);
    const [messages, setMessages] = useState([])
    const [loading, setLoading] = useState(false);
    const [currentPage, setCurrentPage] = useState(1);
    const [total, setTotal] = useState(0)
    const [openedMessage, setOpenedMessage] = useState('')

    useEffect(() => {
        props.handlePageTitleChange('Histórico de Mensagens')
        fetchSentMessages()
    }, [])

    useEffect(() => {
        fetchSentMessages()
    }, [currentPage])

    useEffect(() => {
        window.addEventListener("scroll", handlePageScroll)
        return () => window.removeEventListener("scroll", handlePageScroll)
    }, [handlePageScroll])

    function handlePageScroll() {
        const scrollable = document.documentElement.scrollHeight - window.innerHeight
        const scrolled = window.scrollY

        if (Math.ceil(scrolled) >= (scrollable - 3) && currentPage < total) {
            setAuxLoading(true)
            setCurrentPage(currentPage + 1)
        }
    }

    async function fetchSentMessages() {
        await api.get(`messages?page=${currentPage}&limit=${LIMIT}`, {headers: getHeaderWithToken()}).then(response => {
            setLoading(false)
            setAuxLoading(false)
            setMessages([...messages, ...response.data.messages])
            const totalPages = Math.ceil((response.data.count / LIMIT))
            setTotal(totalPages)
        })
    }

    function openSingleMessageModal(message) {
        console.log(message)
        setOpenedMessage(message)
    }

    // function handleSearch(event) {
    //     setSearchField(event.target.value)
    // }

    return (
        <StyleRoot style>
            <CardComponent>
                <CardBody className="inbox_body">
                    <div className="container">
                        <div style={{display: "flex", flexDirection: "row", justifyContent: "space-between"}}>
                            <h4>Mensagens Enviadas</h4>
                            <h6 style={{color: "#07436C"}}>scroll for more</h6>
                        </div>

                        {
                            (loading === true)
                                ?
                                <>
                                    <Loading/>
                                </>
                                :
                                (
                                    messages.length === 0
                                        ?
                                        <div style={{textAlign: "center"}}>
                                            <img src={ExclamationMark} width="260px" alt='An exclamation image'/>
                                            <h3 className="mt-4 text-secondary" style={{textAlign: "center"}}>Sem
                                                mensagens</h3>
                                        </div>
                                        :
                                        <Table className='table-hover' style={{cursor: 'pointer'}} responsive>
                                            <tbody>
                                            {
                                                messages?.map(message => (
                                                    <tr
                                                        onClick={() => openSingleMessageModal(message)}
                                                        style={{
                                                            display: 'flex',
                                                            flexDirection: 'column',
                                                            marginBottom: '1%',
                                                            padding: '.7%',
                                                            borderLeftStyle: "solid",
                                                            borderWidth: "5px",
                                                            borderColor: '#3498DB',
                                                            ...styles.fadeIn
                                                        }}>
                                                        <div style={{
                                                            display: "flex",
                                                            flexDirection: 'row',
                                                            justifyContent: "space-between"
                                                        }}>
                                                            <p>Por: {message.sender}</p>
                                                            {
                                                                message.sent_at === null
                                                                    ?
                                                                    <p class='text-warning'>Scheduled
                                                                        to {dateOrganizeHelper(message.scheduledTo)}</p>
                                                                    :
                                                                    <p class='text-info'>Sent
                                                                        At: {dateOrganizeHelper(message.sent_at)}</p>
                                                            }
                                                        </div>
                                                        <div style={{
                                                            display: "flex",
                                                            flexDirection: 'row',
                                                            justifyContent: "space-between"
                                                        }}>
                                                            <div style={{
                                                                display: "flex",
                                                                flexDirection: "row",
                                                                alignItems: "center"
                                                            }}>
                                                                <p style={{fontSize: "small"}}>Para: </p>
                                                                <p style={{display: "flex", flexDirection: "row"}}>
                                                                    {
                                                                        message.contactsFullNames.map((contact, i) => (
                                                                            i < 2 &&
                                                                            <i style={{fontSize: "small"}}>{`${message.contactsFullNames.length - 1 > i ? contact.fullName + "," : contact.fullName}`}</i>
                                                                        ))
                                                                    }
                                                                    {
                                                                        message.contactsFullNames.length > 3
                                                                        &&
                                                                        <i style={{fontSize: "small"}}>etc</i>
                                                                    }
                                                                </p>
                                                            </div>
                                                            <div>
                                                                <div style={{
                                                                    display: "flex",
                                                                    flexDirection: "row",
                                                                    alignItems: "center"
                                                                }}>
                                                                    <p style={{display: "flex", flexDirection: "row"}}>
                                                                        {
                                                                            `${message.subject} - ${message.body}`.length > 80
                                                                                ?
                                                                                `${message.subject} - ${message.body}`.slice(0, 80) + "..."
                                                                                :
                                                                                `${message.subject} - ${message.body}`
                                                                        }
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </tr>
                                                ))
                                            }
                                            </tbody>
                                        </Table>
                                )
                        }
                    </div>
                </CardBody>
            </CardComponent>
            {
                <SingleMessageModal
                    message={openedMessage}
                    close={() => setOpenedMessage('')}
                />
            }
        </StyleRoot>
    );
}

export default History;
