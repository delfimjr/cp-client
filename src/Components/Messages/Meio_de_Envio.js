import React, { Component } from "react";

import { FaWhatsapp, FaSms, FaRegEnvelope } from 'react-icons/fa'
import { TiMail } from 'react-icons/ti'

export default class EscolherMeio extends Component {

    constructor(props){
        super(props)

        this.state = {
            whatsAppFlag: false
        }
    }

    checkIt = () => {

        
    }

    render() {

        return (
            <form>
                <div class="form-row mt-4">

                    <div id="sendMode" class="form-group mt-4 ml-3">
                        <h5>Selecione o(s) método(s) de envio</h5>

                        <div class="form-check">
                            <label class="form-check-label">
                                <input  type="checkbox" className = "form-check-input shipping_method" value="" defaultChecked />
                                E-mail
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input type="checkbox" className = "form-check-input shipping_method" value="" />
                                SMS
                            </label>
                        </div>
                        {
                        this.state.whatsAppFlag
                        &&
                            <div class="form-check">
                            <label class="form-check-label">
                                <input type="checkbox" class = "form-check-input shipping_method" value="" />
                                Whatsapp
                            </label>
                        </div>
                        }
                    </div>
                </div>
            </form>
        );
    }
}