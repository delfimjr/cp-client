import React, { Component } from "react";
import { } from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import GroupSelect from './GroupSelect'
import DestinSelect from './DestinSelect'


export default class Destinatario extends Component {

    render(props) {
        return (
            <form >
                <h2>Destinatário</h2>
                <div class="form-row">
                    <div class="form-group col-sm-12">
                        <label for="inputdest">Insira o destinatário</label>
                        {/*Class that performs the Multiple Individual Selection */}
                        <DestinSelect contacts={this.props.contacts} changeDestin={this.props.changeDestin} />
                    </div>

                    <div class="form-group col-sm-12">
                        <label for="contactGroup">Selecionar grupo de contactos</label>
                        {/*Class that performs the Multiple Group Selection */}
                        <GroupSelect changeGroup={this.props.changeGroup} />
                    </div>
                </div>
            </form>
        );
    }
}
