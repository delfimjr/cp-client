import React from 'react'
import Modal from 'react-bootstrap/Modal'
import { TiArrowForward } from 'react-icons/ti'
import { FaBullhorn, FaEnvelope } from 'react-icons/fa'

import history from '../../history'

import '../Inbox/inbox.css'

import OverWriteQuestion from '../OverwriteQuestion'

export default class QuestionModal extends React.Component {
    constructor() {
        super()

        this.state = {
            overWriteText: '',
            overWritePublication: true,
            showOverWrite: false
        }
    }


    handleWriteMessage = () => {
        if (sessionStorage.getItem('message')) {
            this.setState({ showOverWrite: true })
            this.setState({ overWriteText: 'isMessage' })
            this.props.setShowModal(false)

        }
        else {
            const { messageContent } = this.props

            sessionStorage.setItem('message', JSON.stringify(messageContent));
            history.push('/dashboard/enviar-mensagem')
        }
    }

    handlePublish = () => {
        if (sessionStorage.getItem('publish')) {
            this.setState({ showOverWrite: true })
            this.setState({ overWriteText: 'isPublish' })
            this.props.setShowModal(false)

        }
        else {
            const { messageContent } = this.props

            sessionStorage.setItem('publish', JSON.stringify(messageContent));
            history.push('/dashboard/criar-publicacao')
        }
    }

    setShowModal = (estado) => {
        this.setState({ showOverWrite: estado })
    }

    render() {
        return (
            <div>
                <Modal
                    animation
                    size="sm"
                    show={this.props.showQuestion}
                    class="modal fade modal-dialog"
                    centered
                    onHide={() => this.props.setShowModal(false)}
                >

                    <Modal.Body style={{ backgroundColor: '#E8F6F3' }} className='rounded'>

                        <div className='row'>
                            <TiArrowForward style={{ color: '#ABEBC6' }} size='1x' />
                        </div>

                        <div className='row' style={{ fontFamily: '' }}>
                            <button
                                style={{ backgroundColor: '#58D68D' }}
                                onClick={this.handleWriteMessage}
                                className="b1 btn col-sm-6 rounded-0 border-0"
                                type="button"
                            >
                                Mensagem
                            <FaEnvelope className='ml-2' />
                            </button>
                            <button
                                style={{ backgroundColor: '#48C9B0' }}
                                onClick={this.handlePublish}
                                className="b2 btn col-sm-6 rounded-0 border-0"
                                type="button"
                            >
                                publicação
                            <FaBullhorn className='ml-2' />
                            </button>
                        </div>
                    </Modal.Body>
                </Modal>
                <OverWriteQuestion messageContent={this.props.messageContent} overWriteText={this.state.overWriteText} setShowModal={() => this.setShowModal()} showOverWrite={this.state.showOverWrite} />
            </div>
        )
    }
}



