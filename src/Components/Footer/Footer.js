import React, { Component } from "react";
import './footer.css';

export default class Footer extends Component {
    render() {
        return (
            <div className={this.props.classname}>
                <footer className={this.props.classname}>© {new Date().getFullYear()} MPDC. All rights reserved</footer>
            </div>
        );
    }
}
