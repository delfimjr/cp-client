import React, { useState, useEffect, useRef } from "react";
import './publicacoes.css';
import ModalDeletePost from './ModalDeletePost';
import ExclamationMark from '../../Images/ExclamationMark.svg';
import loadingMore from '../../Images/fetching_more_loading.gif';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import api from '../../Services/api'

import QuestionModal from '../ShareQuestionModal/index'
import ConfirmationModal from '../ConfirmationModal/index'
import Loading from '../Loading'
import "react-image-gallery/styles/css/image-gallery.css";
import PostCardContainer from "./PostCardContainer";
import {getHeaderWithToken} from "../../helpers/authHelper";


//CONSTS
const LIMIT = 20
const CONFIRMATION_TITLE = 'Excluir Publicação'
const CONFIRMATION_TEXT = 'Tem certeza que deseja excluir a publicação?'
const CANCEL_BUTTON = 'Não'
const SUCCESS_BUTTON = 'Sim'


const VerPublicacao = (props) => {

    const [posts, setPosts] = useState([])
    const [show, setShow] = useState(false)
    const [loading, setLoading] = useState(true)
    const [total, setTotal] = useState(0)
    const [currentPage, setCurrentPage] = useState(1)
    const [postIdToRemove, setPostIdToRemove] = useState('')
    const [showQuestion, setShowQuestion] = useState(false)
    const [messageContent, setMessageContent] = useState('')
    const [showConfirmationModal, setShowConfirmationModal] = useState(false)
    const [auxLoading, setAuxLoading] = useState(false)



    useEffect((props) => {
        fetchPublications()
    }, [])

    useEffect(() => {
        fetchPublications()
    }, [currentPage])

    useEffect(() => {
        window.addEventListener("scroll", handleScroll)
        return () => window.removeEventListener("scroll", handleScroll)
    }, [handleScroll])

    function handleScroll() {
        const scrollable = document.documentElement.scrollHeight - window.innerHeight
        const scrolled = window.scrollY

        if (Math.ceil(scrolled) >= (scrollable - 3) && currentPage < total) {
            setAuxLoading(true)
            setCurrentPage(currentPage + 1)
        }
    }

    async function fetchPublications() {

        await api.get(`publications?page=${currentPage}&limit=${LIMIT}`).then(response => {
            setLoading(false)
            setAuxLoading(false)
            setPosts([...posts, ...response.data.posts])
            const totalPages = Math.ceil((response.data.count / LIMIT))
            setTotal(totalPages)
        })
    }

    function handleShow(flag) {
        setShow(flag)
    }

    function handleClose(flag) {
        setShow(flag)
    }

    async function handleShowQuestion(flag, id) {
        const auxPost = posts.filter(post => post.id === id)
        setMessageContent({ subject: auxPost[0].subject, body: auxPost[0].body })
        setShowQuestion(flag)
    }

    function setShowModal(flag) {
        setShowQuestion(flag)
    }

    function handleConfirmationModal() {
        setShowConfirmationModal(!showConfirmationModal)
    }

    function onDeletePublication(idPublicacao) {
        setPostIdToRemove(idPublicacao)
        setShowConfirmationModal(true)
    }

    function handleModalResponse(flag) {
        if (flag === 1) {
            let auxPub = posts.filter(post => post.id !== postIdToRemove)
            setPosts(auxPub)
            setShowConfirmationModal(false)
            api.delete(`publications/${postIdToRemove}`, {headers: getHeaderWithToken()})
        } else {
            setShowConfirmationModal(false)
        }
    }

    return (
        <div>
            <ToastContainer />
            <div id="divPublicacoes">
                {
                    (loading === true)
                        ?
                        <>
                            <Loading />
                        </>

                        : (posts.length === 0
                            ? <div style={{ textAlign: "center" }}>
                                <img src={ExclamationMark} width="260px"  alt='An exclamation image'/>
                                <h3 className="mt-4 text-secondary" style={{ textAlign: "center" }}>Sem publicações</h3>
                            </div>

                            :
                            posts.map((pub, i) => {

                                let data = pub.created_at.split('T');
                                let hora = data.pop();
                                hora = hora.split(':');
                                hora.pop();

                                return (
                                    <PostCardContainer
                                        cardId='card'
                                        data={data}
                                        handleShowQuestion={handleShowQuestion}
                                        hora={hora}
                                        onDeletePublication={onDeletePublication}
                                        pub={pub}
                                        userObject={props.userObject}
                                    />
                                );
                            })
                        )
                }
                {
                    auxLoading &&
                    <p className="d-flex justify-content-center mt-4">
                        <img src={loadingMore} width="50px" />
                    </p>
                }
            </div>

            <ConfirmationModal
                showModal={showConfirmationModal}
                handleConfirmationModal={handleConfirmationModal}
                title={CONFIRMATION_TITLE}
                text={CONFIRMATION_TEXT}
                cancelButton={CANCEL_BUTTON}
                successButton={SUCCESS_BUTTON}
                handleModalResponse={handleModalResponse}
            />

            <ModalDeletePost
                handleShow={() => handleShow()}
                handleClose={() => handleClose()}
                show={show}
            />
            <QuestionModal
                messageContent={messageContent}
                setShowModal={setShowModal}
                showQuestion={showQuestion}
            />
        </div>
    );
}

export default VerPublicacao
