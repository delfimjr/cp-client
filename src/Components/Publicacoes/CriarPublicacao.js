import React, { Component } from 'react';
import { Button, Form, FormGroup } from 'react-bootstrap';
import { FiShare2 } from 'react-icons/fi';
import { FaUpload } from 'react-icons/fa';
import axios from 'axios';
import ModalPostSent from './ModalPostSent';
import { Alert } from "reactstrap"
import ReactDOM from 'react-dom';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import history from '../../history'
import { Card, CardBody, CardTitle } from "reactstrap";
import Loading from '../Loading';
import api from '../../Services/api'

import './publicacoes.css'
import {getHeaderWithToken} from "../../helpers/authHelper";

export default class CriarPublicacao extends Component {

    constructor(props) {
        super(props)
        this.state = {
            subject: '',
            body: '',
            selectedFile: null,
            show: false,
            messageText: '',
            assuntoIsEmptyBorder: '',
            bodyIsEmptyBorder: '',
            emptinessAlert: false,
            loading: false,
        }

        this.onAssuntoChange = this.onAssuntoChange.bind(this);
        this.onBodyChange = this.onBodyChange.bind(this);
        this.onSubmitPublication = this.onSubmitPublication.bind(this);
        this.handleShow = this.handleShow.bind(this);
        this.handleClose = this.handleClose.bind(this);
    }

    UNSAFE_componentWillMount() {
        this.props.handlePageTitleChange("Criar Publicação")
        if (sessionStorage.getItem('publish')) {
            this.setState({ messageText: JSON.parse(sessionStorage.getItem('publish')) })
            this.setState({ subject: JSON.parse(sessionStorage.getItem('publish')).subject })
            this.setState({ body: JSON.parse(sessionStorage.getItem('publish')).body })

        }
    }

    componentWillUnmount() {
        if (!this.state.body && !this.state.subject) {
            sessionStorage.setItem("publish", '')
        }
        else {
            sessionStorage.setItem("publish", JSON.stringify({ subject: this.state.subject, body: this.state.body }))
        }
    }

    onAssuntoChange(e) {
        this.setState({ [e.target.name]: e.target.value });
        this.setState({ emptinessAlert: false })
        this.setState({ assuntoIsEmptyBorder: '' })
        this.setState({ bodyIsEmptyBorder: '' })
    }

    onBodyChange(e) {
        this.setState({ [e.target.name]: e.target.value });
        this.setState({ emptinessAlert: false })
        this.setState({ assuntoIsEmptyBorder: '' })
        this.setState({ bodyIsEmptyBorder: '' })
    }

    async handleShow(estado) {
        await this.setState({ show: estado })
    }

    handleClose(estado) {
        this.setState({ show: estado })
        history.push('/dashboard');
    }

    onChangeHandler = event => {
        if (this.checkMimeType(event)) {
            this.setState({ selectedFile: event.target.files })
        }
        this.setState({ emptinessAlert: false })
        this.setState({ assuntoIsEmptyBorder: '' })
        this.setState({ bodyIsEmptyBorder: '' })
    }

    //validacao do formato dos ficheiros
    checkMimeType = (event) => {
        let format;
        let files = event.target.files
        let err = ''
        const types = [
            'image/jpeg',
            'image/png',
            'image/pjpeg',
            'image/gif',
            'image/svg',
            'application/pdf',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'application/vnd.openxmlformats-officedocument.presentationml.presentation',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'application/msword'
        ]
        for (var x = 0; x < files.length; x++) {
            if (types.every(type => files[x].type !== type)) {
                format = files[x].type.split('/');
                format = format.pop();
                err += 'Ficheiros do formato ' + format + ' não são permitidos!\n Se se tratar de um documento, por favor, converta para o formato PDF.';
            }
        };

        if (err !== '') {
            event.target.value = null
            toast.error(err)
            return false;
        }

        return true;
    }

    async onSubmitPublication(e) {
        e.preventDefault();
        const { userObject } = this.props;

        if (this.state.subject === '' || this.state.body === '') {
            this.setState({ emptinessAlert: true })
            if (this.state.subject === '') {
                this.setState({ assuntoIsEmptyBorder: 'border border-warning' })
            }
            else {
                this.setState({ bodyIsEmptyBorder: 'border border-warning' })
            }
        }

        const data = new FormData();
        this.setState({ loading: true })
        if (this.state.selectedFile != null) {
            for (var index = 0; index < this.state.selectedFile.length; index++) {
                data.append('file', this.state.selectedFile[index])
            }
        }

        data.append('content', JSON.stringify({
            subject: this.state.subject,
            body: this.state.body,
            communicationsId: userObject.user.id
        }))

        api.post("publications", data, {headers: getHeaderWithToken()})
            .then(res => {
                this.setState({ loading: false })
                this.setState({ subject: '' })
                this.setState({ body: '' })
                this.setState({ subject: '' })
                toast.success('A Publicação foi partilhada')
                setTimeout(() => {
                    history.push('/dashboard/publicacoes')
                }, 2000)
            })
            .catch(err => {
                this.setState({ loading: false })
                if (err?.response?.data?.error) {
                    toast.error(err?.response?.data?.error)
                }
                else {
                    toast.error('❌ Não foi possível publicar o conteudo!')
                }
            })
    }

    render() {
        const { messageText, emptinessAlert } = this.state;

        return (
            <div>
                <ToastContainer />
                {(this.state.loading === true)
                    ? <Loading />
                    : null
                }

                <Card className="shadow-sm p-3 mb-4">
                    <CardTitle>
                        <span style={{ display: "flex" }}>
                            <h4 className="title-contacts">
                                Publicação
                                &nbsp;
                                <FiShare2
                                    size={26}
                                    style={{ color: "#B2BABB " }}
                                />
                            </h4>
                        </span>
                    </CardTitle>

                    <CardBody>
                        <Form className="form">
                            <FormGroup>
                                <Form.Label>Título</Form.Label>
                                {
                                    (messageText !== '')
                                        ? <Form.Control
                                            size="md"
                                            className={`form-control ${this.state.assuntoIsEmptyBorder} publication-input`}
                                            type="text"
                                            name='subject'
                                            placeholder="Escrever título da publicação"
                                            value={this.state.subject}
                                            onChange={this.onAssuntoChange}
                                        />
                                        : <Form.Control
                                            className={`form-control ${this.state.assuntoIsEmptyBorder} publication-input`}
                                            size="md"
                                            type="text"
                                            name='subject'
                                            value={this.state.subject}
                                            placeholder="Escrever título da publicação"
                                            onChange={this.onAssuntoChange}
                                        />
                                }

                            </FormGroup>
                            <Form.Group>
                                <Form.Label>Corpo</Form.Label>
                                {
                                    (messageText !== '')
                                        ? <Form.Control
                                            className={`form-control ${this.state.bodyIsEmptyBorder}`}
                                            as="textarea"
                                            name='body'
                                            placeholder="Escrever o corpo da publicação"
                                            value={this.state.body}
                                            rows="15"
                                            onChange={this.onBodyChange}
                                        />
                                        : <Form.Control
                                            className={`form-control ${this.state.bodyIsEmptyBorder} publication-input`}
                                            as="textarea"
                                            name='body'
                                            value={this.state.body}
                                            placeholder="Escrever o corpo da publicação"
                                            rows="15"
                                            onChange={this.onBodyChange}
                                        />
                                }

                            </Form.Group>
                        </Form>

                        <label>Seleccione ficheiros</label>
                        <div style={{ border: "solid 1px #d3d3d3", padding: "10px", width: "50%" }}>
                            <form action="\CriarPublicacao" method="POST">
                                <label for="fileInput"><FaUpload size={25} className="mr-3" style={{ color: "#3b6186" }} /></label>
                                <input multiple onChange={this.onChangeHandler} type="file" id="fileInput" name="file" />
                            </form>
                        </div>
                    </CardBody>

                    {
                        !emptinessAlert
                            ? null
                            : <div className="row justify-content-center mt-1">
                                <Alert className="col-6 text-center" color="warning">
                                    <span>Por favor, preencha os campos vazios.</span>
                                </Alert>
                            </div>
                    }

                    <div style={{ display: "flex", justifyContent: "center" }}>
                        <Button
                            style={{ width: "200px" }}
                            variant="primary"
                            className="mt-5 ml-2"
                            onClick={this.onSubmitPublication}
                        >
                            Publicar
                            &nbsp;
                            <FiShare2 />
                        </Button>
                    </div>
                </Card>

                <ModalPostSent
                    handleShow={() => this.handleShow()}
                    handleClose={() => this.handleClose()}
                    show={this.state.show}
                />
            </div>
        );
    }
}
