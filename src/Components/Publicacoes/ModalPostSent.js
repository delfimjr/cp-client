import React, { Component } from 'react'
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'
import { BrowserRouter as Router, Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheck} from '@fortawesome/free-solid-svg-icons';


export default class ModalPostSent extends Component {

    constructor(props) {
        super(props)
    }

    render() {
        return (
            <>
                <Modal style={{marginTop:"10%", marginLeft:"5%"}} show={this.props.show} onHide={() => this.props.handleClose(false)}>
                    <Modal.Body style={{textAlign:"center"}}> <FontAwesomeIcon icon={faCheck} color="green" size="3x" className="mr-3"/> Publicação carregada com êxito!</Modal.Body>
                    <Modal.Footer >
                        <Button variant="secondary" onClick = {() =>this.props.handleClose(false)}>OK</Button>
                        <Link to = "/dashboard/publicacoes">
                            <Button variant="primary">Ver</Button>
                        </Link>
                    </Modal.Footer>
                </Modal>
            </>
        )
    }
}