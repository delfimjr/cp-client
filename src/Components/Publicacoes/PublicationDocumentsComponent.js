import React, { useEffect, useState } from 'react'
import { API_URL } from '../../helpers/api';
import { Button } from 'react-bootstrap';

import {
    FaRegFilePdf,
    FaRegFileExcel,
    FaRegFileWord,
    FaRegFilePowerpoint
} from 'react-icons/fa'

const PublicationDocumentsComponent = ({ pub }) => {
    const [documents, setDocuments] = useState([])

    useEffect(() => {
        const auxDocuments = pub.attachments.filter(att =>
            att.includes(".pdf") ||
            att.includes(".xlsx") ||
            att.includes(".xls") ||
            att.includes(".docx") ||
            att.includes(".doc") ||
            att.includes(".ppt") ||
            att.includes(".pptx"))

        setDocuments(auxDocuments.map(aux => `${API_URL}/files/${aux}`))

    }, [])


    return (
        <>

            {
                (documents.length > 0)
                    ?
                    <p className="alertaDownload">Clique no(s) anexo(s) abaixo para baixar &darr;</p>
                    :
                    null
            }

            {
                documents.map(document => {
                    let doc = document.split('-');
                    doc.shift();
                    console.log("EX", doc)

                    if (document.indexOf('.pdf') !== -1) {
                        return (
                            <p style={{ marginBottom: "5px" }}>
                                <a href={document} target="">
                                    <Button variant="light" style={{ backgroundColor: '#f0f0f0' }}>
                                        <FaRegFilePdf className="mr-2" size={25} style={{ color: '#C0392B' }} /> {doc.join('-')}
                                    </Button>
                                </a>
                            </p>
                        );
                    } 
                    else {
                        if (document.indexOf('.xlsx') !== -1 || document.indexOf('.xls') !== -1) {
                            return (
                                <p style={{ marginBottom: "5px" }}>
                                    <a href={document} target="">
                                        <Button variant="light" style={{ backgroundColor: '#f0f0f0' }}>
                                            <FaRegFileExcel className="mr-2" size={25} style={{ color: 'green' }} /> {doc.join('-')}
                                        </Button>
                                    </a>
                                </p>
                            );
                        } else {
                            if (document.indexOf('.docx') !== -1 || document.indexOf('.doc') !== -1) {
                                return (
                                    <p style={{ marginBottom: "5px" }}>
                                        <a href={document} target="">
                                            <Button variant="light" style={{ backgroundColor: '#f0f0f0' }}>
                                                <FaRegFileWord className="mr-2" size={25} style={{ color: 'blue' }} /> {doc.join('-')}
                                            </Button>
                                        </a>
                                    </p>
                                );
                            } 
                            else {
                                if (document.indexOf('.pptx') !== -1 || document.indexOf('.ppt') !== -1) {
                                    return (
                                        <p style={{ marginBottom: "5px" }}>
                                            <a href={document} target="">
                                                <Button variant="light" style={{ backgroundColor: '#f0f0f0' }}>
                                                    <FaRegFilePowerpoint className="mr-2" size={25} style={{ color: 'red' }} /> {doc.join('-')}
                                                </Button>
                                            </a>
                                        </p>
                                    );
                                }
                            }
                        }
                    }

                })
            }
        </>
    )
}

export default PublicationDocumentsComponent