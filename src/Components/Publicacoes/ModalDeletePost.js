import React, { Component } from 'react'
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'
import { BrowserRouter as Router, Link } from 'react-router-dom';
import deleteicon from '../../Images/deleteicon.png';
import './publicacoes.css'


export default class ModalPostSent extends Component {

    constructor(props) {
        super(props)
    }

    render() {
        return (
            <>
                <Modal id="modalId" style={{marginTop:"10%", marginLeft:"5%"}} show={this.props.show} onHide={() => this.props.handleClose(false)}>
                    <Modal.Body style={{textAlign:"center"}}> <img src={deleteicon} width="55px" className="mr-3"/>Publicação Exluída</Modal.Body>
                    <Modal.Footer >
                        <Button variant="primary" onClick = {() =>this.props.handleClose(false)}>OK</Button>  
                    </Modal.Footer>
                </Modal>
            </>
        )
    }
}