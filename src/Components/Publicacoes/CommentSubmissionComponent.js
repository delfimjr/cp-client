import React, { useEffect, useState } from 'react'
import { FaUserCircle } from 'react-icons/fa';
import { MdPlayArrow } from 'react-icons/md';
import { Alert } from "reactstrap"
import { Button } from 'react-bootstrap';
import validator from 'validator';
import { toast } from 'react-toastify';
import api from '../../Services/api'
import numberFormatValidationHelper from '../../helpers/phoneNumberValidationHelper';
import Loading from '../Loading';

const CommentSubmissionComponent = ({ pub }) => {

    const [contact, setContact] = useState('')
    const [name, setName] = useState('')
    const [contactIsEmptyBorder, setContactIsEmptyBorder] = useState('')
    const [commentIsEmptyBorder, setCommentIsEmptyBorder] = useState('')
    const [sending, setSending] = useState(false)
    const [comment, setComment] = useState('')
    const [showMoreFields, setShowMoreFields] = useState(false)

    function showMoreFieldHandler() {
        setShowMoreFields(!showMoreFields)
    }

    function nameChangeHandler(event) {
        setName(event.target.value)
    }

    function contactChangeHandler(event) {
        setContact(event.target.value)
        setContactIsEmptyBorder('')
    }

    function commentChangeHandler(event) {
        setComment(event.target.value)
        setCommentIsEmptyBorder('')
    }

    async function commentSubmissionHandler() {
        if (contact === '' || comment === '') {
            if (contact === '') {
                setContactIsEmptyBorder('border border-warning')
            }
            if (comment === '') {
                setCommentIsEmptyBorder('border border-warning')
            }
            toast.info('Por favor, preencha os campos obrigatorios')
        }
        else {
            setSending(!sending)

            const isEmail = validator.isEmail(contact)
            const isValidNumber = await numberFormatValidationHelper([contact.indexOf('+258') === -1 ? `+258${contact}` : contact])

            if (isValidNumber !== '' && !isEmail) {
                toast.info("O contacto introduzido  é inválido")
            }
            else {
                const auxContact = contact.indexOf('@') === -1
                    ?
                    (
                        contact.indexOf('+258') === -1
                            ?
                            `+258${contact}`
                            :
                            contact
                    )
                    :
                    contact
                // const headers = {
                //     'Authorization': 'Bearer ' + JSON.parse(sessionStorage.getItem('id')).accessToken
                // }
                api.post('publicationComments', {
                    authorContact: auxContact,
                    authorName: name,
                    commentBody: comment,
                    publicationId: pub.id
                }).then(response => {
                    setSending(false)
                    setShowMoreFields(!showMoreFields)
                    toast.success('Seu comentario foi Enviado.')
                })
                    .catch((err) => {
                        setSending(false)
                        if (err?.response?.data?.error) {
                            toast.error(err?.response?.data?.error)
                        }
                        else {
                            toast.error('❌ Não foi possível submeter comentário. Verifique sua ligação à internet e tente novamente ou')
                        }
                    })
            }
        }
    }

    return (
        <>
            <hr />
            <span className="ml-2">
                <FaUserCircle size={25} style={{ color: "#949191" }} />
                <MdPlayArrow size={10} style={{ color: "#949191" }} />

                {
                    !showMoreFields
                        ?
                        <form className="comments">
                            <input
                                className="form-control form-control-sm inputs"
                                type="text"
                                placeholder="Deixe aqui o seu comentario sobre este post..."
                                onClick={showMoreFieldHandler}
                            />
                        </form>
                        :
                        <>
                            <form className="comments">
                                <input
                                    className={`form-control form-control-sm ${contactIsEmptyBorder} inputs`}
                                    type="text"
                                    name='contact'
                                    value={contact}
                                    placeholder="Informe seu email ou numero de telefone(+258)"
                                    onChange={contactChangeHandler}
                                />

                                <input
                                    className="form-control form-control-sm inputs"
                                    type="text"
                                    name='name'
                                    value={name}
                                    placeholder="Informe seu Nome (Opcional)"
                                    onChange={nameChangeHandler}
                                />

                                <textarea
                                    className={`form-control ${commentIsEmptyBorder} inputs`}
                                    id="exampleFormControlTextarea1"
                                    name='comment'
                                    value={comment}
                                    rows="5"
                                    placeholder="Escreva o seu comentario"
                                    onChange={commentChangeHandler}
                                />

                                <Button
                                    variant="danger"
                                    className="mr-2"
                                    onClick={showMoreFieldHandler}
                                >
                                    Cancelar
                                </Button>
                                <Button
                                    variant="success"
                                    onClick={commentSubmissionHandler}
                                >
                                    Enviar
                                </Button>
                            </form>
                            {
                                sending
                                &&
                                <Loading />
                            }
                        </>
                }
            </span>
        </>
    )
}

// return (
//     <>
//         <hr />
//         <span className="ml-2">
//             <FaUserCircle size={25} style={{ color: "#949191" }} />
//             <MdPlayArrow size={10} style={{ color: "#949191" }} />

//             {(comment === false)
//                 ?
//                 <form className="comments">
//                     <input
//                         className="form-control form-control-sm inputs"
//                         type="text"
//                         value={comment}
//                         placeholder="Deixe aqui o seu comentario sobre este post..."
//                         onClick={() => renderCommentBox(true, pub.idPublicacao)}
//                     />
//                 </form>
//                 :
//                 (idPub === pub.idPublicacao
//                     ?
//                     <>
//                         <form className="comments">
//                             <input
//                                 className={`form-control form-control-sm ${contactIsEmptyBorder} inputs`}
//                                 type="text"
//                                 name='contacto'
//                                 value={contact}
//                                 placeholder="Informe seu email ou numero de telefone (inclua o prefixo do seu país, ex. +2588xxxxxxxx)"
//                                 onChange={handleInputData}
//                             />

//                             <input
//                                 className="form-control form-control-sm inputs"
//                                 type="text"
//                                 name='nome'
//                                 value={name}
//                                 placeholder="Informe seu nome completo (Opcional)"
//                                 onChange={handleInputData}
//                             />

//                             <textarea
//                                 className={`form-control ${commentIsEmptyBorder} inputs`}
//                                 id="exampleFormControlTextarea1"
//                                 name='comentario'
//                                 value={comment}
//                                 rows="5"
//                                 placeholder="Escreva o seu comentario"
//                                 onChange={handleInputData}
//                             >

//                             </textarea>

//                             {(
//                                 !emptinessAlert
//                                     ? null
//                                     : <div className="row justify-content-center mt-1">
//                                         <Alert className="col-6 text-center" color="warning">
//                                             <span>Os campos de contacto e comentário não podem estar vazios!</span>
//                                         </Alert>
//                                     </div>
//                             )}

//                             {(sending === true)
//                                 ? <><div className="d-flex justify-content-center">
//                                     <div class="spinner-border text-primary" role="status"></div>
//                                 </div>
//                                     <p className="d-flex justify-content-center mt-2">A enviar...</p>
//                                 </>
//                                 : null
//                             }

//                             <Button
//                                 variant="secondary"
//                                 className="mr-2"
//                                 onClick={() => renderCommentBox(false)}
//                             >
//                                 Cancelar
//                                                             </Button>
//                             <Button
//                                 variant="secondary"
//                                 onClick={() => onSubmitComment(pub.idPublicacao)}
//                             >
//                                 Enviar
//                                                             </Button>
//                         </form></>
//                     :
//                     <form className="comments">
//                         <input
//                             className="form-control form-control-sm inputs"
//                             type="text"
//                             placeholder="Deixe aqui o seu comentario sobre este post..."
//                             onClick={() => renderCommentBox(true, pub.idPublicacao)}
//                         />
//                     </form>
//                 )
//             }<>
//                 <hr />
//                 <span className="ml-2">
//                     <FaUserCircle size={25} style={{ color: "#949191" }} />
//                     <MdPlayArrow size={10} style={{ color: "#949191" }} />

//                     {(comment === false)
//                         ?
//                         <form className="comments">
//                             <input
//                                 className="form-control form-control-sm inputs"
//                                 type="text"
//                                 placeholder="Deixe aqui o seu comentario sobre este post..."
//                                 onClick={() => renderCommentBox(true, pub.idPublicacao)}
//                             />
//                         </form>
//                         :
//                         (idPub === pub.idPublicacao
//                             ?
//                             <>
//                                 <form className="comments">
//                                     <input
//                                         className={`form-control form-control-sm ${contactIsEmptyBorder} inputs`}
//                                         type="text"
//                                         name='contacto'
//                                         value={contact}
//                                         placeholder="Informe seu email ou numero de telefone (inclua o prefixo do seu país, ex. +2588xxxxxxxx)"
//                                         onChange={handleInputData}
//                                     />

//                                     <input
//                                         className="form-control form-control-sm inputs"
//                                         type="text"
//                                         name='nome'
//                                         placeholder="Informe seu nome completo (Opcional)"
//                                         onChange={handleInputData}
//                                     />

//                                     <textarea
//                                         className={`form-control ${commentIsEmptyBorder} inputs`}
//                                         id="exampleFormControlTextarea1"
//                                         name='comentario'
//                                         rows="5"
//                                         placeholder="Escreva o seu comentario"
//                                         onChange={handleInputData}
//                                     >

//                                     </textarea>

//                                     {(
//                                         !emptinessAlert
//                                             ? null
//                                             : <div className="row justify-content-center mt-1">
//                                                 <Alert className="col-6 text-center" color="warning">
//                                                     <span>Os campos de contacto e comentário não podem estar vazios!</span>
//                                                 </Alert>
//                                             </div>
//                                     )}

//                                     {(sending === true)
//                                         ? <><div className="d-flex justify-content-center">
//                                             <div class="spinner-border text-primary" role="status"></div>
//                                         </div>
//                                             <p className="d-flex justify-content-center mt-2">A enviar...</p>
//                                         </>
//                                         : null
//                                     }

//                                     <Button
//                                         variant="secondary"
//                                         className="mr-2"
//                                         onClick={() => renderCommentBox(false)}
//                                     >
//                                         Cancelar
//                     </Button>
//                                     <Button
//                                         variant="secondary"
//                                         onClick={() => onSubmitComment(pub.idPublicacao)}
//                                     >
//                                         Enviar
//                     </Button>
//                                 </form></>
//                             :
//                             <form className="comments">
//                                 <input
//                                     className="form-control form-control-sm inputs"
//                                     type="text"
//                                     placeholder="Deixe aqui o seu comentario sobre este post..."
//                                     onClick={() => renderCommentBox(true, pub.idPublicacao)}
//                                 />
//                             </form>
//                         )
//                     }
//                 </span>
//             </>
//         </span>
//     </>
// )

export default CommentSubmissionComponent;
