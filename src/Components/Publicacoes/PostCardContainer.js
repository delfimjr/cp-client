import React from 'react'
import './publicacoes.css';
import { Button, Carousel, Form, FormGroup } from 'react-bootstrap';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { FaRegCalendarAlt, FaUserCircle } from 'react-icons/fa';
import { BsTrash } from 'react-icons/bs';
import { RiShareForwardLine } from 'react-icons/ri';
import { TiArrowForwardOutline } from 'react-icons/ti';
import { MdPlayArrow, MdKeyboardArrowLeft, MdKeyboardArrowRight } from 'react-icons/md';
import ImageGallery from 'react-image-gallery';
import CommentSubmissionComponent from "./CommentSubmissionComponent";
import PublicationImageContainer from "./publicationImageContainer"
import PublicationDocumentsComponent from "./PublicationDocumentsComponent"

const PostCardContainer = ({data, hora, cardId, pub, onDeletePublication, handleShowQuestion, userObject, fromNotifications}) => {
    return (
        <Card id={cardId}>
            <CardHeader
                title="Departamento de Comunicações · MPDC"
                subheader={
                    <span>
                        <FaRegCalendarAlt size={18} className="mr-1" />
                        {data + ', ' + hora.join(':')}
                    </span>
                }
            />

            <CardContent>
                <Typography variant="subtitle1" id="assunto" component="p">
                    {pub.subject}
                </Typography>

                <Typography variant="body1" id="corpo">
                    <pre id="corpoPreTag">
                        {pub.body}
                    </pre>
                </Typography>
                {
                    pub.attachments?.length > 0
                    &&
                    <>
                        <PublicationImageContainer pub={pub} />
                        <PublicationDocumentsComponent pub={pub} />
                    </>
                }
            </CardContent>

            {
                (userObject != null)
                    ?
                    <span>
                        <BsTrash
                            size={25}
                            style={{ marginLeft: "83%", marginBottom: "3%", cursor: 'pointer' }}
                            data-toggle="tooltip"
                            data-placement="top"
                            title="Excluir Publicação"
                            onClick={() => onDeletePublication(pub.id)}
                        />
                        <RiShareForwardLine
                            size={30} style={{ marginBottom: "3%", cursor: 'pointer' }}
                            className="ml-4"
                            data-toggle="tooltip"
                            data-placement="top"
                            title="Reencaminhar"
                            onClick={() => handleShowQuestion(true, pub.id)}
                        />
                    </span>
                    :
                    <CommentSubmissionComponent pub={pub} />
            }
        </Card>
    )
}

export default PostCardContainer;