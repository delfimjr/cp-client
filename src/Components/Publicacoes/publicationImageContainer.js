import React, { useEffect, useState } from 'react'
import { API_URL } from '../../helpers/api'
import ImageGallery from 'react-image-gallery';


const PublicationImageContainer = ({ pub }) => {
    const [images, setImages] = useState([])

    useEffect(() => {
        const auxImages = pub.attachments.filter(att =>
            att.includes(".jpeg") ||
            att.includes(".jpg") ||
            att.includes(".png") ||
            att.includes(".pjpeg") ||
            att.includes(".gif") ||
            att.includes(".svg"))

        setImages(auxImages.map(aux => ({
            original: `${API_URL}/files/${aux}`,
            thumbnail: `${API_URL}/files/${aux}`
        })))

        // if (auxImages.length > 1) {

        // }
        // else {
        //     setImages(auxImages.map(aux => ({
        //         original: `${API_URL}/files/${aux}`
        //     })))
        // }
    }, [])


    if (images?.length > 0)
        return (
            < ImageGallery
                items={images}
                showPlayButton={false}
                showNav
                showThumbnails={true}
            />
        )
    else
        return (<></>)
}

export default PublicationImageContainer;